package controller;

import java.util.List;

import javax.swing.JOptionPane;

import view.MainView;

import model.SudokuModel;

import classes.Registry;

import exception.SudokuException;

/**
 * <strong>Klasse BaseOpenSudokuController</strong>
 * 
 * <p>Diese Klasse manupuliert das Model
 * des JTable indem sie zuf�llig ein Sudoku aus einer
 * Liste l�dt..</p>
 * 
 * @see		OpenSudokuTextController
 * @see		OpenSudokuDBController
 * @author 	Marco Martens
 * @version 1.0 M�rz 2013
 *
 */

public class BaseOpenSudokuController {
	
	
	/**
	 * MainView
	 */
	
	private SudokuModel _model;
	
	/**
	 * Sudokugr��e
	 */
	
	private int size =  Registry.get().getInt("size");

	
	/**
	 * MainView
	 */
	protected MainView _mainView;
	
	
	/**
	 * Konstruktor
	 * 
	 * <p>L�dt das Model</p>
	 */
	
	public BaseOpenSudokuController(MainView _mainView, SudokuModel _model) {
		this._model = _model;
		this._mainView  = _mainView;
	}
	
	/**
	 * Setzt die Zahlen des geladenen Sudokus im Model.
	 * 
	 * 
	 * @throws SudokuException
	 */
	
	protected void updateSudokuModel (List<String> sudokus) throws SudokuException {
			//Zufalls Sudoku bestimmen
			int random = 0;
			int max = sudokus.size();
			if (max == 0) {
				throw new SudokuException("Die Sudokudatei darf nicht leer sein.");
			}
			
			String sudoku;
			random = (int) ((Math.random() * (0 + max)));
			
			
			//Sudoku holen
			sudoku = sudokus.get(random);
			//Sudoku teilen

		
			int stringend = sudoku.length();
			
			if(stringend != (size*size)) {
				throw new SudokuException("Die Gr��e der Sudokus in der Datei stimmen nicht mit den Einstellungen �berein. �berpr�fen Sie ihre Einstellungen.");
			}
			int end = 1;
			int start = 0;
			int[][] orgSudoku = new int[_model.getRowCount()][_model.getColumnCount()];
			while (end < stringend-1) {
				for (int row = 0; row < _model.getRowCount(); row++) {
					for (int col = 0; col < _model.getColumnCount(); col++) {
						Integer output = null;
						try {
							output = Integer.parseInt(sudoku.substring(start, end));
					
						}
						catch (NumberFormatException e1) {
							e1.printStackTrace();
							JOptionPane.showMessageDialog(null,e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
						}
					
						if (output == 0) {
							_model.setValueAt(null, row, col);
							orgSudoku[row][col]=0;
						} else {
							_model.setValueAt(output, row, col);
							orgSudoku[row][col]=output;
						}
						start++;
						end++;
					}
				}
			}
			_model.setOrgSudoku(orgSudoku);
		}
}
