package controller;
import javax.swing.JOptionPane;
import view.MainView;

import model.SudokuModel;

import classes.HumanSolver;


/**
 * <strong>Klasse TippController</strong>
 * 
 * <p>�ffnet einen Tipp</p>
 * 
 * @see		classes.BaseSolver
 * @author 	Marco Martens
 * @version 1.0 Dezember 2012
 *
 */
public class TippController {

	/**
	 * SudokuModel
	 */
	
	private SudokuModel _model;
	
	/**
	 * MainView
	 */
	
	private MainView _view;
	
	/**
	 * Konstruktor
	 * 
	 * <p>Legt das Model, die View fest und zeigt einen Tipp an</p>
	 * @param _model
	 * @param _view
	 */
	
	public TippController (SudokuModel _model, MainView _view) {
		this._model = _model;
		this._view = _view;
		executeAction ();
	}
	
	/**
	 * Zeigt einen Tipp an.
	 */
	
	public void executeAction () {
		// TODO Auto-generated method stub
				
		int row = _view.getSelectedRow();
		int col = _view.getSelectedColumn();
				
				if (row == -1 || col==-1) {
					JOptionPane.showMessageDialog(null,"Sie m�ssen erst eine Zelle ausw�hlen.", "Tipp", JOptionPane.INFORMATION_MESSAGE);
				} else {
					int[][] sudoku = new int[9][9];
					sudoku = _model.SudokuModeltoArray();
					final HumanSolver solver = new HumanSolver(sudoku);
					if (sudoku[row][col] == 0) {
						JOptionPane.showMessageDialog(null,solver.getCandidates(row, col) + "" + row + ","+ col , "Tipp", JOptionPane.INFORMATION_MESSAGE);
					}
					
				}
	}
	

}
