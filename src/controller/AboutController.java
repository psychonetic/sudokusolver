package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.AboutView;



/**
 * <strong>Klasse AboutController</strong>
 * 
 * <p>Diese Klasse zeigt ein typischen "�ber" Men� an.
 * Es zeigt wichtige Informationen zu dem Ersteller des Programmes
 * und zu der Lizenz unter welcher das Programm ver�ffentlicht wurde.</p>
 * 
 * @see 	AboutView
 * @author 	Marco Martens
 * @version 1.0 Dezember 2012
 *
 */
public class AboutController {

	/**
	 * View 
	 */
	
	private AboutView _view;
	
	/**
	 * Konstruktor
	 * 
	 * <p>Startet eine Instanz der View, legt die Listener fest und
	 * zeigt die View schlie�lich an.</p>
	 * 
	 */
	
	public AboutController () {
		this._view = new AboutView(null);
		setListener();
		showView();
	}
	
	/**
	 * Legt die Listener fest
	 */
	
	private void setListener () {
		_view.setExitListener(new ExitListener());
	}
	
	/**
	 * Zeigt die View an 
	 */
	
	public void showView() {
		_view.setVisible(true);
	}
	
	/**
	 * <strong>ExitListener</strong>
	 * 
	 * <p>Schlie�t die View</p>
	 */
	
	class ExitListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			_view.setVisible(false);
	        _view.dispose();
		}
		
	}
	

}
