package controller;

import classes.Registry;
import classes.SudokuGenerator;
import model.SudokuModel;


/**
 * <strong>Klasse GeneratorController</strong>
 * 
 * <p>Generiert das Sudoku anhand der Einstellungen</p>
 * 
 * 
 * @see		SudokuGenerator
 * @author 	Marco Martens
 * @version 1.0 Dezember 2012
 *
 */

public class GeneratorController {
	
	/**
	 * SudokuModel
	 */
	private SudokuModel _model;
	
	/**
	 * Konstruktor
	 * 
	 * <p>Setzt das Model und startet die Generierung</p>
	 * @param _model
	 */
	
	public GeneratorController (SudokuModel _model) {
		this._model = _model;
		executeAction();
	}
	
	/**
	 * Generiert das Sudoku
	 */
	private void executeAction() {
		generateSudoku();
	}
	/**
	 * Generiert das Sudoku
	 * 
	 */
	private void generateSudoku () {
		int[][] sudoku = new int[9][9];
		//Gr��e festlegen...
		//getSize -> Registry
		//Zufall zwischen x<->y
		//Sudoku generieren
		String grade = Registry.get().getString("grade");
		String type  = Registry.get().getString("generatetype");
		int size = 0;
		switch (grade) {
		case "easy": 
				size =  (int) (Math.random()* (55 - 45) + 45);
				break; //45 - 55
			case "normal": 
				size =  (int) (Math.random()* (45 - 35) + 35);
				break; //35-45
			case "hard": 
				size =  (int) (Math.random()* (35 - 25) + 25);
				break; //25-35
			case "hell": 
				size =  (int) (Math.random()* (25 - 17) + 17);
				break; //17-25
			
			
			default: size =  (int) (Math.random()* (46 - 35) + 35);
			break;
		}
		
		SudokuGenerator g = new SudokuGenerator(type, (81 - size));
		sudoku = g.generate();
		for (int frow = 0; frow < sudoku.length; frow++) {
			for (int fcol = 0; fcol < sudoku[0].length; fcol++) {
				//g.returnSudoku();
				//Fehler!!
				int value = sudoku[frow][fcol];
				if (value == 0) {
					_model.setValueAt(null, frow, fcol);
				} else {
					_model.setValueAt(value, frow, fcol);
				}
				
			}
		}
		_model.setOrgSudoku(sudoku);
	}
}
