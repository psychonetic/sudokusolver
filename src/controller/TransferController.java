package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import classes.SudokuTransfer;
import exception.SudokuException;

import view.SudokuFileChooserView;
import view.TransferView;


/**
 * <strong>Klasse TransferController</strong>
 * 
 * <p>�ffnet ein Menu um Sudokus zu importieren und exportieren.</p>
 * 
 * @see		SudokuTransfer
 * @see		TransferView
 * @author 	Marco Martens
 * @version 1.0 Dezember 2012
 *
 */

public class TransferController {

	
	/**
	 * TransferView
	 */
	
	private TransferView _view;
	
	/**
	 * SudokuTransfer
	 */
	
	private SudokuTransfer transfer;
	
	/**
	 * Konstruktor
	 * 
	 * <p>L�dt die View, setzt die Listener und zeigt den View an.</p>
	 */
	
	public TransferController () {
		_view = new TransferView(null);
		setListener();
		showView();
	}
	
	/**
	 * Setzt die Listener
	 */
	
	private void setListener() {
		_view.setStartListener(new StartListener());
		_view.setExitListener(new ExitListener());
		_view.setSearchListener(new SearchListener());
	}
	
	/**
	 * Zeigt den View an
	 */
	
	public void showView () {
		_view.setVisible(true);
	}
	
	/**
	 * 
	 * <strong>Klasse StartListener</strong>
	 * 
	 * <p>Startet den Import oder den Export.</p>
	 * 
	 * 
	 * @author 	Marco Martens
	 * @version 1.0 Dezember 2012
	 *
	 */
	
	class StartListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			String source = _view.getSource();
			String target = _view.getTarget();
			String file = _view.getFile();
			if(source.equals(target)) {
				JOptionPane.showMessageDialog(null,"Die Quelle und das Ziel m�ssen sich unterscheiden.", "Error", JOptionPane.ERROR_MESSAGE);
			} else {
				if(file.equals("")) {
					try {
						throw new IOException ("Es wurde keine Datei �bergeben.");
					} catch (IOException e) {
						// TODO Auto-generated catch block
						JOptionPane.showMessageDialog(null,e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
						e.printStackTrace();
						
					}
				} else {
					try {
						transfer = new SudokuTransfer();
					} catch (InstantiationException | IllegalAccessException
							| ClassNotFoundException | SQLException e) {
						// TODO Auto-generated catch block
						JOptionPane.showMessageDialog(null,"Es ist ein Fehler mit der Datenbank aufgetreten: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
						e.printStackTrace();
					}
					
					
					
					
					
					if (!source.equals(target)) {
						if (source=="Text") {
							try {
								try {
									transfer.importSudokusToDatabase(new File(file));
								} catch (SQLException e) {
									// TODO Auto-generated catch block
									JOptionPane.showMessageDialog(null,"Es ist ein Fehler mit der Datenbank aufgetreten: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
									e.printStackTrace();
								}
								JOptionPane.showMessageDialog(null,"Der Vorgang wurde erfolgreich durchgef�hrt.", "Information", JOptionPane.INFORMATION_MESSAGE);
							} catch (IOException e1) {
								// TODO Auto-generated catch block
								JOptionPane.showMessageDialog(null,"Es ist ein Fehler mit der Datenbank aufgetreten: " + e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
								e1.printStackTrace();
							} catch (SudokuException e2) {
								// TODO Auto-generated catch block
								JOptionPane.showMessageDialog(null, e2.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
								e2.printStackTrace();
							}
						} else {
							try {
								transfer.exportSudokusToText(new File(file));
								JOptionPane.showMessageDialog(null,"Der Vorgang wurde erfolgreich durchgef�hrt.", "Information", JOptionPane.INFORMATION_MESSAGE);
							} catch (SudokuException e1) {
								// TODO Auto-generated catch block
								JOptionPane.showMessageDialog(null,e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
								e1.printStackTrace();
							} catch (IOException e2) {
								// TODO Auto-generated catch block
								JOptionPane.showMessageDialog(null,e2.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
								e2.printStackTrace();
							}
						}
						
					}
				}	
			}
			
			
			
			
			
			
			
		
		}
	}
	
	/**
	 * <strong>Klasse SearchListener</strong>
	 * 
	 * <p>Erm�glicht es eine Datei zum Import/Export auszuw�hlen.</p>
	 *
	 * @author 	Marco Martens
	 * @version 1.0 Dezember 2012
	 *
	 */
	
	class SearchListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			SudokuFileChooserView s = new SudokuFileChooserView("open");
			_view.setFile(s.getSelectedFileName());
		}
		
	}
	

	/**
	 * <strong>ExitListener</strong>
	 * 
	 * <p>Schlie�t die View</p>
	 */
	
	class ExitListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			_view.setVisible(false);
	        _view.dispose();
		}
		
	}

	
}
