package controller;

import java.sql.SQLException;
import java.util.List;

import javax.swing.JOptionPane;

import model.SudokuModel;
import view.MainView;

import classes.Registry;
import classes.SudokuDatabase;
import exception.SudokuException;

/**
 * <strong>Klasse OpenSudokuDBController</strong>
 * 
 * <p>�ffnet ein Sudoku aus der Datenbank</p>
 * 
 * 
 * 
 * @see		BaseOpenSudokuController
 * @see		OpenSudokuTextController
 * @see		SudokuDatabase
 * @author 	Marco Martens
 * @version 1.0 Dezember 2012
 *
 */

public class OpenSudokuDBController extends BaseOpenSudokuController {
	
	/**
	 * SudokuDatabase 
	 */
	
	private SudokuDatabase sDB = null;
	
	/**
	 * Konstruktor
	 * 
	 * <p>Setzt die View, das Model und l�dt ein Sudoku.</p>
	 * 
	 * @param _mainView
	 * @param _model
	 */
	public OpenSudokuDBController(MainView _mainView, SudokuModel _model) {
		super(_mainView, _model);
		executeAction();
		// TODO Auto-generated constructor stub
	}

	/**
	 * L�dt ein Sudoku in die GUI
	 */
	public void executeAction() {
		try {
			updateSudokuModel(getSudokus());
		} catch (SudokuException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null,e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}
	/**
	 * L�dt ein Sudoku aus der Datenbank
	 * 
	 * @return
	 */
	public List<String> getSudokus() {
		try {
			sDB = new SudokuDatabase();
		} catch (InstantiationException | IllegalAccessException
				| ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			JOptionPane.showMessageDialog(null,e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		
		}
		return sDB.getAllSudokusByType(Registry.get().getInt("size"));
	}
			
	
}
