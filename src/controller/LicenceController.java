package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URISyntaxException;

import javax.swing.JOptionPane;

import view.LicenceView;

/**
 * <strong>Klasse LicenceController</strong>
 * 
 * <p>�ffnet die Lizenz</p>
 * 
 * 
 * @see		LicenceView
 * @author 	Marco Martens
 * @version 1.0 Dezember 2012
 *
 */


public class LicenceController {
	
	/**
	 * LicenceView
	 */
	
	private LicenceView _view;
	
	
	/**
	 * Konstruktor
	 * 
	 * <p>�ffnet den View, setzt die Listener und zeigt die View an.</p>
	 */
	
	public LicenceController () {
		
			try {
				this._view = new LicenceView(null);
			} catch (IOException | URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				JOptionPane.showMessageDialog(null,e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
		
		setListener();
		showView();
	}
	
	/**
	 * Setzt die Listener
	 */
	
	private void setListener () {
		_view.setExitListener(new ExitListener());
	}
	
	/**
	 * Zeigt die View an.
	 */
	
	public void showView() {
		_view.setVisible(true);
	}
	
	/**
	 * <strong>ExitListener</strong>
	 * 
	 * <p>Schlie�t die View</p>
	 */
	
	class ExitListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			_view.setVisible(false);
	        _view.dispose();
		}
		
	}
	

}
