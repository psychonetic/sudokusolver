package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import classes.InstallerDatabase;
import classes.PropertiesHandler;
import classes.Registry;
import view.SettingsView;
import view.SudokuFileChooserView;

/**
 * <strong>Klasse SettingsController</strong>
 * 
 * <p>�ffnet die SettingsView und bietet dem User
 * die M�glichkeit Einstellungen vorzunehmen.
 * Dazu z�hlen folgende Einstellungsm�glichkeiten:</p>
 * 
 * <li>Schnellladedatei</li>
 * <li>Gr��e des Sudokus</li>
 * <li>Datenbank oder mit Textdateien arbeiten</li>
 * <li>Datenbankeinstellungen</li>
 * <li>Installation/Deinstallation der DB-Tabellen</li>
 * <li>Konfiguration der Solver</li>
 * <li>Konfiguration des Generators</li>
 * <li>Benchmarkdatei</li>
 * 
 * 
 * 
 * @see		SettingsView
 * @see		PropertiesHandler
 * @see		Registry
 * @author 	Marco Martens
 * @version 1.0 Dezember 2012
 *
 */


public class SettingsController{

	/**
	 * Schnellladedatei
	 */
	
	private String fastFile;
	
	/**
	 * Sudokufeldgr��e
	 */
	
	private String size ;
	
	/**
	 * Art des Zugriffes
	 */
	
	private String saveType ;
	
	/**
	 * Host 
	 */
	
	private String host;
	
	/**
	 * Datenbank
	 */
	
	private String db ;
	
	/**
	 * User
	 */
	
	private String user ;
	
	/**
	 * Passwort
	 */
	
	private char[] pw ;
	
	/**
	 * showSolve
	 */
	
	private Boolean showSolve;
	
	/**
	 * smartSolve
	 */
	
	private Boolean smartSolve;
	
	/**
	 * hiddenSingle
	 */
	
	private Boolean hiddenSingle;
	
	/**
	 * nakedSingle
	 */
	
	private Boolean nakedSingle;
	
	/**
	 * Generierungsart
	 */
	
	private String generateType;
	
	/**
	 * Schwierigkeitsgrad
	 */
	
	private String grade;
	
	/**
	 * Benchmarkfile
	 */
	
	private String bfile;	
	
	/**
	 * SettingsView
	 */
	
	private SettingsView _view;
	
	/**
	 * Konstruktor
	 * 
	 * <p>L�dt den View, setzt die Listener und zeigt anschlie�end die View an.</p>
	 */
	
	public SettingsController () {
		this._view = new SettingsView(null);
		setListener();
		if(_view.getSaveType()=="text") {
			_view.setDatabaseFields(false);
		}
		showView();
		
	}
	
	/**
	 * Setzt die Listener 
	 */
	
	private void setListener() {
		_view.setExitListener(new ExitListener());
		_view.setResetListener(new ResetListener());
		_view.setInstallTablesListener(new InstallListener());
		_view.setDeinstallTablesListener(new DeinstallListener());
		_view.setSaveListener(new SaveListener());
		_view.setBenchmarkFileListener(new BenchmarkFileListener());
		_view.setsearchFastFileListener(new FastFileListener());
		_view.setTypeListener(new TypeListener());
		_view.setGenerateTypeListener(new HellListener());
		_view.setWarningGradeListener(new WarningGradeListener());
		
	}
	
	/**
	 * Zeigt den View an
	 */
	
	public void showView() {
		_view.setVisible(true);
	}
	
	/**
	 * Gibt die Daten aus dem View zur�ck
	 */
	
	private void getData () {
		fastFile = _view.getSudokuFile();
		size = _view.getSudokuSize();
		saveType = _view.getSaveType();
		host = _view.getHost();
		db = _view.getDatabase();
		user = _view.getUser();
		pw = _view.getPassword();
		showSolve = _view.isSelectShowSolve();
		smartSolve = _view.isSelectSmartSolve();
		hiddenSingle = _view.isSelectHiddenSingle();
		nakedSingle = _view.isSelectNakedSingle();
		generateType = _view.getGeneratorType();
		grade = _view.getGrade();
		bfile = _view.getBenchmarkFile();
	}
	
	
	/**
	 * Speichert die Daten
	 * @throws IOException
	 */
	
	private void saveData() throws IOException {
		PropertiesHandler p = new PropertiesHandler("config/config.txt");
		
		p.setConfigItem("fastfile", fastFile);
		p.setConfigItem("size", size);
		p.setConfigItem("savetype", saveType);
		p.setConfigItem("host", host);
		p.setConfigItem("db", db);
		p.setConfigItem("user", user);
		p.setConfigItem("pw", String.valueOf(pw));
		p.setConfigItem("showsolve", String.valueOf(showSolve));
		p.setConfigItem("smartsolve", String.valueOf(smartSolve));
		p.setConfigItem("hiddensingle", String.valueOf(hiddenSingle));
		p.setConfigItem("nakedsingle", String.valueOf(nakedSingle));
		p.setConfigItem("generatetype", generateType);
		p.setConfigItem("grade", grade);
		p.setConfigItem("bfile", bfile);
		if(_view.getSaveType()=="db") {
			
			if (user.equals("")  || host.equals("") || db.equals("")) {
				p.setConfigItem("savetype", "text");
			}
			
		}
		p.writeProperty("Einstellungen");
		
	}
	
	
	/**
	 * <strong>Klasse SaveListener</strong>
	 * 
	 * <p>Speichert die Daten und zeigt bei Erfolg eine Meldung an.</p>
	 * 
	 * @author 	Marco Martens
	 * @version 1.0 Dezember 2012>
	 *
	 */
	
	class SaveListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			//Daten holen
			getData();
			try {
				saveData();
				JOptionPane.showMessageDialog(null,"Ihre Einstellungen wurden erfolgreich gespeichert!" , "Information", JOptionPane.INFORMATION_MESSAGE);
				_view.setVisible(false);
		        _view.dispose();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				JOptionPane.showMessageDialog(null,e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				e.printStackTrace();
			}
		}
		
	}
	
	/**
	 * <strong>ExitListener</strong>
	 * 
	 * <p>Schlie�t die View</p>
	 */
	
	class ExitListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			_view.setVisible(false);
            _view.dispose();
		}
		
	}
	
	/**
	 * <strong>Klasse ResetListener</strong>
	 * 
	 * <p>Setzt die Einstellungen zur�ck auf den Standard.</p>
	 * 
	 * 
	 * @author 	Marco Martens
	 * @version 1.0 Dezember 2012
	 *
	 */
	
	class ResetListener  implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			File config = new File("config/config.txt");
			File defaultConfig = null;
			try {
				defaultConfig = new File(getClass().getClassLoader().getResource("ressource/config.txt").toURI());
			} catch (URISyntaxException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			BufferedReader reader = null;
			BufferedWriter writer = null;
			String line = null;		
			
						try {
							reader = new BufferedReader( new FileReader(defaultConfig));
						} catch (FileNotFoundException e1) {
							// TODO Auto-generated catch block
							JOptionPane.showMessageDialog(null,e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
							e1.printStackTrace();
						}
						try {
							writer = new BufferedWriter( new FileWriter(config, false)); 
							while ((line = reader.readLine()) != null) {
									
									writer.write(line);
									writer.write(System.getProperty("line.separator"));
									writer.flush();	
									
							}
							writer.close();	
							reader.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							JOptionPane.showMessageDialog(null,e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
							e.printStackTrace();
						} 
						
				JOptionPane.showMessageDialog(null,"Die Standardeinstellungen wurden geladen!" , "Information", JOptionPane.INFORMATION_MESSAGE);
				_view.setVisible(false);
				_view.dispose();
		}
		
	}
	
	/**
	 * <strong>Klasse InstallListener</strong>
	 * 
	 * <p>Bietet dem User die M�glichkeit die Tabellen zu installieren</p>
	 * 
	 * 
	 * @author 	Marco Martens
	 * @version 1.0 Dezember 2012
	 *
	 */
	
	class InstallListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			if (Registry.get().getString("db").equals("") || Registry.get().getString("host").equals("") || Registry.get().getString("user").equals("") || Registry.get().getString("savetype").equals("text")) {
				try {
					throw new IllegalAccessException("Sie m�ssen erst die Datenbankeinstellungen vornehmen.");
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(null,e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				}
			} else {
				InstallerDatabase iDB = null;
				
				try {
					iDB = new InstallerDatabase();
				} catch (InstantiationException | IllegalAccessException
						| ClassNotFoundException | SQLException e) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(null,e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				}
			
			
				try {
					iDB.install();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(null,e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				}
			}
			
			
			
			
		}
	}
	
	/**
	 * <strong>Klasse DeinstallListener</strong>
	 * 
	 * <p>Bietet dem User die M�glichkeit alle Tabellen aus der Datenbank zu l�schen,
	 * die bei der Installation erstellt wurden.</p>
	 * 
	 * <p><strong>Achtung: Alle vorhandenen Datens�tze in den Tabellen werden unwiederuflich mitgel�scht!</strong></p>
	 * 
	 * 
	 * @author 	Marco Martens
	 * @version 1.0 Dezember 2012
	 *
	 */
	
	class DeinstallListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			if (Registry.get().getString("db").equals("") || Registry.get().getString("host").equals("")|| Registry.get().getString("user").equals("") || Registry.get().getString("savetype").equals("text")) {
				try {
					throw new IllegalAccessException("Sie m�ssen erst die Datenbankeinstellungen vornehmen.");
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(null,e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				}
			} else {
				InstallerDatabase iDB = null;
				
				try {
					iDB = new InstallerDatabase();
				} catch (InstantiationException | IllegalAccessException
						| ClassNotFoundException | SQLException e) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(null,e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				}
			
		
				try {
					iDB.deinstall();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(null,e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				}
			}
			
			
			
			
		}
	}
	/**
	 * <strong>Klasse FastFileListener</strong>
	 * 
	 * <p>�ffnet ein Durchsuchendialog um die Schnellladedatei auszuw�hlen</p>
	 * 
	 * 
	 * @see		SudokuFileChooserView
	 * @author 	Marco Martens
	 * @version 1.0 Dezember 2012
	 *
	 */
	
	class FastFileListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			//Open FileChooser
			//GetSelectedFile
			//UpdateView
			SudokuFileChooserView s = new SudokuFileChooserView("open");
			_view.setSudokuFile(s.getSelectedFileName());
		}
	}
	/**
	 * <strong>Klasse BenchmarkFileListener</strong>
	 * 
	 * <p>�ffnet ein Durchsuchendialog um die Benchmarkdatei auszuw�hlen</p>
	 * 
	 * 
	 * @see		SudokuFileChooserView
	 * @author 	Marco Martens
	 * @version 1.0 Dezember 2012
	 *
	 */
	class BenchmarkFileListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			SudokuFileChooserView sv = new SudokuFileChooserView("open");
			_view.setBenchmarkFile(sv.getSelectedFileName());
			
		}
	}
	
	/**
	 * <strong>Klasse TypeListener</strong>
	 * 
	 * <p>Verbietet das bearbeiten der Datenbankfelder, wenn
	 * der User Text als Einstellungen gew�hlt hat.</p>
	 * 
	 *
	 * @author 	Marco Martens
	 * @version 1.0 Dezember 2012
	 *
	 */
	
	class TypeListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			if(_view.getSaveType()=="text") {
				_view.setDatabaseFields(false);
			} else {
				_view.setDatabaseFields(true);
			}
		}
		
	}
	
	/**
	 * <strong>Klasse HellListener</strong>
	 * 
	 * <p>Verbietet dem User Hellsudokus mit dem HumanRemover zu erstellen
	 * da dies in der aktuellen Version nicht m�glich ist, da f�r Hellsudokus
	 * mehr Algorithmen ben�tigt werden..</p>
	 * 
	 * 
	 * @author 	Marco Martens
	 * @version 1.0 Dezember 2012
	 *
	 */
	
	class HellListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			if(_view.getGeneratorType()=="human") {
				_view.setGrade("hell", false);
				_view.setGrade("normal");
			}
			if(_view.getGeneratorType()=="backtracking") {
				_view.setGrade("hell", true);
			}
		}
	}
	
	/**
	 * <strong>Klasse WarningGradeListener</strong>
	 * 
	 * <p>Falls der User ein Hellsudoku erstellen m�chte, wird
	 * er darauf hingewiesen, dass das Erstellen eines Hellsudokus
	 * unter Umst�nden sehr lange dauern kann.</p>
	 * 
	 * @author 	Marco Martens
	 * @version 1.0 Dezember 2012
	 *
	 */
	
	class WarningGradeListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			if(_view.getGrade()=="hell") {
				//Austauschen durch Best�tidungsfenster
			int check =	JOptionPane.showConfirmDialog(null,"Das Erstellen eines Sudokus mit dem Schwierigkeitsgrad H�lle kann unter Umst�nden sehr viel Zeit Anspruch nehmen. Bitte haben Sie bei der Generierung des Sudokus Geduld.", "Warnung", JOptionPane.YES_NO_CANCEL_OPTION);
			if(check != 0 ) {
				_view.setGrade("normal");
			}
			}
		}
		
	}
	
	

}
