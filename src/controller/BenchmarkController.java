package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import exception.SudokuException;

import view.BenchView;
import view.StartBenchmarkView;

/**
 * <strong>Klasse BenchmarkController</strong>
 * 
 * <p>�ffnet die BenchmarkView und startet den Benchmark</p>
 * 
 * @see 	BenchView
 * @see		StartBenchmarkView
 * @author 	Marco Martens
 * @version 1.0 Dezember 2012
 *
 */

public class BenchmarkController {
	
	/**
	 * View
	 */
	private StartBenchmarkView _sbView;
	
	/**
	 * Konstruktor
	 * 
	 * <p>�ffnet die View, setzt die Listener und zeigt die View an.</p>
	 */
	
	public BenchmarkController () {
		try {
			_sbView = new StartBenchmarkView(null);
		} catch (IOException | SudokuException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null,e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
		setListener();
		showView();
	}

	/**
	 * Zeigt die View an
	 */
	
	private void showView() {
		_sbView.setVisible(true);
	}

	/**
	 * Setzt die Listener
	 */
	
	private void setListener() {
		_sbView.setStartListener(new StartListener());
	}
	
	/**
	 * <strong>Klasse StartListener</strong>
	 * 
	 * <p>Startet den eigentlichen Benchmark und zeigt danach dessen Ergebnisse
	 * im View an.</p>
	 * 
	 * @see 	BenchView
	 * @author 	Marco Martens
	 * @version 1.0 Dezember 2012
	 *
	 */
	
	class StartListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			try {
				@SuppressWarnings("unused")
				BenchView bView = new BenchView(null, _sbView);
			} catch (InstantiationException | IllegalAccessException
					| ClassNotFoundException | SQLException | IOException
					| SudokuException e) {
				// TODO Auto-generated catch block
				JOptionPane.showMessageDialog(null,e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				e.printStackTrace();
			}
		}
		
	}
	
	
	
	
}
