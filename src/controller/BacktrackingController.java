package controller;
import javax.swing.JOptionPane;

import view.MainView;
import model.SudokuModel;
import classes.BacktrackSolver;
import classes.Registry;

/**
 * <strong>Klasse BacktrackingController</strong>
 * 
 * <p>L�st das Sudoku mit dem Backtracking.
 * Dabei erfolgt das L�sen des Sudokus als eigener Thread,
 * da sonst das L�sen weiterlaufen w�rde, obwohl man schon ein 
 * neues Sudoku angefangen hat.</p>
 * 
 * 
 * @see		BacktrackSolver
 * @author 	Marco Martens
 * @version 1.0 Dezember 2012
 *
 */


public class BacktrackingController extends Thread {

	/**
	 * View
	 */
	
	private MainView _view;
	
	/**
	 * Model
	 */
	
	private SudokuModel _model;
	
	/**
	 * L�sungzeit
	 */
	
	private long solveTime;
	
	/**
	 * Versuche zum L�sen
	 */
	
	private long backtracks;
	
	/**
	 * Aktuelle Zeit
	 */
	
	private long timenow;
	
	/**
	 * Zeit nachdem L�sen
	 */
	
	private long timeafter;
	
	/**
	 * Konstruktor
	 * 
	 * <p>Setzt das Model, den View und startet den Thread.</p>
	 * 
	 * @param _model
	 * @param _view
	 */
	
	public BacktrackingController (SudokuModel _model, MainView _view) {
		this._view = _view;
		this._model = _model;
		start();
	}
	
	
	/**
	 * Startet den Thread
	 */
	
	@Override
	public void run () {
		executeAction(); 
		Registry.get().set("backtrackingcounter", Registry.get().getInt("backtrackingcounter")+1);
		Registry.get().set("lasttime", this.solveTime );
		Registry.get().set("lastbacktracks", this.backtracks);
		Registry.get().set("time", Registry.get().getLong("time")+ this.solveTime);
		Registry.get().set("backtracks", Registry.get().getLong("backtracks") + this.backtracks);
	}
	
	/**
	 * L�st das Sudoku mit dem Backtracking
	 * 
	 * @see BacktrackingController
	 */
	
	private void executeAction() {
		// TODO Auto-generated method stub
		//Keine weiteren Methoden
		//Model holen
		int[][] sudoku = new int[_model.getRowCount()][_model.getColumnCount()];
		sudoku = _model.SudokuModeltoArray();
		final BacktrackSolver solver = new BacktrackSolver(sudoku, _model);
		if(solver.isSolved()==true) {
			JOptionPane.showMessageDialog(null,"Das Sudoku ist bereits gel�st!", "Information", JOptionPane.INFORMATION_MESSAGE);
		} else {
			
			if(Registry.get().getString("showsolve").equals("false")) {
				
				if(Registry.get().getString("smartsolve").equals("true")) {
					timenow = System.currentTimeMillis();
					solver.smartSolve(0, 0);	
					timeafter = System.currentTimeMillis();
					solveTime = (timeafter - timenow); 
					
				} else {	
					timenow = System.currentTimeMillis();
					solver.solve(0, 0);	
					timeafter = System.currentTimeMillis();
					solveTime = (timeafter - timenow); 
				
				}
				for (int frow = 0; frow < sudoku.length; frow++) {
					for (int fcol = 0; fcol < sudoku[0].length; fcol++) {
					int value = sudoku[frow][fcol];
						_model.setValueAt(value, frow, fcol);
					}
				}
				
			} else {
					
				if(Registry.get().getString("smartsolve").equals("true")) {
					timenow = System.currentTimeMillis();
					solver.smartShowSolve(0, 0);
					timeafter = System.currentTimeMillis();
					solveTime = (timeafter - timenow); 
					
					
				} else {
					timenow = System.currentTimeMillis();
					solver.showSolve(0, 0);
					timeafter = System.currentTimeMillis();
					solveTime = (timeafter - timenow); 
				
				}
				
			}
			this.backtracks = solver.getBacktracks();
			
			_view.setBacktracks("Anzahl der Versuche: " + backtracks);
			_view.setTime("Ben�tigte Zeit: " + solveTime + " Millisekunden"); 
			_view.setEmptyCells("Noch freie Felder: 0");
		}
			
	}
}
