package controller;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import model.SudokuModel;

import classes.Registry;
import classes.SudokuHandler;

import view.MainView;
import view.SudokuFileChooserView;

/**
 * <strong>Klasse OpenSudokuController</strong>
 * 
 * <p>�ffnet ein Sudoku aus einer Textdatei.</p>
 * 
 * 
 * 
 * @see		BaseOpenSudokuController
 * @see		OpenSudokuDBController
 * @see		SudokuHandler
 * @see 	SudokuFileChooserView
 * @author 	Marco Martens
 * @version 1.0 Dezember 2012
 */

public class OpenSudokuTextController extends BaseOpenSudokuController {
	
	/**
	 * SudokuFileChooserView
	 */
	
	private SudokuFileChooserView _view;
	
	
	/**
	 * Datei
	 */
	
	private String filename = null;
	
	
	/**
	 * Konstruktor
	 * 
	 * <p>L�dt die View und erm�glicht es dem User eine Datei auszuw�hlen
	 * und ein zuf�lliges Sudoku daraus zu laden</p>
	 * 
	 * @param _Mainview
	 * @param _model
	 * @param showDialog
	 */
	public OpenSudokuTextController (MainView _mainView, SudokuModel _model, Boolean showDialog) {
		super(_mainView, _model);
		if (showDialog == true) {
			_view = new SudokuFileChooserView("open");
			executeAction();
		} else {
			filename = Registry.get().getString("fastfile");
		}
		_mainView.resetView();
		
		try {
			if(this.filename != null ) {
			updateSudokuModel(getSudokus());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null,e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}
	
	/**
	 * Setzt die Datei
	 */
	
	public void executeAction() {
		if (_view.getReturnValue()==JFileChooser.APPROVE_OPTION) {
			this.filename = _view.getSelectedFileName();
		}
	}
	
	/**
	 * L�dt alle Sudokus aus der Datei
	 * 
	 * @return sudokus
	 */
	
	private List<String> getSudokus() {
		List<String> sudokus = new ArrayList<String>();
			SudokuHandler su = new SudokuHandler();
			try {
				sudokus = su.readSudoku(new File(filename));
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				JOptionPane.showMessageDialog(null,e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				e1.printStackTrace();
			}
		
		return sudokus;
	}
	
	
		

	
}
