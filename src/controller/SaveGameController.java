package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import model.SudokuModel;

import classes.Registry;
import classes.SavegameHandlerDatabase;
import classes.SavegameHandler;

import view.MainView;
import view.NewSaveGameView;

/**
 * <strong>Klasse SaveGameController</strong>
 * 
 * <p>Erm�glicht es ein Savegame zu speichern</p>
 * 
 * @see	NewSaveGameView
 * @see	SavegameHandler
 * @see	SavegameHandlerDatabase
 * 
 * @author 	Marco Martens
 * @version 1.0 Dezember 2012
 *
 */

public class SaveGameController {

	/**
	 * NewSaveGameView
	 */
	
	private NewSaveGameView _view;
	
	/**
	 * SudokuModel
	 */
	
	private SudokuModel _model;
	
	/**
	 * Konstruktor
	 * 
	 * <p>Legt die View, das Model und Listener fest und zeigt anschlie�end die View an.</p>
	 * 
	 * @param _MainView
	 * @param _model
	 */
	
	public SaveGameController (MainView _MainView, SudokuModel _model) {
		this._view = new NewSaveGameView(null);
		this._model = _model;
		setListener();
		showView();
		
	}
	
	/**
	 * Setzt die Listener
	 */
	
	private void setListener() {
		_view.setExitListener(new ExitListener());
		_view.setSaveListener(new SaveListener());
	}
	
	/**
	 * Zeigt die View an
	 */
	
	public void showView () {
		this._view.setVisible(true);
	}
	
	/**
	 * <strong>ExitListener</strong>
	 * 
	 * <p>Schlie�t die View</p>
	 */
	
	class ExitListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			_view.setVisible(false);
            _view.dispose();
		}
		
	}
	
	/**
	 * <strong>Klasse SaveListener</strong>
	 * 
	 * <p>Speicher das Sudoku</p>
	 * 
	 * @see		SavegameHandler
	 * @author 	Marco Martens
	 * @version 1.0 Dezember 2012
	 *
	 */
	
	class SaveListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			//Name f�r das Savegame
			String text = _view.getSaveGame();
			SavegameHandler game = new SavegameHandler();
			
			
			
			//Sudoku?^^
			String sudoku = "";
			for (int i = 0; i < _model.getRowCount(); i++) {
				for (int j = 0; j < _model.getColumnCount(); j++) {
					if (_model.getValueAt(i, j) == null) {
						sudoku += "0"; 
					} else {
							sudoku += _model.getValueAt(i, j);
						}
						
						
					}
			}
			if(Registry.get().getString("savetype").equals("db")) {
				SavegameHandlerDatabase sDB = null;
				try {
					sDB = new SavegameHandlerDatabase();
				} catch (InstantiationException | IllegalAccessException
						| ClassNotFoundException | SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					JOptionPane.showMessageDialog(null,e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				}
				sDB.saveGame(sudoku, text);
			}
			if(Registry.get().getString("savetype").equals("text")) {
				try {
					game.saveGame(new File("config/save.txt"), sudoku, text);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(null,e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					e1.printStackTrace();
				}
			}
			_view.setVisible(false);
			_view.dispose();
			
		}
		
	}
}
