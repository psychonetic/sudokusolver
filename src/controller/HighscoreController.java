package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.xml.stream.XMLStreamException;

import model.HighscoreModel;

import classes.HighscoreDatabase;
import classes.HighscoreXML;
import classes.InstallerDatabase;
import classes.Registry;

import view.HighscoreView;

/**
 * <strong>Klasse HighscoreController</strong>
 * 
 * <p>Speichert oder �ffnet die Highscores.</p>
 * 
 * 
 * @see		HighscoreView
 * @see 	HighscoreDatabase
 * @see		HighscoreXML
 * @author 	Marco Martens
 * @version 1.0 Dezember 2012
 *
 */
public class HighscoreController {

	/**
	 * View
	 */
	private HighscoreView _view;
	
	/**
	 * Model
	 */
	
	private DefaultTableModel _model;
	
	/**
	 * HighscoreDatabase
	 */
	
	private HighscoreDatabase hDB;
	
	
	/**
	 * Highscores
	 */
	private List<HighscoreModel> highscores = new ArrayList<HighscoreModel>();
	
	/**
	 * Konstruktor
	 * 
	 * <p>L�dt die View, setzt die Listener und l�dt die Highscores. Anschlie�end wird die View angezeigt.</p>
	 */
	
	public HighscoreController() {
		_model = new DefaultTableModel(10,3);
		
		
		try {
			_view = new HighscoreView(null, _model);
		} catch (InstantiationException | IllegalAccessException
				| ClassNotFoundException | XMLStreamException | IOException
				| SQLException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null,e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
		setListener();
		loadHighscores();
		showView();
	}
	
	/**
	 * Setzt die Listener
	 */
	
	private void setListener() {
		_view.setResetListener(new ResetListener());
		_view.setExitListener(new ExitListener());
		_view.setDropDownListener(new DropDownListener());
	}
	
	/**
	 * Zeigt die View an
	 */
	
	private void showView() {
		_view.setVisible(true);
	}
	
	/**
	 * L�dt die Highscores 
	 * 
	 */
	
	private void loadHighscores() {
		if(Registry.get().getString("savetype").equals("db")) {
			try {
				hDB = new HighscoreDatabase();
			} catch (InstantiationException | IllegalAccessException
					| ClassNotFoundException | SQLException e1) {
				// TODO Auto-generated catch block
				JOptionPane.showMessageDialog(null,e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				e1.printStackTrace();
			}
			try {
				hDB.connect();
			} catch (InstantiationException | IllegalAccessException
					| ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				JOptionPane.showMessageDialog(null,e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				e.printStackTrace();
				
			}
			highscores = hDB.getAllHighscores();
		}
		if(Registry.get().getString("savetype").equals("text")) {
			//Alle Sudokus holen
			HighscoreXML hXML = new HighscoreXML("config/highscore.xml");
			try {
				highscores = hXML.getHighscores();
			} catch (XMLStreamException | IOException e) {
				// TODO Auto-generated catch block
				JOptionPane.showMessageDialog(null,e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				e.printStackTrace();
			}
		}
		
	}
	
	/**
	 * <strong>Klasse DropDownLister</strong>
	 * 
	 * <p>Stellt die Highscores in einer DropDown-Liste dar.</p>
	 * 
	 * 
	 * 
	 * @author 	Marco Martens
	 * @version 1.0 Dezember 2012
	 *
	 */
	class DropDownListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			//getSelectedItem
			//Suche nach Highscore in DB
			//�ffne es
			
			Object sudoku = _view.getSelectedItem();
			_view.resetView();
			int place = 0;
			for (int h = 0; h < highscores.size(); h++) {
				if (highscores.get(h).getSudoku().equals(sudoku)) {
					if(h==10) {
						break;
					}
					_model.setValueAt(highscores.get(h).getName(), place, 1);
					_model.setValueAt(highscores.get(h).getTime(), place, 2);
					place++;
				}
				
			}
			
			
		}
		
	}
	
	/**
	 * <strong>Klasse ResetListener</strong>
	 * 
	 * <p>L�scht alles Highscores.</p>
	 * 
	 * 
	 * @author 	Marco Martens
	 * @version 1.0 Dezember 2012
	 *
	 */
	
	class ResetListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			
			//SQLHandler s = new SQLHandler();
			//s.installTables();
			//s.deinstallTables();
			if(Registry.get().getString("savetype").equals("text")) {
				File config = new File("config/highscore.xml");
				File defaultConfig = null;
				try {
					defaultConfig = new File(getClass().getClassLoader().getResource("ressource/highscore.xml").toURI());
				} catch (URISyntaxException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				BufferedReader reader = null;
				BufferedWriter writer = null;
				String line = null;		
							try {
								reader = new BufferedReader( new FileReader(defaultConfig));
							} catch (FileNotFoundException e1) {
								// TODO Auto-generated catch block
								JOptionPane.showMessageDialog(null,e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
								e1.printStackTrace();
							}
							try {
								writer = new BufferedWriter( new FileWriter(config, false)); 
								while ((line = reader.readLine()) != null) {
										
										writer.write(line);
										writer.write(System.getProperty("line.separator"));
										writer.flush();	
										
								}
								writer.close();	
								reader.close();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								JOptionPane.showMessageDialog(null,e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
								e.printStackTrace();
							} 
							
					
			}
			if(Registry.get().getString("savetype").equals("db")) {
			InstallerDatabase db = null;
			try {
				db = new InstallerDatabase();
				db.deinstall();
				db.install();
			} catch (InstantiationException | IllegalAccessException
					| ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				JOptionPane.showMessageDialog(null, e.getMessage() , "Error", JOptionPane.ERROR_MESSAGE);
				e.printStackTrace();
			}
			
			}
			JOptionPane.showMessageDialog(null,"Die Highscores wurden zur�ckgesetzt!" , "Information", JOptionPane.CANCEL_OPTION);
			_view.setVisible(false);
			_view.dispose();
			}
		
		
		
	}
	
	/**
	 * <strong>ExitListener</strong>
	 * 
	 * <p>Schlie�t die View</p>
	 */
	
	class ExitListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			_view.setVisible(false);
            _view.dispose();
           
		}
		
	}
}
