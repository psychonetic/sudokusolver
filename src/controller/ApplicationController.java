package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;

import javax.swing.JOptionPane;

import classes.PropertiesHandler;
import classes.Registry;
import classes.SudokuFieldListener;
import classes.Timer;
import model.SudokuModel;

import view.MainView;

/**
 * <strong>Klasse ApplicationController</strong>
 * 
 * <p>Diese Klasse ist der FrontController
 * und l�dt leitet alle weiteren Aktionen an andere Controller weiter
 * die ihre eigenen Views und teilweise ihre Models laden.</p>
 * 
 * @see controller.AboutController
 * @see controller.BacktrackingController
 * @see controller.BenchmarkController
 * @see controller.GeneratorController
 * @see controller.HighscoreController
 * @see controller.HumanController
 * @see controller.LicenceController
 * @see controller.LoadSaveGameController
 * @see controller.OpenSudokuTextController
 * @see controller.OpenSudokuDBController
 * @see controller.SaveGameController
 * @see controller.SaveSudokuTxtController
 * @see controller.SettingsController
 * @see controller.StatisticController
 * @see controller.TippController
 * @see controller.TransferController
 * 
 * @see view.MainView
 * @see classes.Timer
 * @see models.SudokuModel
 * @see classes.Registry
 * 
 * @author 	Marco Martens
 * @version 1.0 Oktober 2012
 *
 */

public class ApplicationController {

	/**
	 * MainView
	 */
	
	private MainView _view;
	
	/**
	 * SudokuModel
	 */
	
	private SudokuModel _model;
	
	/**
	 * Timer
	 */
	
	private Timer timer;
	
	/**
	 * Registry
	 */
	
	public Registry reg;
	
	/**
	 * AboutController
	 */
	
	@SuppressWarnings("unused")
	private AboutController aboutController;
	
	/**
	 * BacktrackingContoller
	 */
	
	private BacktrackingController backtrackingController;
	
	/**
	 * BenchmarkController
	 */
	
	@SuppressWarnings("unused")
	private BenchmarkController benchmarkController;
	
	/**
	 * GeneratorController
	 */
	
	@SuppressWarnings("unused")
	private GeneratorController generatorController;
	
	/**
	 * HighscoreController
	 */
	
	@SuppressWarnings("unused")
	private HighscoreController highscoreController;
	
	/**
	 * HumanController
	 */
	
	private HumanController humanController;
	
	/**
	 * LoadSaveGameController
	 */
	
	@SuppressWarnings("unused")
	private LoadSaveGameController loadsavegameController;
	
	/**
	 * OpenSudokuTextController
	 */
	
	@SuppressWarnings("unused")
	private OpenSudokuTextController opensudokutextController;
	
	
	/**
	 * OpenSudokuDBController
	 */
	
	@SuppressWarnings("unused")
	private OpenSudokuDBController opensudokudbController;
	
	/**
	 * SaveGameController
	 */
	
	@SuppressWarnings("unused")
	private SaveGameController savegameController;
	
	/**
	 * SaveSudokuTxtController
	 */
	
	@SuppressWarnings("unused")
	private SaveSudokuTxtController savesudokutxtController;
	
	/**
	 * SaveSudokuDBController
	 */
	
	@SuppressWarnings("unused")
	private SaveSudokuDBController savesudokudbController;
	
	/**
	 * SettingsController
	 */
	
	@SuppressWarnings("unused")
	private SettingsController settingsController;
	
	/**
	 * StatisticController
	 */
	
	@SuppressWarnings("unused")
	private StatisticController statisticController;
	
	
	/**
	 * TippController
	 */
	
	@SuppressWarnings("unused")
	private TippController tippController;
	
	/**
	 * TransferController
	 */
	
	@SuppressWarnings("unused")
	private TransferController transferController;
	
	/**
	 * LicenceController
	 */
	
	@SuppressWarnings("unused")
	private LicenceController licenceController;
	
	/**
	 * Konstruktor
	 * 
	 * <p>Der Konstruktor legt alle ben�tigten Dateien an,
	 * die f�r dieses Programm ben�tigt werden.
	 * Dazu z�hlen folgende Dateien:</p>
	 * <li>Konfigurationsdatei</li>
	 * <li>Spielstanddatei</li>
	 * <li>Highscoredatei</li>
	 * 
	 * <p>Weiterhin werden folgende Klassen gestartet:</p>
	 * <li>{@link Registry} 	- Zum Laden und Bereithalten der Einstellungen in der gesamten Application.</li>
	 * <li>{@linkMainView }		- Die Haupview wird geladen, die Listener werden festgelegt und die View wird angezeigt.</li>
	 * <li>{@linkSudokuModel}   - Das SudokuModel wird geladen.</li>
	 * <li>{@linkTimer}			- Der Timer wird instaziert.</li>
	 * 
	 */
	public ApplicationController () {
		
			try {
				createApplicationFiles();
			} catch (IOException | URISyntaxException e) {
				// TODO Auto-generated catch block
				JOptionPane.showMessageDialog(null,e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				e.printStackTrace();
			}
		
		reg = new Registry();
		loadSettings();
		this._view = new MainView();
		this._model = new SudokuModel();
		this._view.setTableModel(_model);
		this.timer = new Timer(this._view);
		
		setListener();
		
	}
	
	/**
	 * Speichert die Statistik
	 * 
	 * @see PropertiesHandler
	 * @see Registry
	 * 
	 */
	private void saveStatistic () {
		PropertiesHandler p = new PropertiesHandler("config/statistic.txt");
		try {
			p.setConfigItem("playedgames",String.valueOf(reg.getInt("playedgames")));
			p.setConfigItem("losegames", String.valueOf(reg.getInt("losegames")));
			p.setConfigItem("wongames", String.valueOf(reg.getInt("wongames")));
			p.setConfigItem("humancounter", String.valueOf(reg.getInt("humancounter")));
			p.setConfigItem("backtrackingcounter", String.valueOf(reg.getInt("backtrackingcounter")));
			p.setConfigItem("backtracks",String.valueOf(reg.getLong("backtracks")));
			p.setConfigItem("lastbacktracks", String.valueOf(reg.getLong("lastbacktracks")));
			p.setConfigItem("benchmarkcounter", String.valueOf(reg.getInt("benchmarkcounter")));
			p.setConfigItem("time", String.valueOf(reg.getLong("time")));
			p.setConfigItem("lasttime", String.valueOf(reg.getLong("lasttime")));
			p.setConfigItem("benchmarks", String.valueOf(reg.getInt("benchmarks")));
			p.setConfigItem("tipps", String.valueOf(reg.getInt("tipps")));
			
			p.writeProperty("Statistik");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null,e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
		
		

	}
	
	/**
	 * L�dt die Einstellungen
	 * 
	 * @see PropertiesHandler
	 * @see Registry
	 */

	private void loadSettings () {
		File configFile = new File("config/config.txt");
		String config = "";
		if (!configFile.exists()) {
			config = getClass().getClassLoader().getResource("bin/ressource/config.txt").toString();
		} else {
			config = "config/config.txt";
		}
		PropertiesHandler p = new PropertiesHandler(config);
		try {
			reg.set("fastfile", p.getConfigItem("fastfile"));
			reg.set("grade", p.getConfigItem("grade"));
			reg.set("smartsolve", p.getConfigItem("smartsolve"));
			reg.set("showsolve",p.getConfigItem("showsolve"));
			reg.set("hiddensingle", p.getConfigItem("hiddensingle"));
			reg.set("nakedsingle", p.getConfigItem("nakedsingle"));
			reg.set("user", p.getConfigItem("user"));
			reg.set("host", p.getConfigItem("host"));
			reg.set("pw", p.getConfigItem("pw"));
			reg.set("db", p.getConfigItem("db"));
			reg.set("size",Integer.parseInt(p.getConfigItem("size")));
			reg.set("savetype", p.getConfigItem("savetype"));
			reg.set("generatetype", p.getConfigItem("generatetype"));
			reg.set("bfile", p.getConfigItem("bfile"));	
		} catch (IOException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null,e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
		File statisticFile = new File("config/statistic.txt");
		String statistic = "";
		if (!statisticFile.exists()) {
			statistic = getClass().getClassLoader().getResource("ressource/statistic.txt").toString();
		} else {
			statistic = "config/statistic.txt";
		}
		
		PropertiesHandler s = new PropertiesHandler(statistic);
		
		//Statistic
		try {
			reg.set("humancounter", Integer.parseInt(s.getConfigItem("humancounter")));
			reg.set("backtrackingcounter", Integer.parseInt(s.getConfigItem("backtrackingcounter")));
			reg.set("playedgames", Integer.parseInt(s.getConfigItem("playedgames")));
			reg.set("losegames", Integer.parseInt(s.getConfigItem("losegames")));
			reg.set("wongames", Integer.parseInt(s.getConfigItem("wongames")));
			reg.set("benchmarkcounter", Integer.parseInt(s.getConfigItem("benchmarkcounter")));
			reg.set("time", Long.valueOf(s.getConfigItem("time")));
			reg.set("backtracks", Long.valueOf(s.getConfigItem("backtracks")));
			reg.set("benchmarks",Integer.parseInt(s.getConfigItem("benchmarks")));
			reg.set("tipps", Integer.parseInt(s.getConfigItem("tipps")));
			reg.set("lastbacktracks", Long.valueOf(s.getConfigItem("lastbacktracks")));
			reg.set("lasttime", Long.valueOf(s.getConfigItem("lasttime")));
			reg.set("size", s.getConfigItem("size"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null,e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
		//_model.fireTableStructureChanged();
		
	}
	
	
	/**
	 * Legt die Listener fest 
	 */
	
	private void setListener () {
		_view.setNewSudokuListener(new NewSudokuListener());
		_view.setExitListener(new ExitListener());
		_view.setOpenSudokuListener(new OpenSudokuListener());
		_view.setFastLoadingListener(new FastLoadingListener());
		_view.setSaveSudokuListener(new SaveSudokuListener());
		_view.setPropertiesListener(new PropertiesListener());
		_view.setSaveListener(new SaveListener());
		_view.setAboutListener(new AboutListener());
		_view.setShowHighscoreListener(new ShowHighscoreListener());
		_view.setShowStatisticListener(new ShowStatisticListener());
		_view.setLoadListener(new LoadListener());
		_view.setGenerateListener(new GenerateListener());
		_view.setTestListener(new TestListener());
		_view.setTippListener(new TippListener());
		_view.setBacktrackingListener(new BacktrackingListener());
		_view.setHumanListener(new HumanListener());
		_view.setSudokuFieldListener(new SudokuFieldListener(_model, _view, timer));
		_view.setTransferListener(new TransferListener());
		_view.setLicenceListener(new LicenceListener());
		_view.setWindow(new WinAdapter());
		
	}
	
	/**
	 * Zeigt die View bzw. die Benutzeroberfl�che an
	 */
	
	public void showView () {
		this._view.setVisible(true);
	}
	/**
	 * Legt alle ben�tigten Dateien an, die f�r dieses Programm ben�tigt werden.
	 * 
	 * <p>Dabei werden teilweise Standardeinstellungen geladen, falls die Konfigurationsdatei
	 * versehentlich gel�scht wurde.</p>
	 * 
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	private void createApplicationFiles () throws IOException, URISyntaxException {
		File configFolder = new File("config");
		File save = new File("config/save.txt");
		File highscore = new File("config/highscore.xml");
		File statistic = new File("config/statistic.txt");
		File settings = new File("config/config.txt");
		
		if (!configFolder.exists()) {
			configFolder.mkdir();
		}
		if(!save.exists()) {
			save.createNewFile();
		}
		
		if(!statistic.exists()) {
			statistic.createNewFile();
			copyFile(getClass().getClassLoader().getResourceAsStream("ressource/statistic.txt"), statistic);
			
		}
		if(!settings.exists()) {
			settings.createNewFile();
			copyFile(getClass().getClassLoader().getResourceAsStream("ressource/config.txt"), settings);
		}
		if(!highscore.exists()) {
			highscore.createNewFile();
			copyFile(getClass().getClassLoader().getResourceAsStream("ressource/highscore.xml"), highscore);
		}
		
	}
	
	/**
	 * 
	 * <strong>Klasse NewSudokuListener</strong>
	 * 
	 * <p>Diese Klasse l�scht die get�tigten Eingaben und setzt alle Felder wieder auf 
	 * Null bzw- auf nicht belegt. So kann der Spieler ein neues Spiel starten. </p>
	 * 
	 * @author 	Marco Martens
	 * @version 1.0 Oktober 2012
	 *
	 */
	class NewSudokuListener implements ActionListener {

		@SuppressWarnings("deprecation")
		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			if(timer.isStarted()==true) {
				timer.setRunning(false);
			}
			_view.resetView();
			_model.setOrgSudoku(null);
			int loseGames = Registry.get().getInt("playedgames") - Registry.get().getInt("wongames");
			Registry.get().set("losegames",loseGames);
			if (backtrackingController != null) {
				backtrackingController.stop();
			}
			
		}
		
	}
	
	/**
	 * <strong>Klasse TransferListener</strong>
	 * 
	 * <p>Startet den TransferController und erlaubt dem Benutzer
	 * den Import/Export der Sudokus</p>
	 * 
	 * 
	 * @author 	Marco Martens
	 * @version 1.0 Dezember 2012
	 *
	 */
	class TransferListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			transferController = new TransferController();
		}
		
	}
	
	/**
	 * <strong>ExitListener</strong>
	 * 
	 * <p>Schlie�t die View</p>
	 */
	
	class ExitListener implements ActionListener {

		@SuppressWarnings("deprecation")
		@Override
		public void actionPerformed(ActionEvent arg0) {
			if(timer.isStarted()==true) {
				timer.setRunning(false);
			}
			
			saveStatistic();
			_view.setVisible(false);
            _view.dispose();
            if (backtrackingController != null) {
				backtrackingController.stop();
			}
            System.exit(0);
		}
		
	}
	
	/**
	 * <strong>Klasse OpenSudokuListener</strong>
	 * 
	 * <p>�ffnet ein neues Sudoku</p>
	 * 
	 * @see 	OpenSudokuTextController
	 * @author 	Marco Martens
	 * @version 1.0 Dezember 2012
	 *
	 */
	
	class OpenSudokuListener implements ActionListener {

		@SuppressWarnings("deprecation")
		@Override
		public void actionPerformed(ActionEvent arg0) {
			if(timer.isStarted()==true) {
				timer.setRunning(false);
				
			}
			if (backtrackingController != null) {
				backtrackingController.stop();
			}
			
			if(Registry.get().getString("savetype").equals("text")) {
				opensudokutextController = new OpenSudokuTextController(_view, _model, true);
			}
			if(Registry.get().getString("savetype").equals("db")) {
				opensudokudbController = new OpenSudokuDBController(_view, _model);
			}
			
			SudokuFieldListener.setFlag(true);
			int loseGames = Registry.get().getInt("playedgames") - Registry.get().getInt("wongames");
			Registry.get().set("losegames",loseGames);
			
		}
		
	}
	
	/**
	 * <strong>Klasse FastLoadingListener</strong>
	 * 
	 * <p>Erm�glicht dem User durch einen Klick oder Tastenk�rzel ein
	 * neues Sudoku festzulegen. Dabei muss der User vorher nur in den Einstellungen ({@link SettingsController})
	 * eine Datei mit Sudokus festlegen.</p>
	 * 
	 * <p>Dies hat den Vorteil, dass der User schnell durch die Sudokus "switchen" kann, falls er schon viele Sudokus gespielt hat.</p>
	 * 
	 * @see 	OpenSudokuTextController
	 * @author 	Marco Martens
	 * @version 1.0 Dezember 2012
	 *
	 */
	
	class FastLoadingListener implements ActionListener {

		@SuppressWarnings("deprecation")
		@Override
		public void actionPerformed(ActionEvent arg0) {
			if(Registry.get().getString("savetype").equals("text") && Registry.get().getString("fastfile").equals("") || Registry.get().getString("savetype").equals("text") &&  Registry.get().getString("fastfile") == null) {
				JOptionPane.showMessageDialog(null,"W�hlen Sie erst eine Datei in den Einstellungen aus f�r das schnelle Laden.", "Information", JOptionPane.INFORMATION_MESSAGE);
			} else {
				if(timer.isStarted()==true) {
					timer.setRunning(false);	
					
				}
				
				if (backtrackingController != null) {
					backtrackingController.stop();
				}
				opensudokutextController = new OpenSudokuTextController(_view, _model, false);
				SudokuFieldListener.setFlag(true);
				int loseGames = Registry.get().getInt("playedgames") - Registry.get().getInt("wongames");
				Registry.get().set("losegames",loseGames);
			}
			
		}
		
	}
	
	/**
	 * <strong>Klasse SaveSudokuLister</strong>
	 * 
	 * <p>Speichert ein Sudoku</p>
	 * 
	 * 
	 * @see SaveSudokuTxtController
	 * 
	 * @author 	Marco Martens
	 * @version 1.0 Dezember 2012
	 *
	 */
	
	class SaveSudokuListener implements ActionListener {

		@SuppressWarnings("deprecation")
		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			if (backtrackingController != null) {
				backtrackingController.stop();
			}
			
			if(Registry.get().getString("savetype").equals("text")) {
				savesudokutxtController = new SaveSudokuTxtController(_model);
			}
			if(Registry.get().getString("savetype").equals("db")) {
				savesudokudbController = new SaveSudokuDBController(_model);
			}
			
			
		}
		
	}
	
	/**
	 * <strong>Klasse PropertiesListener</strong>
	 * 
	 * <p>�ffnet die Einstellungen und erm�glicht es dem User 
	 * diese zu ver�ndern.
	 * 
	 * @see PropertiesController
	 * 
	 * @author 	Marco Martens
	 * @version 1.0 Dezember 2012
	 *
	 */
	
	class PropertiesListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			int oldSize = reg.getInt("size");
			settingsController = new SettingsController();
			loadSettings();
			if(oldSize != reg.getInt("size")) {
				_model.resetModel();
				_model.fireTableStructureChanged();
				_view.revalidate();
				_view.repaint();
			}
			
		}
		
		
		
	}
	
	/**
	 * <strong>Klasse SaveLister</strong>
	 * 
	 * <p>Speichert ein Sudoku.</p>
	 * 
	 * @see 	SaveGameController
	 * 
	 * @author 	Marco Martens
	 * @version 1.0 Dezember 2012
	 *
	 */
	
	class SaveListener implements ActionListener {

		@SuppressWarnings("deprecation")
		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			if (backtrackingController != null) {
				backtrackingController.stop();
			}
			savegameController = new SaveGameController(_view, _model);
		}
		
	}
	
	/**
	 * <strong>Klasse LoadListener</strong>
	 * 
	 * <p>L�dt einen Sudoku-Spielstand.</p>
	 * 
	 * 
	 * @see 	LoadSaveGameController
	 * @author 	Marco Martens
	 * @version 1.0 Dezember 2012
	 *
	 */
	
	class LoadListener implements ActionListener {

		@SuppressWarnings("deprecation")
		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			if(timer.isStarted()==true) {
				timer.setRunning(false);	
				
			}
			if (backtrackingController != null) {
				backtrackingController.stop();
			}
			loadsavegameController = new LoadSaveGameController(_model, _view);
			SudokuFieldListener.setFlag(true);
		}
		
	}
	
	/**
	 * <strong>Klasse AboutListener</strong>
	 * 
	 * <p>�ffnet das �ber-Menu</p>
	 * 
	 * @see 	AboutController
	 * @author 	Marco Martens
	 * @version 1.0 Dezember 2012
	 */
	class AboutListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			aboutController  = new AboutController();
			
		}
		
	}
	
	/**
	 * <strong>Klasse ShowHighScoreListener</p>
	 * 
	 * <p>Zeigt die Highscores an</p>
	 * 
	 * @see		HighscoreController
	 * @author 	Marco Martens
	 * @version 1.0 Dezember 2012
	 *
	 */
	class ShowHighscoreListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			highscoreController = new HighscoreController();
		}
		
	}
	
	/**
	 * <strong>Klasse ShowStatisticListener</strong>
	 * 
	 * <p>Zeigt die Statistik an</p>
	 * 
	 * @see 	StatistikController
	 * @author 	Marco Martens
	 * @version 1.0 Dezember 2012
	 *
	 */
	
	class ShowStatisticListener	implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			statisticController  = new StatisticController();
		}
		
	}
	
	/**
	 * <strong>Klasse GenerateListener/<strong>
	 * 
	 * <p>Startet den SudokuGenerator</p>
	 * 
	 * <p><strong>Information: Das Generieren steht nur f�r 9x9 Sudokus zur Verf�gung.</strong></p>
	 * 
	 * @see		GeneratorController
	 * @author 	Marco Martens
	 * @version 1.0 Dezember 2012
	 *
	 */
	
	class GenerateListener	implements ActionListener {

		@SuppressWarnings("deprecation")
		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			if(timer.isStarted()==true) {
				timer.setRunning(false);	
				
			}
			if (backtrackingController != null) {
				backtrackingController.stop();
			}
			if(Registry.get().getInt("size")!= 9) {
				JOptionPane.showMessageDialog(null,"Diese Funktion ist nur f�r 9x9 Sudokus verf�gbar.", "Information", JOptionPane.INFORMATION_MESSAGE);
				
			} else {
				_view.resetView();
				
				generatorController = new GeneratorController(_model);
				SudokuFieldListener.setFlag(true);
				int loseGames = Registry.get().getInt("playedgames") - Registry.get().getInt("wongames");
				Registry.get().set("losegames",loseGames);
			}
			
		}
		
	}
	
	/**
	 * <strong>Klasse TestListener</strong>
	 * 
	 * <p>Startet das BenchmarkMenu</p>
	 * 
	 * @see		BenchmarkController
	 * @author 	Marco Martens
	 * @version 1.0 Dezember 2012
	 *
	 */
	class TestListener	implements ActionListener {

		@SuppressWarnings("deprecation")
		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			if(Registry.get().getString("bfile").equals("") || Registry.get().getString("bfile") == null) {
				JOptionPane.showMessageDialog(null,"W�hlen Sie erst eine Datei in den Einstellungen aus f�r einen Benchmark.", "Information", JOptionPane.INFORMATION_MESSAGE);
			} else {
				if(timer.isStarted()==true) {
					timer.setRunning(false);	
				}
				if (backtrackingController != null) {
					backtrackingController.stop();
				}
				_view.resetView();
				benchmarkController = new BenchmarkController();
				Registry.get().set("benchmarkcounter", Registry.get().getInt("benchmarkcounter")+1);
			}
			
		}
		
	}
	
	/**
	 * <strong>Klasse TippListener</strong>
	 * 
	 * <p>Zeigt einen Tipp an</p>
	 * 
	 * @see		TippController
	 * @author 	Marco Martens
	 * @version 1.0 Dezember 2012
	 *
	 */
	class TippListener implements ActionListener{

		@SuppressWarnings("deprecation")
		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			if (backtrackingController != null) {
				backtrackingController.stop();
			}
			tippController  = new TippController(_model, _view);
			Registry.get().set("tipps", Registry.get().getInt("tipps")+1);
		}
		
	}
	
	/**
	 * <strong>Klasse HumanListener</strong>
	 * 
	 * <p>Startet den HumanSolver</p>
	 * 
	 * @see		HumanController
	 * @author 	Marco Martens
	 * @version 1.0 Dezember 2012
	 *
	 */
	
	class HumanListener implements ActionListener{

		@SuppressWarnings("deprecation")
		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			//Stoppen
			if(timer.isStarted()==true) {
				timer.setRunning(false);	
		}
			if (backtrackingController != null) {
				backtrackingController.stop();
			}
			humanController  = new HumanController(_model, _view);
			if (humanController.isSolved()) {
				Registry.get().set("humancounter", Registry.get().getInt("humancounter")+1);
				Registry.get().set("lasttime", humanController.getTime() );
			} else {
				timer.resumeTimer();
			}
			
		}
		
	}
	/**
	 * <strong>Klasse BacktrackingListener</strong>
	 * 
	 * <p>Startet den BacktrackingSolver</p>
	 * 
	 * @see		BacktrackingController
	 * @author 	Marco Martens
	 * @version 1.0 Dezember 2012
	 *
	 */
	
	class BacktrackingListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			if(timer.isStarted()==true) {
				timer.setRunning(false);
				
			}
			backtrackingController = new BacktrackingController(_model, _view);
		
		}
		
	}
	
	/**
	 * <strong>Klasse LicenceListener</strong>
	 * 
	 * <p>�ffnet die Lizenz des Programmes</p>
	 * 
	 * @see		LicenceController
	 * @author 	Marco Martens
	 * @version 1.0 Dezember 2012
	 *
	 */
	
	class LicenceListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			licenceController = new LicenceController();
		}
		
	}
	
	/**
	 * <strong>Klasse WinAdapter</strong>
	 * 
	 * <p>Da der User auch �ber den Exit-Button das Programm beenden kann
	 * und nicht nur �ber das Menu, muss sichergestellt werden, dass
	 * alle Statistiken gespeichert werden bevor das Programm geschlossen wird.</p>
	 * 
	 * <p>Der User m�sste sonst jedes mal selber darauf achten die Statistiken zu speichern und kann
	 * sie so auch leicher manupulieren, wenn die Statisk mal etwas schlechter ausf�llt, indem er darauf verzichtet 
	 * sie abzuspeichern</p>
	 * 
	 * @author 	Marco Martens
	 * @version 1.0 Dezember 2012
	 *
	 */
	
	class WinAdapter extends WindowAdapter {
		 public void windowClosing(WindowEvent e) {
			 if(timer.isStarted()==true) {
					timer.setRunning(false);
				}
			 	saveStatistic();
			 	_view.setVisible(false);
			    System.exit(0); 
			  }
	}
	
	/**
	 * 
	 * Kopiert eine Datei
	 * 
	 * 
	 * @param 	inputStream
	 * @param 	target
	 * @throws 	IOException
	 */
	private void copyFile (InputStream inputStream, File target) throws IOException {
		
		BufferedWriter output = new BufferedWriter (new FileWriter(target)); 
		int line;
		while((line = inputStream.read()) != -1) {
			output.write(line);
		}
		inputStream.close();
		output.close();
		
	}
	
	
	
	
	
	
	
	
	
	
	
	

}
