package controller;

import java.sql.SQLException;

import javax.swing.JOptionPane;
import model.SudokuModel;

import classes.Registry;
import classes.SudokuDatabase;

/**
 * <strong>Klasse SaveSudokuController</strong>
 * 
 * <p>Speichert ein Sudoku</p>
 * 
 * 
 * @see		SaveSudokuTxtController
 * @see		SudokuDatabase
 * 
 * @author 	Marco Martens
 * @version 1.0 Dezember 2012
 *
 */

public class SaveSudokuDBController {

	/**
	 * SudokuModel
	 */
	
	private SudokuModel _model;
	
	public SaveSudokuDBController(SudokuModel _model) {
		this._model = _model;
		save();
	}
	
	/**
	 * Gibt das Sudoku als String zur�ck
	 * @return sudoku
	 */
	
	private String getSudoku() {
		return _model.SudokuModeltoString();
	}
	
	
	/**
	 * Speichert das Sudoku in der Datenbank.
	 */
	
	public void save() {
			String sudoku = getSudoku();
			SudokuDatabase sDB;
			try {
				sDB = new SudokuDatabase();
				sDB.addSudoku(sudoku, Registry.get().getInt("size"));
			} catch (InstantiationException | IllegalAccessException
					| ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				JOptionPane.showMessageDialog(null,e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				e.printStackTrace();
			}
			JOptionPane.showMessageDialog(null,"Das Sudoku wurde erfolgreich gespeichert!", "Information", JOptionPane.INFORMATION_MESSAGE);
		}
		
		
	
}
