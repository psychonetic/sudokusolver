package controller;

import javax.swing.JOptionPane;
import view.MainView;
import model.SudokuModel;
import classes.HumanSolver;

/**
 * <strong>Klasse HumanController</strong>
 * 
 * <p>Versucht das Sudoku mit dem HumanSolve zu l�sen.</p>
 * 
 * 
 * @see		HumanSolver
 * @author 	Marco Martens
 * @version 1.0 Dezember 2012
 *
 */

public class HumanController {
	
	/**
	 * MainView
	 */
	
	private MainView _view;
	
	/**
	 * SudokuModel
	 */
	
	private SudokuModel _model;
	
	/**
	 * Zeit zum l�sen
	 */
	private long solveTime;
	
	/**
	 * HumanSolver
	 */
	
	private HumanSolver solver = null; 
	
	/**
	 * Konstruktor
	 * 
	 * <p>Setzt die View und das Model. Danach erfolgt das L�sen des Sudokus</p>
	 * 
	 * @param _model
	 * @param _view
	 */
	
	public HumanController(SudokuModel _model, MainView _view) {
		this._view = _view;
		this._model = _model;
		executeAction();
	}
	
	/**
	 * Gibt die Zeit zum L�sen zur�ck.
	 * 
	 * @return solveTime
	 */
	
	public long getTime() {
		return this.solveTime;
	}
	
	/**
	 * Versucht das Sudoku mit dem HumanSolve zu l�sen. 
	 */
	
	private void executeAction () {
		//Spalte 3/Zeile 6
				//Model holen
				int[][] sudoku = new int[9][9];
				sudoku = _model.SudokuModeltoArray();
				solver = new HumanSolver(sudoku);
				if(solver.isSolved()==true) {
					JOptionPane.showMessageDialog(null,"Das Sudoku ist bereits gel�st!", "Information", JOptionPane.INFORMATION_MESSAGE);
				} else {
					
				long timenow;
				long timebench;
				
				timenow = System.currentTimeMillis();
				//solver.returnSudoku();
				solver.solve();	
				
				
				timebench = System.currentTimeMillis();
				solveTime = (timebench - timenow);
				for (int frow = 0; frow < sudoku.length; frow++) {
					for (int fcol = 0; fcol < sudoku[0].length; fcol++) {
						int value = sudoku[frow][fcol];
						if (value == 0) {
							_model.setValueAt(null, frow, fcol);
						} else {
							_model.setValueAt(value, frow, fcol);
						}
						
					}
				}
					if (solver.isSolved()==false) {
						JOptionPane.showMessageDialog(null,"Das Sudoku konnte nicht gel�st werden!", "Information", JOptionPane.INFORMATION_MESSAGE);
					} else {
						_view.setTime("Ben�tigte Zeit: " + solveTime + " Millisekunden"); 
					}		
				
				
				}
				
				
	}
	
	/**
	 * �berpr�ft ob das Sudoku gel�st wurde
	 * 
	 * @return boolean
	 */
	public boolean isSolved() {
		// TODO Auto-generated method stub
		if(this.solver.isSolved()) {
			return true;
		}
		return false;
	}
}
