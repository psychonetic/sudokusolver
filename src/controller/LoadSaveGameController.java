package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

import javax.swing.JOptionPane;

import classes.Registry;
import classes.SavegameHandlerDatabase;
import classes.SavegameHandler;
import model.SudokuModel;

import view.MainView;
import view.SaveGameView;
/**
 * <strong>Klasse LoadSaveGameController</strong>
 * 
 * <p>�ffnet die Savegames</p>
 * 
 * @see		SaveGameView
 * @see		MainView
 * @see		SavegameHandler
 * @see		SavegameHandlerDatabase
 * @author 	Marco Martens
 * @version 1.0 Dezember 2012
 *
 */
public class LoadSaveGameController {

	/**
	 * SaveGameView
	 */
	
	private SaveGameView _view;
	
	/**
	 * MainView
	 */
	
	private MainView _MainView;
	
	/**
	 * SudokuModel 
	 */
	
	private SudokuModel _model;
	
	
	/**
	 * Sudoku
	 */
	private String sudoku;
	
	
	/**
	 * Konstruktor
	 * 
	 * <p>Legt das Model, die View und die Listener fest. Anschlie�end wird die View angezeigt.</p>
	 * 
	 * @param _model
	 * @param _MainView
	 */
	
	public LoadSaveGameController(SudokuModel _model, MainView _MainView) {
		this._model = _model;
		this._MainView = _MainView;
		try {
			_view = new SaveGameView(null);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null,e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
		setListener();
		showView();
	}

	/**
	 * Zeigt die View an
	 */
	
	private void showView() {
		_view.setVisible(true);
		
	}

	/**
	 * Setzt die Listener
	 */
	
	
	private void setListener() {
		_view.setExitListener(new ExitListener());
		_view.setDeleteListener(new DeleteListener());
		_view.setLoadListener(new LoadListener());
		
	}
	
	/**
	 * <strong>Klasse DeleteListener</strong>
	 * 
	 * <p>L�scht ein Savegame</p>
	 * 
	 * 
	 *  @author 	Marco Martens
	 * 	@version 1.0 Dezember 2012
	 *
	 */
	
	
	class DeleteListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			String savegame = _view.getSavegame();
			System.out.print(savegame);
			if(savegame == null) {
				try {
					throw new IOException("Es wurde kein Sudoku ausgew�hlt.");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(null,e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				}
			} else {
				if(Registry.get().getString("savetype").equals("text")) {
					SavegameHandler  sgame = new SavegameHandler ();
					
						HashMap<String, String> saves = null;
						try {
							saves = sgame.loadGame(new File("config/save.txt"));
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						//Umwandlung zum Model
						String sudoku = saves.get(savegame);
						//System.out.print(sudoku);
						try {
							//System.out.print(savegame + ":" + sudoku);
							sgame.deleteGame(savegame, sudoku);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				}
				if(Registry.get().getString("savetype").equals("db")) {
					
					SavegameHandlerDatabase sDB = null;
					try {
						sDB = new SavegameHandlerDatabase();
						sDB.deleteGame(savegame);
					} catch (InstantiationException | IllegalAccessException
							| ClassNotFoundException | SQLException e) {
						// TODO Auto-generated catch block
						JOptionPane.showMessageDialog(null,e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
						e.printStackTrace();
					}
					
				}
				
					 JOptionPane.showMessageDialog(null,"Der Spielstand wurde erfolgreich gel�scht!", "Information", JOptionPane.INFORMATION_MESSAGE);
					_view.setVisible(false);
		            _view.dispose();
			}
			}
			
			
			
		
	}
	
	/**
	 * <strong>Klasse LoadListener</strong>
	 * 
	 * <p>L�dt einen Spielstand</p>
	 * 
	 * @author 	Marco Martens
	 * @version 1.0 Dezember 2012
	 * 
	 */
	
	
	class LoadListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			//Select zur�ckgeben
			//String von dem Sudoku holen
			//String in Model umwandeln
			//In SudokuTable schreiben
			
			SavegameHandler  sgame = null;
			String savegame = (String) _view.getSavegame();
			
			if(savegame == null) {
				try {
					throw new IOException("Es wurde kein Spielstand ausgew�hlt!");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(null,e.getMessage(),"Error", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				}
			}
			
			if(Registry.get().getString("savetype").equals("text")) {
				 sgame = new SavegameHandler ();
				 try {
					HashMap<String, String> saves = sgame.loadGame(new File("config/save.txt"));
					sudoku = saves.get(savegame);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(null,e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				}
			}
			
			if(Registry.get().getString("savetype").equals("db")) {
				SavegameHandlerDatabase sDB;
				try {
					sDB = new SavegameHandlerDatabase();
					sudoku = sDB.loadGame(savegame);
				} catch (InstantiationException | IllegalAccessException
						| ClassNotFoundException | SQLException e) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(null,e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				}
				
			}
			_MainView.resetView();
				int stringend = sudoku.length();
				//Controller
				int end = 1;
				int start = 0;
						while (end <= stringend) {
						
						for (int row = 0; row < _model.getRowCount(); row++) {
							for (int col = 0; col < _model.getColumnCount(); col++) {
								Integer output = null;
								try {
								output = Integer.parseInt(sudoku.substring(start, end));
								
								}
								catch (NumberFormatException e1) {
									JOptionPane.showMessageDialog(null, e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
								}
								if (output == 0) {
									_model.setValueAt(null, row, col);
								} else {
									_model.setValueAt(output, row, col);
								}
								start++;
								end++;
							}
						}
							
						
					}
			_view.setVisible(false);
            _view.dispose();
		}
		
	}
	
	/**
	 * <strong>ExitListener</strong>
	 * 
	 * <p>Schlie�t die View</p>
	 */
	
	
	class ExitListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			_view.setVisible(false);
            _view.dispose();
		}
		
	}
}
