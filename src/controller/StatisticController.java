package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import classes.Registry;

import model.StatisticModel;
import view.StatisticView;
/**
 * <strong>Klasse StatisticController</strong>
 * 
 * <p>�ffnet die Statistiken oder updatet das Model,
 * indem die Informationen gespeichert sind.</p>
 * 
 * <p>Dazu geh�ren folgende Kriterien:</p>
 * 
 * <li>Letzte L�sungszeit</li>
 * <li>Zeitgesamt</li>
 * <li>Letzte Anzahl an Versuchen</li>
 * <li>Versuchegesamt</li>
 * <li>Anzahl des Backtrackingalgorithmus zur L�sung des Sudokus</li>
 * <li>Anzahl des Humansolvealgorithmus zur L�sung des Sudokus</li>
 * <li>Gespielte Spiele</li>
 * <li>Gewonnene Spiele</li>
 * <li>Verlorene Spiele</li>
 * <li>Anzahl der durchgef�hrten Benchmarks</li>
 * <li>Anzahl der Tipps</li>
 * 
 * @see		StatisticView
 * @see		StatisticModel	
 * @author 	Marco Martens
 * @version 1.0 Dezember 2012
 *
 */
public class StatisticController {

	
	/**
	 * StatisticView
	 */
	
	private StatisticView _view;
	
	/**
	 * StatisticModel
	 */
	
	private StatisticModel _model;
	
	/**
	 * Konstruktor
	 * 
	 * <p>View, Model laden, Listener setzen die Daten im Model updaten
	 * und anschlie�end im View anzeigen.</p>
	 * 
	 * 
	 */
	
	public StatisticController () {
		_view = new StatisticView(null);
		this._model = new StatisticModel();
		setListener();
		updateModel();
		setValues();
		showView();
	}
	
	/**
	 * Model updaten
	 */
	
	public void updateModel() {
		_model.setCountBacktracking(Registry.get().getInt("backtrackingcounter"));
		_model.setBacktracks(Registry.get().getLong("backtracks"));
		_model.setCountHumanSolve(Registry.get().getInt("humancounter"));
		_model.setTime(Registry.get().getLong("time"));
		_model.setLastBacktracks(Registry.get().getLong("lastbacktracks"));
		_model.setLastTime(Registry.get().getLong("lasttime"));
		_model.setPlayedSudokus(Registry.get().getInt("playedgames"));
		_model.setLoseSudokus(Registry.get().getInt("losegames"));
		_model.setWonSudokus(Registry.get().getInt("wongames"));
		_model.setBenchmarks(Registry.get().getInt("benchmarkcounter"));
		_model.setTipps(Registry.get().getInt("tipps"));
	}
	
	/**
	 * Setzt die Werte im View
	 */
	
	public void setValues () {
		_view.setBacktrackingSolve (String.valueOf(_model.getCountBacktracking())); 
		_view.setLastBacktracks( String.valueOf(_model.getLastBacktracks()));
		_view.setBacktracks(String.valueOf(_model.getBacktracks()) );
		_view.setHumanSolve (String.valueOf(_model.getCountHumanSolve())) ;
		_view.setTime(String.valueOf(_model.getTime()) );
		_view.setLastTime(String.valueOf(_model.getLastTime())); 
		_view.setPlayedGames (String.valueOf(_model.getPlayedSudokus()));
		_view.setLoseGames (String.valueOf(_model.getLoseSudokus())) ;
		_view.setWonGames (String.valueOf(_model.getWonSudokus()));
		_view.setBenchmarks(String.valueOf(_model.getBenchmarks()));
		_view.setTipp(String.valueOf(_model.getTipps()));
	}
	
	/**
	 * Setzt die Listener
	 */
	
	private void setListener() {
		_view.setExitListener(new ExitListener());
	}
	
	/**
	 * Zeigt die View an
	 */
	
	private void showView() {
		_view.setVisible(true);
	}
	
	/**
	 * <strong>ExitListener</strong>
	 * 
	 * <p>Schlie�t die View</p>
	 */
	
	class ExitListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			_view.setVisible(false);
            _view.dispose();
		}
		
	}
}
