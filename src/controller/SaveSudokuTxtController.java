package controller;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import model.SudokuModel;
import classes.SudokuHandler;
import view.SudokuFileChooserView;

/**
 * <strong>Klasse SaveSudokuController</strong>
 * 
 * <p>Speichert ein Sudoku</p>
 * 
 * 
 * @see		SudokuFileChooserView
 * @see		SudokuHandler
 * @see		SaveSudokuDBController
 * 
 * @author 	Marco Martens
 * @version 1.0 Dezember 2012
 *
 */

public class SaveSudokuTxtController {

	/**
	 * SudokuFileChooserView 
	 */
	
	private SudokuFileChooserView _view;
	
	/**
	 * SudokuModel
	 */
	
	private SudokuModel _model;
	
	/**
	 * Datei
	 */
	private String file;
	
	
	/**
	 * Konstruktor
	 * 
	 * <p>Legt das Model fest und �ffnet ein Durchsuchendialog
	 * um das Sudoku zu speichern.</p>
	 * 
	 * @param _model
	 */
	
	public SaveSudokuTxtController (SudokuModel _model) {
		this._model = _model;
		_view = new SudokuFileChooserView("save");
		saveSudoku();
	}
	
	/**
	 * Speichert das Sudoku
	 */
	
	private void saveSudoku () {
		
		if (_view.getReturnValue()==JFileChooser.APPROVE_OPTION) {
			file = _view.getSelectedFileName();
			String sudoku = _model.SudokuModeltoString();
			
			
				
				SudokuHandler su = new SudokuHandler();
				try {
					su.writeSudoku(new File(file), sudoku);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(null,e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					e1.printStackTrace();
				}	
			JOptionPane.showMessageDialog(null,"Das Sudoku wurde erfolgreich gespeichert!", "Information", JOptionPane.INFORMATION_MESSAGE);
		}
		
	}
		
	
}
