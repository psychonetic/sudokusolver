package classes;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.HighscoreModel;

/**
 * <strong>Klasse HighscoreDatabase</strong>
 * 
 * <p>Diese Klasse erm�glicht es �ber eine MySQL-Datenbank
 * auf die Highscores zuzugreifen, sie zu speichern und sie zu l�schen.</p>
 * 
 * @see 	HighscoreXML
 * @see 	SQLWrapper
 * @author 	Marco Martens
 * @version 1.0 Oktober 2012
 *
 */
public class HighscoreDatabase extends SQLWrapper {
	/**
	 * NamenID
	 */
	private int nameID;
	/**
	 * SudokuID
	 */
	private int sudokuID;
	/**
	 * TypeID
	 */
	private int typeID;
	
	
	
	/**
	 * Konstruktor
	 * 
	 * <p>Ruft den Konstuktor von {@link SQLWrapper} auf und verbindet sich mit 
	 * der Datenbank und f�gt einen neuen Highscore der Datenbank hinzu.</p>
	 * 
	 * @param name
	 * @param sudoku
	 * @param type
	 * @param time
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public HighscoreDatabase (String name, String sudoku, int type, long time) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		super();
		addHighscore(name, sudoku, type, time);
	}
	/**
	 * Konstruktor
	 * 
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public HighscoreDatabase() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		super();
	}
	
	/**
	 * Legt einen neuen Highscore an.
	 * 
	 * 
	 * @param name
	 * @param sudoku
	 * @param type
	 * @param time
	 */
	public void addHighscore (String name, String sudoku, int type, long time) {
		try {
			
			//Name
			PreparedStatement preState = conn.prepareStatement("SELECT id FROM name WHERE name =?");
			preState.setString(1, name);
			ResultSet result = preState.executeQuery();
			//Datensatz gefunden
			
			if (result.next()  ) {
				this.nameID = result.getInt("id");
				
			} else {
				//Sonst Datensatz hinzuf�gen
				preState = conn.prepareStatement("INSERT INTO name(name) VALUES (?)",Statement.RETURN_GENERATED_KEYS);
				preState.setString(1, name);
				preState.executeUpdate();
				
				ResultSet generatedKeys = preState.getGeneratedKeys();
				if (generatedKeys.next()) {
					this.nameID = generatedKeys.getInt(1);
					System.out.print("NameID: " + this.nameID);
				}
				//Letzte ID
				
			}
			//type
			//Sudoku
			preState = conn.prepareStatement("SELECT id FROM type WHERE type =?");
			preState.setInt(1, type);
			result = preState.executeQuery();
			//Datensatz gefunden
			if (result.next()  ) {
				this.typeID = result.getInt("id");
			} else {
				//Sonst Datensatz hinzuf�gen
				preState = conn.prepareStatement("INSERT INTO type(type) VALUES (?)", Statement.RETURN_GENERATED_KEYS);
				preState.setInt(1, type);
				preState.executeUpdate();
				
				ResultSet generatedKeys = preState.getGeneratedKeys();
				if (generatedKeys.next()) {
					this.typeID = generatedKeys.getInt(1);
				}
			}
			
			
			//Sudoku
			preState = conn.prepareStatement("SELECT id FROM sudoku WHERE sudoku =?");
			preState.setString(1, sudoku);
			result = preState.executeQuery();
			//Datensatz gefunden
			if (result.next()  ) {
				this.sudokuID = result.getInt("id");
			} else {
				//Sonst Datensatz hinzuf�gen
				preState = conn.prepareStatement("INSERT INTO sudoku(sudoku, t_id) VALUES (?,?)", Statement.RETURN_GENERATED_KEYS);
				preState.setString(1, sudoku);
				preState.setInt(2, this.typeID);
				preState.executeUpdate();
				
				ResultSet generatedKeys = preState.getGeneratedKeys();
				if (generatedKeys.next()) {
					this.sudokuID = generatedKeys.getInt(1);
				}
			}
			
			//Highscore
			//IDs hinzuf�gen zur DB
			preState = conn.prepareStatement("INSERT INTO highscore(s_id, n_id, time ) VALUES (?,?,?)");
			preState.setInt(1, this.sudokuID);
			preState.setInt(2, this.nameID);
			preState.setLong(3, time);
			preState.executeUpdate();
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * L�scht alle Highscores.
	 * 
	 * @return boolean 
	 */
	public boolean resetHighscore() {
		try {
			PreparedStatement preState = conn.prepareStatement("DELETE FROM highscore;");
			if (preState.executeUpdate() > 0) {
				return true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	/**
	 * L�dt eine bestimme Anzahl an Highscores von einer Sudokugr��e.
	 * 
	 * @param limit
	 * @param type
	 * @return highscores
	 */
	public List<HighscoreModel> getHighscores (int limit, String type) {
		List<HighscoreModel> highscores = new ArrayList<HighscoreModel>();
		try {
			PreparedStatement preState = conn.prepareStatement("Select n.name,s.sudoku, t.type, h.time FROM highscore h INNER JOIN name n ON (n.id = h.n_id) INNER JOIN sudoku s ON(s.id = h.s_id) INNER JOIN type t ON (s.t_id = t.id) WHERE type =  ? ORDER BY `h`.`time` ASC LIMIT ?");
			
					preState.setString(1, type);
					preState.setInt(2, limit);
			ResultSet result = preState.executeQuery();
		
			while (result.next()) {
				String name = result.getString("n.name");
				String sudoku = result.getString("s.sudoku");
				String time = result.getString("h.time");
				
				HighscoreModel h = new HighscoreModel();
				h.setName(name);
				h.setSudoku(sudoku);
				h.setTime(Long.valueOf(time));
				highscores.add(h);
				
				
			}
			
			
			preState.close();
			conn.close();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return highscores;
		
	}
	/**
	 * L�dt alle Highscores aus der Datenbank.
	 * 
	 * @return highscores
	 */
	public List<HighscoreModel> getAllHighscores () {
		List<HighscoreModel> highscores = new ArrayList<HighscoreModel>();
		try {
			PreparedStatement preState = conn.prepareStatement("Select n.name,s.sudoku, t.type, h.time FROM highscore h INNER JOIN name n ON (n.id = h.n_id) INNER JOIN sudoku s ON(s.id = h.s_id) INNER JOIN type t ON (s.t_id = t.id)   ORDER BY `h`.`time` ASC");
			ResultSet result = preState.executeQuery();
		
			while (result.next()) {
				String name = result.getString("n.name");
				String sudoku = result.getString("s.sudoku");
				String time = result.getString("h.time");
				
				HighscoreModel h = new HighscoreModel();
				h.setName(name);
				h.setSudoku(sudoku);
				h.setTime(Long.valueOf(time));
				highscores.add(h);
				
				
			}
			preState.close();
			conn.close();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return highscores;
	}
	

}
