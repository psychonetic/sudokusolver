package classes;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;



/**
 * <strong>Klasse Properties Handler</strong>
 * 
 * <p>Diese Klasse wird ben�tigt um die Einstellungen der Konfigurationsdatei
 * des SudokuSolvers zu verwalten.
 * Der PropertiesHandler kann Einstellungen setzen oder sie abfragen.</p>
 * 
 * 
 * 
 * @author 	Marco Martens
 * @version 1.0 Oktober 2012
 *
 */
public class PropertiesHandler {

	/**
	 * Konfigurationsdatei
	 * 
	 */
	
	protected String configFile;
	
	/**
	 * Propertiesobjekt
	 */
	protected Properties prob = null;
	
	/**
	 * Readerobjekt
	 */
	protected FileReader reader = null;
	
	/**
	 * Konstruktor
	 * 
	 * <p>Legt die Datei fest aus der die Einstellungen geladen werden sollen.</p>
	 * @param configFile
	 */
	public PropertiesHandler(String configFile) {
		setConfig(configFile);
		this.prob = new Properties();
		
		
	}
	/**
	 * Legt die Konfigurationsdatei fest.
	 * 
	 * @param configFile
	 */
	
	public void setConfig (String configFile) {
		this.configFile = configFile;
	}
	
	/**
	 * Gibt die aktuelle Konfigurationsdatei zur�ck.
	 * @return configFile
	 */
	
	public String getConfig() {
		return this.configFile;
	}
	
	/**
	 * Legt ein neue Einstellung fest.
	 * 
	 * @param key
	 * @param value
	 * @throws IOException
	 */
	public void setConfigItem (String key, String value) throws IOException {
		this.prob.setProperty(key, value);
		
	}
	/**
	 * Gibt eine Einstellung zur�ck.
	 * 
	 * @param key
	 * @return
	 * @throws IOException
	 */
	public String getConfigItem (String key) throws IOException {
	  	reader = new FileReader(this.configFile);
		prob.load(this.reader);
		reader.close();
		return prob.getProperty(key);
	  
	}
	
	/**
	 * Schreibt die gesetzen Einstellungen in die Konfigurationsdatei.
	 * 
	 * @param information
	 * @throws IOException
	 */
	public void writeProperty(String information) throws IOException {
		FileWriter writer = new FileWriter( this.configFile);
		prob.store(writer, information);
		writer.close();
	}
	
	
}
