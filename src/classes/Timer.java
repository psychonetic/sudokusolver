package classes;

import view.MainView;

/**
 * <strong>Klasse Timer</strong>
 * 
 * <p>Diese Klasse misst die Zeit beim Spielen eines Sudokus.
 * Die Klasse ist dabei ein Thread der parallel zur eigentlichen Anwendung l�uft, sodass
 * dieser auch von au�en gestoppt und gestartet werden kann.</p>
 * 
 * 
 * @author 	Marco Martens
 * @version	1.0 Dezember 2012
 *
 */
public class Timer extends Thread {

	/**
	 * Millisekunden
	 */
	
	private long milSec = 0;
	
	/**
	 * Sekunden
	 */
	
	private long sec = 0;
	
	/**
	 * Minuten
	 */
	
	private long min = 0;
	
	/**
	 * Stunden
	 */
	
	private long h = 0;
	
	/**
	 * View auf dem der Timer dargestellt wird. 
	 */
	
	private MainView _view;
	
	/**
	 * Status des Timers
	 */
	
	private boolean running = true;
	
	/**
	 * Start des Timers 
	 */
	
	private boolean hasStarted = false;
	
	/**
	 * Konstruktor
	 * 
	 * <p>Legt die Viewinstanz fest.</p>
	 * 
	 * @param _view
	 */
	
	public Timer(MainView _view) {
		this._view = _view;
	}
	
	
	/**
	 * Startet den Timer
	 * 
	 */
	public void run() {
		while(true) {
			hasStarted = true;
			if(running == false) {
				synchronized(this) {
					try {
						this.wait();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			try{Thread.sleep(9);}catch(Exception e){}
				if (milSec <= 99) {
					milSec = milSec + 1;
					
				} else {
					milSec = 0;
					
					if(sec <=59) {
						sec = sec + 1;
						
					} else {
						sec = 0;
						
						if(min <=59) {
							min = min + 1;
						} else {
							min = 0;
							h = h + 1;
						}
					}
				}
				_view.setTime("Zeit: " + h + " : " + min + " : " + sec);
		}
	}
	
	/**
	 * Startet oder Stoppt den Timer.
	 * 
	 * @param bool
	 */
	
	public void setRunning (Boolean bool) {
		running = bool;
		//this.notify();
		
	}
	
	/**
	 * Fragt den Status ab, ob der Timer gestartet wurde.
	 * 
	 * @return hasStarted
	 */
	
	public boolean isStarted () {
		return this.hasStarted;
	}
	
	/**
	 * L�sst den Timer weiter laufen.
	 */
	
	public synchronized void resumeTimer() {
		running = true;
		notify();
	}
	
	/**
	 * Setzt die Zeit des Timers auf 0.
	 */
	
	public void resetTimer() {
		min = 0;
		milSec = 0;
		sec = 0;
		h = 0;
		
	}
	
	/**
	 * Gibt die Zeit des Timers in Sekunden zur�ck.
	 * 
	 * @return time
	 */
	
	public long getTime() {
		return (sec + min * 60 + h * 60 * 60);
	}
}
