package classes;

import java.util.ArrayList;
import java.util.List;
import model.SudokuModel;

/**
 * <strong>Klasse Backtrack_Solver</strong>
 * 
 * <p>Diese Klasse l�st ein Sudoku
 * mit dem Backtracking Verfahren.
 * Dabei werden alle M�glichkeiten ausprobiert
 * bis ein g�ltige L�sung gefunden wurde.
 * Die Klasse stellt f�r das Backtracking 
 * verschiedene Methoden bereit.</p>
 * 
 *  <li>showSolve: Zeigt den Fortschritt des Backtrackings Live an.</li>
 *  <li>smartSolve: Untersucht nur die m�gl. Zahlen.</li>
 *  <li>Solve: Untersucht alle Felder von den Zahlen 1-9.</li>
 *  
 * 
 * @see 	HumanSolver
 * @see 	BaseSolver
 * @author 	Marco Martens
 * @version 1.0 Oktober 2012
 */
public class BacktrackSolver extends BaseSolver {
	
	/**
	 *  L�sungen
	 *  
	 *  Es werden alle m�gl. L�sungen f�r das Sudoku
	 *  als Strings abgelegt. (wird ben�tigt f�r das generieren
	 *  von Sudokus!)
	 *  
	 */
	private List<String> solutions = new ArrayList<String>();
	
	/**
	 *  SudokoModel
	 *  
	 *  Das TableModel des Sudokufeldes.
	 */
	private SudokuModel _model;
	
	/**
	 *  Backtracks
	 *  
	 *  Anzahl der Versuche beim Backtracking.
	 */
	public long backtracks = 0;
	
	/**
	 * Konstruktor
	 * 
	 * <p>Legt das zu l�sende Sudoku fest.</p>
	 * @param input 
	 */
	public BacktrackSolver (int[][] input) {
		
		super (input);
	}
	
	/**
	 * Konstruktor
	 * 
	 * <p>Legt das zu l�sende Sudoku fest und
	 * definiert das SudokoModel f�r das
	 * Live-Backtracking.</p>
	 * 
	 * @param input 
	 * @param model 
	 */
	public BacktrackSolver (int[][] input, SudokuModel model) {
		
		super (input);
		this._model = model;
	}
	
	
	/**
	 * Gibt die Backtracks (Versuche) zur�ck.
	 * 
	 * @return backtracks
	 */
	
	public long getBacktracks() {
		return this.backtracks;
	}
	
	/**
	 * 
	 * Sucht alle L�sungen f�r ein Sudoku.
	 * 
	 * Wird ben�tigt f�r das Generieren von Sudokus,
	 * damit diese beim Erstellen eindeutig sind.
	 * Laut Definition ist ein Sudoku nur dann ein echtes 
	 * Sudoku, wenn es nur eine einzige L�sung hat.
	 * 
	 * @see classes.SudokuGenerator
	 * 
	 * @param max 
	 * @return solutions 
	 */
	public List<String> getAllSolutions (int max) {
		boolean exists = true;
		for (int row = 0; row < input.length; row++) {
			for (int col = 0; col < input[0].length; col++) {
					solve(row, col);
					for (int i = 0; i < solutions.size(); i++) {
						
						if (solutions.get(i).equals(SudokuToString(this.input))) {
							exists = false;
						}
					}
					if (exists != false) {
						solutions.add(SudokuToString(this.input));
					}
					
					if (solutions.size()==max) {
						return solutions;
					}
				}
		}
		return solutions;
	}

	/**
	 * Backtracking (einfaches)
	 * 
	 * 
	 * Die Funktion ruft sich Rekursiv durch und probiert
	 * alle m�glichen L�sungen bis das Sudoku gel�st ist.
	 * Wenn ein Widerspruch entsteht wird das letzte Feld
	 * wieder zur�ck gesetzt und der Wert um +1 erh�ht.
	 * 
	 * @param row
	 * @param col
	 * @return solvedSudoku
	 */
	public boolean solve (int row, int col) {
		
		//Versuche
		this.backtracks++;
		
		//Zeilenende erreicht?
		if (row == input.length) {
			col++;
			row = 0;
			
			//Wenn die letzte Spalte+1 erreicht wurde, 
			//ist das Sudoku gel�st.
			if (col == input[0].length) {
				return true;
			}
			
		}
		//Feld leer?
		if (input[row][col] > 0) {
			return solve(row+1, col);
		}
		//Zahl erh�hen
		for (byte value = 1; value <= input.length; value++) {
			//Zahl m�glich?
			if (this.isPossible(row, col, value)==false) {
				//Wenn ja, Zahl setzen
				input[row][col] = value;
				//Weiter machen mit n�chstem Feld
				if (solve(row+1, col)) {
					return true;
				}
			}
		}
	//Fehler, Feld zur�cksetzen.
	input[row][col] = 0;
	return false;
	}
	
	/**
	 * Backtracking (Live-Ansicht)
	 *  
	 * L�st das Sudoku sowie bei solve.
	 * Allerdings kann man den Fortschritt
	 * des Backtrackings in der GUI erkennen.
	 * @see solve 
	 * 
	 * 
	 * @param row
	 * @param col
	 * @return solvedSudoku
	 */
	public boolean showSolve (int row, int col) {
		
		//Versuche
		this.backtracks++;
		//Zeilenende?
		if (row == input.length) {
			col++;
			row = 0;
			if (col == input[0].length) {
				return true;
			}	
		}
		//Feld leer?
		if (input[row][col] > 0) {
			return showSolve(row+1, col);
		}
		//Zahl erh�hen
		for (byte value = 1; value <= input.length; value++) {
			//Zahl m�glich?
			if (this.isPossible(row, col, value)==false) {
				//Wenn ja, Zahl setzen
				input[row][col] = value;
				try {
					//Legt den Thread schlafen, damit man etwas auf der GUI sieht.
					Thread.sleep(200);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				_model.setValueAt(value, row, col);
				
				//Weiter machen mit n�chstem Feld
				if (showSolve(row+1, col)) {
					return true;
				}
			}
		}
	//Fehler: Wert zur�cksetzen und wieder anfangen mit value+1;
	input[row][col] = 0;
	try {
		//Legt den Thread schlafen, damit man etwas auf der GUI sieht.
		Thread.sleep(200);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	_model.setValueAt(null, row, col);
	
	return false;
	}
	
	/**
	 * Backtracking (intelligentes)
	 * 
	 * L�st das Sudoku sowie bei solve.
	 * @see solve 
	 * 
	 * Im Gegensatz zu dem normalen solve
	 * werden hier nur die m�glichen Zahlen getestet
	 * und nicht alle von 1-9.
	 * Dadurch wird die Funktion weniger oft aufgerufen
	 * und ist somit effizienter als solve.
	 *
	 * @param row
	 * @param col
	 * @return solvedSudoku
	 */
	public boolean smartSolve (int row, int col) {
		//Versuche
		this.backtracks++;
		//Zeilenende?
		
		if (row == input.length) {
			col++;
			row = 0;
			
			
			if (col == input[0].length) {
				return true;
			}
			
		}
		//Feld leer?
		if (input[row][col] > 0) {
			return solve(row+1, col);
		}
		//Kandidaten
		List<Integer>  candidates = new ArrayList<Integer>(); 
		candidates = getCandidates(row, col);
		for (byte i = 0; i < candidates.size(); i++) {
			//Zahl m�glich?
			if (this.isPossible(row, col, candidates.get(i))==false) {
				//Wenn ja, Zahl setzen
				input[row][col] = candidates.get(i);
				//Weiter machen mit n�chstem Feld
				if (solve(row+1, col)) {
					return true;
				}
			}
		}
	input[row][col] = 0;
	return false;
	}
	/**
	 * Backtracking (Live-Ansicht + Intelligentes)
	 * 
	 * Bei dieser Variante werden das Live-Backtracking und
	 * die Smart-Variante miteinander kombiniert.
	 * Diese Variante eignet sich am besten, da bei dieser Versuche
	 * nur wirklich n�tige Versuche durch gef�hrt werden und schont so
	 * die Ressourcen.
	 * 
	 * 
	 * 
	 * @param row
	 * @param col
	 * @return solvedSudoku
	 */
	
	public boolean smartShowSolve (int row, int col) {
		//Versuche
		this.backtracks++;
		//Zeilenende?
		
		if (row == input.length) {
			col++;
			row = 0;
			
			
			if (col == input[0].length) {
				return true;
			}
			
		}
		//Feld leer?
		if (input[row][col] > 0) {
			return smartShowSolve(row+1, col);
		}
		//Kandidaten
		List<Integer>  candidates = new ArrayList<Integer>(); 
		candidates = getCandidates(row, col);
		for (byte i = 0; i < candidates.size(); i++) {
			//Zahl m�glich?
			if (this.isPossible(row, col, candidates.get(i))==false) {
				//Wenn ja, Zahl setzen
				input[row][col] = candidates.get(i);
				_model.setValueAt(candidates.get(i), row, col);
				try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//Weiter machen mit n�chstem Feld
				if (smartShowSolve(row+1, col)) {
					return true;
				}
			}
		}
	//Fehler: Wert zur�cksetzen und wieder anfangen mit value+1;
	input[row][col] = 0;
	_model.setValueAt(null, row, col);
	try {
		Thread.sleep(200);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return false;
	}
	
	/**
	 * Wandelt ein Sudoku-Array in einen String um.
	 * 
	 * Wird ben�tigt um alle m�glichen
	 * L�sungen f�r ein Sudoku zu finden.
	 * Der Vergleich der Strings ist performanter
	 * als der Vergleich jedes einzelnen Wertes 
	 * mit jedem Sudoku.
	 * 
	 * @see getAllSolutions
	 * 
	 * @param sudoku
	 * @return String sudoku
	 */
	private String SudokuToString(int[][] sudoku) {
		String newsudoku = "";
		for (int row = 0; row < sudoku.length; row++) {
			for (int col = 0; col < sudoku[0].length; col++) {
				newsudoku += sudoku[row][col];
				}
		}
		return newsudoku;
	}
}

