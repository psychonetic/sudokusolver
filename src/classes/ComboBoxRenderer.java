package classes;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
/**
 * <strong>Klasse ComboBoxRenderer</strong>
 * 
 * <p>Der ComboBoxRenderer k�rzt die Sudokus
 * in der Highscore-Darstellung, da die ComboxBox ohne den Renderer
 * das Sudoku komplett darstellen w�rde.
 * Dadurch zieht sich die ComboBox unn�tig in die Breite.
 * Deswegen wird die Anzeige mit dem Renderer auf 15 Zahlen gek�rzt.</p>
 * 
 * @author 	Marco Martens
 * @version 1.0 Oktober 2010
 *
 */
public class ComboBoxRenderer extends JLabel
implements ListCellRenderer<Object> {

	
	/**
	 * F�r den Compiler...
	 */
	private static final long serialVersionUID = -8968707523260122472L;

	/**
	 * K�rzt den dargestellten Text auf max. 15 Zeichen.
	 * 
	 * @param list
	 * @param value
	 * @param index
	 * @param isSelected
	 * @param cellHasFocus
	 * @return Component
	 */
	@Override
	public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected,boolean cellHasFocus) {
		// TODO Auto-generated method stub
		
		
		String show = String.valueOf(value);
		
		
		
		if (show.length() > 15) {
			this.setText(show.substring(0, 15));
		} else {
			this.setText(show);
		}
		
		setHorizontalAlignment(CENTER);
	    setVerticalAlignment(CENTER);
		
	    setOpaque(true);
		return this;
	}
	
}
