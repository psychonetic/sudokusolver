package classes;


import java.io.File;
import java.io.IOException;

/**
 * <strong>Klasse FileHandler</strong>
 * 
 * <p>Diese Klasse �ffnet eine Datei
 * und macht ein paar �berpr�fungen bevor
 * mit der Datei gearbeitet wird.</p>
 * 
 * @see 	SavegameHandler
 * @see 	SudokuHandler
 * @author	Marco Martens
 * @version 1.0 Oktober 2012	
 */

public class FileHandler {
	/**
	 * �ffnet eine Datei.
	 * 
	 * @param file
	 * @param write
	 * @return boolean
	 * @throws IOException
	 */
	protected boolean openFile (File file, Boolean write) throws IOException {
		
		if (!file.isFile()) {
			throw new IOException("Es wurde keine Datei �bergeben");
		}
		if (!file.exists()) {
			throw new IOException("Die Datei " + file.getAbsolutePath() + " konnte nicht gefunden werden");
		}
		if(!file.canRead()) {
			throw new IOException("Es existieren keine Leserechte f�r die Datei " + file.getAbsolutePath());
		}
		if (write == true) {
			if(!file.canWrite()) {
				throw new IOException("Es existieren keine Schreibrechte f�r die Datei " + file.getAbsolutePath());
			}
		}
		
		
		return true;
	}
}
