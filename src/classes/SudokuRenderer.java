package classes;
import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * <strong>Klasse SudokuRenderer</strong>
 * 
 * <p>Diese Klasse stellt die typischen Boxen des Sudokus dar.
 * Das SudokuFeld wird dabei wie ein Schachbrett dargestellt.
 * Das ganze funktioniert dynamisch, sodass auch gr��ere Sudoku-Gr��en
 * richtig dargestellt werden.</p>
 * 
 * 
 * @author 	Marco Martens
 * @version	1.0 Oktober 2012
 *
 */
public class SudokuRenderer extends DefaultTableCellRenderer{
	

	/**
	 * F�r den Compiler
	 */
	
	private static final long serialVersionUID = -720661078965654563L;
	
	/**
	 * Feldgr��e 
	 */
	
	private int size = -1;
	
	/**
	 * 
	 * Schachbrettmusterdarstellung des Sudokufeldes
	 * 
	 * @param	hasFocus
	 * @param	isSelected
	 * @param	value
	 * @param	table
	 * @param	row
	 * @param 	col
	 * @return 	component
	 */
	public Component getTableCellRendererComponent(JTable table, Object value,
		            boolean isSelected, boolean hasFocus, int row, int col) {
		        this.size = (int) Math.sqrt(Registry.get().getInt("size"));
				super.getTableCellRendererComponent(table, value, isSelected,
		                hasFocus, row, col);
				setOpaque(true);
				
				setBackground((row/size+col/size)%2==0?Color.lightGray:Color.white);
		        setHorizontalAlignment(SwingConstants.CENTER);
		   
		        return this;
		    }
		}