package classes;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
/**
 * <strong>Klasse SQLWrapper</strong>
 * 
 * <p>Diese Klasse stellt die Verbindung mit der Datenbank her.
 * Die Zugangsdaten der Datenbank stammen dabei aus der Konfigurationsdatei.</p>
 * 
 * @see 	InstallerDatabase
 * @see		HighscoreDatabase
 * @see     SavegameHandlerDatabase
 * @see     SudokuDatabase
 * @author 	Marco Martens
 * @version 1.0 Oktober 2012
 *
 */
public class SQLWrapper {

	/**
	 * Datenbank
	 */
	
	private String database = Registry.get().getString("db");
	
	/**
	 * Benutzer
	 */
	
	private String user = Registry.get().getString("user");
	
	/**
	 * Passwort
	 */
	
	private String password = Registry.get().getString("pw");
	
	/**
	 * Host
	 */
	
	private String host = Registry.get().getString("host");
	
	/**
	 * Aktuelle Verbindung
	 */
	
	Connection conn = null;
	
	/**
	 * Konstruktor
	 * 
	 * <p>Stellt die Datenbankverbindung her.</p>
	 * 
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	
	public SQLWrapper () throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		connect();
	}
	
	/**
	 * Stellt die Datenbankverbindung her.
	 * 
	 * @return boolean
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	
	public boolean connect () throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException  {
		
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			this.conn = DriverManager.getConnection("jdbc:mysql://"+ this.host +"/" + this.database + "", this.user, this.password);
			
			return true;
		
		
	}
	
}
