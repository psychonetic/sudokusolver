package classes;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import model.HighscoreModel;

/**
 * <strong>Klasse HighscoreXML</strong>
 * 
 * <p>Diese Klasse bietet die M�glichkeit
 * die Sudokus aus einer XML-Datei zu lesen,
 * speichern und zu l�schen.</p>
 * 
 * @see 	HighscoreDatabase
 * @author 	Marco Martens
 * @version 1.0 Oktober 2012
 *
 */
public class HighscoreXML {
	  
	/**
	   * Bezeichnung des Sudokus in der XML-Datei
	   */
	  static final String SUDOKU = "sudoku";
	 
	  /**
	   * Bezeichnung der Zahlen des Sudokus in der XML-Datei
	   */
	  static final String NUMBERS = "numbers";
	 
	  /**
	   * Bezeichnung des Highscores in der XML-Datei
	   */
	  static final String HIGHSCORE = "highscore";
	  
	  /**
	   * Bezeichnung des Namens in der XML-Datei
	   */
	  static final String NAME = "name";
	  
	  /**
	   * Bezeichnung der Zeit in der XML-Datei
	   */
	  static final String TIME = "time";
	  
	  /**
	   * Der Typ des Sudokus
	   */
	 
	  private String type;
	  
	  /**
	   * Die Highscoredatei
	   */
	  
	  private String file;
	  
	  /**
	   * Das aktuelle Sudoku
	   */
	  
	  private String currentSudoku;
	  
	  /**
	   * Flag ob es schon Eintr�ge gibt.
	   * @see addHighscore
	   */
	  
	  private boolean hasWrite = false;
	  
	  /**
	   * Konstruktor
	   * 
	   * <p>Legt die Highscoredatei fest.</p>
	   * @param file
	   */
	  public HighscoreXML(String file) {
		  setHighscoreFile(file);
	  }
	  /**
	   * Legt die Highscoredatei fest.
	   * 
	   * @param file
	   */
	 
	  private void setHighscoreFile(String file) {
		  this.file = file;
	  }
	 
	  /**
	   * Gibt die aktuelle Highscoredatei wieder.
	   * @return file
	   */
	  public String getHighscoreFile() {
		  return this.file;
	  }
	  /**
	   * L�dt alle Highscores aus der XML-Datei.
	   *
	   * @see HighscoreModel
	   * @return highscores
	   * @throws XMLStreamException
	   * @throws IOException
	   * 
	   */
	  public List<HighscoreModel> getHighscores() throws XMLStreamException, IOException {
		    List<HighscoreModel> highscores = new ArrayList<HighscoreModel>();
		   
		      
		      XMLInputFactory inputFactory = XMLInputFactory.newInstance();
		     
		      InputStream in = new FileInputStream(this.file);
		      XMLEventReader eventReader = inputFactory.createXMLEventReader(in);
		 
		      HighscoreModel highscore = null;
		     
		      while (eventReader.hasNext()) {
		        XMLEvent event = eventReader.nextEvent();
		        
		        if (event.isStartElement()) {
		          StartElement startElement = event.asStartElement();
		         
		          if (startElement.getName().getLocalPart() == (SUDOKU)) {
		           highscore = new HighscoreModel();
		          
		          
		            @SuppressWarnings("unchecked")
					Iterator<Attribute> attributes = startElement.getAttributes();
		            while (attributes.hasNext()) {
		              Attribute attribute = attributes.next();
		             
		              if (attribute.getName().toString().equals(NUMBERS)) {
		               this.type = attribute.getValue();
		               highscore.setSudoku(attribute.getValue());
		              }

		            }
		           
		          }
		          if (this.type != null) {
		        	 
		        		  if (event.isStartElement()) {
					            if (event.asStartElement().getName().getLocalPart().equals(HIGHSCORE)) {
					              event = eventReader.nextEvent();
					              highscore = new HighscoreModel();
					              continue;
					              
					            }
					            if (event.asStartElement().getName().getLocalPart().equals(NAME)) {
						            event = eventReader.nextEvent();
						            highscore.setName(event.asCharacters().getData());
						            continue;
						          }

						          if (event.asStartElement().getName().getLocalPart().equals(TIME)) {
						            event = eventReader.nextEvent();					
						            highscore.setTime(Long.valueOf(event.asCharacters().getData()));
						            continue;
						          }
					          }
					         
					          
					          
					        
			       
		        	  }
		          }
		      
		        if (event.isEndElement()) {
			          EndElement endElement = event.asEndElement();
			          if (endElement.getName().getLocalPart() == (HIGHSCORE)) {
			        	highscore.setSudoku(type);
			        	highscores.add(highscore);
			          }
			        }

		        if (event.isEndElement()) {
		          EndElement endElement = event.asEndElement();
		          if (endElement.getName().getLocalPart() == (SUDOKU)) {
		        	  
		          }
		        }  
		        

		      }
		   in.close();
		   Collections.sort(highscores);
		   return highscores;
		  }
	  /**
	   * 
	   * Erstellt einen neuen Highscore.
	   * 
	   * @param sudoku
	   * @param name
	   * @param time
	   * @throws XMLStreamException
	   * @throws IOException
	   */
	  public void addHighscore (String sudoku, String name, String time) throws XMLStreamException, IOException {
		  //Highscores holen
		  //Vergleich sudoku mit Highscore
		  //True -> Write Sudoku
		  //Highscores
		  //Ende
		  List<HighscoreModel> highscores= new ArrayList<HighscoreModel>();
		  highscores = getHighscores();
		  File file = new File(this.file);
		  BufferedWriter writer = null; 
		  writer = new BufferedWriter( new FileWriter(file,  false));
		  writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");	
		  writer.write(System.getProperty("line.separator"));
		  writer.write("<config>");		
		  writer.write(System.getProperty("line.separator"));	
		  
			
		  for (int i = 0; i < highscores.size(); i++) {
			 
			  if (this.currentSudoku == null) {
				 this.currentSudoku = highscores.get(i).getSudoku();
				 writer.write("<sudoku numbers=\"" + highscores.get(i).getSudoku()  + "\">" );
				 writer.write(System.getProperty("line.separator"));
			 } 
			if(hasWrite == false) {
				
				if (this.currentSudoku.equals(sudoku)) {
					 writer.write("<highscore>");
					 writer.write(System.getProperty("line.separator"));
					 writer.write("<name>" + name + "</name>");
					 writer.write(System.getProperty("line.separator"));
					 writer.write("<time>" + time + "</time>");
					 writer.write(System.getProperty("line.separator"));
					 writer.write("</highscore>");
					 writer.write(System.getProperty("line.separator"));
					 writer.write(System.getProperty("line.separator"));
					 hasWrite = true;
			}
			}
			if (this.currentSudoku != highscores.get(i).getSudoku()) {
				
				this.currentSudoku = highscores.get(i).getSudoku();
					 writer.write("</sudoku>");
					 writer.write(System.getProperty("line.separator"));
					 writer.write("<sudoku numbers=\"" +highscores.get(i).getSudoku() + "\">" );
					 writer.write(System.getProperty("line.separator"));
					 writer.write("<highscore>");
					 writer.write(System.getProperty("line.separator"));
					 writer.write("<name>" + highscores.get(i).getName() + "</name>");
					 writer.write(System.getProperty("line.separator"));
					 writer.write("<time>" + highscores.get(i).getTime() + "</time>");
					 writer.write(System.getProperty("line.separator"));
					 writer.write("</highscore>");
					 writer.write(System.getProperty("line.separator"));
			
			} else {
					 //Das sudoku ist gleich
					 writer.write("<highscore>");
					 writer.write(System.getProperty("line.separator"));
					 writer.write("<name>" + highscores.get(i).getName() + "</name>");
					 writer.write(System.getProperty("line.separator"));
					 writer.write("<time>" +  highscores.get(i).getTime() + "</time>");
					 writer.write(System.getProperty("line.separator"));
					 writer.write("</highscore>");
					 writer.write(System.getProperty("line.separator"));
				 }
				 
			  
		  }
		  if (hasWrite==false) {
			  	 if (highscores.size() != 0) {
			  		writer.write("</sudoku>");
			  		 writer.write(System.getProperty("line.separator"));
			  	 }
			  	
			  	 writer.write("<sudoku numbers=\"" + sudoku  + "\">" );
				 writer.write(System.getProperty("line.separator")); 
				 writer.write("<highscore>");
				 writer.write(System.getProperty("line.separator"));
				 writer.write("<name>" + name + "</name>");
				 writer.write(System.getProperty("line.separator"));
				 writer.write("<time>" + time + "</time>");
				 writer.write(System.getProperty("line.separator"));
				 writer.write("</highscore>");
				 writer.write(System.getProperty("line.separator"));
		  }
		  writer.write("</sudoku>");
		  writer.write(System.getProperty("line.separator"));
		  writer.write("</config>");			
		  writer.flush();	
		  writer.close();
		  this.currentSudoku = null;
		  hasWrite = false;
	  }

		
	} 
