package classes;
import java.util.HashMap;
 /**
  * <strong>Klasse Registry</strong>
  * 
  * <p>Die Registry stellt Methoden bereit
  * um Einstellungen global zu speichern und
  * abzurufen.
  * <p>F�r weitere Informationen zur Registry-Pattern:
  * <a href="http://de.wikipedia.org/wiki/Entwurfsmuster">Registry</a></p>
  * 
  * 
  * @author 	Marco Martens
  * @version 	1.0 Oktober 2012
  *
  */
final public class Registry {
	
	/**
	 * <a href="http://de.wikipedia.org/wiki/Entwurfsmuster">Singleton</a>
	 */
    private static Registry singleton = null;
   
    /**
     * Boolische-Variablen
     */
    
    private static HashMap<String, Boolean> booleanMap = new HashMap<String, Boolean>();
   
    /**
     * Integer-Variablen
     */
    
    private static HashMap<String, Integer> intMap = new HashMap<String, Integer>();
    
    /**
     * Long-Variablen
     */
    private static HashMap<String, Long> longMap = new HashMap<String, Long>();
    
    /**
     * Float-Variablen
     */
    
    private static HashMap<String, Float> floatMap = new HashMap<String, Float>();
    
    /**
    * Double-Variablen
    */
    
    private static HashMap<String, Double> doubleMap = new HashMap<String, Double>();
    
    /**
     * Char-Variablen
     */
    
    private static HashMap<String, Character> charMap = new HashMap<String, Character>();
    
    /**
     * String-Variablen
     */
    private static HashMap<String, String> stringMap = new HashMap<String, String>();
 
    /**
     * Gibt die Instanz der Registry wieder
     * 
     * @return singleton
     */
    public static Registry get() {
        if (singleton == null) {
            singleton = new Registry();
        }
        return singleton;
    }
    
    /**
     * �berpr�ft ob ein Wert existiert in der Registry.
     * 
     * @param key
     * @return key
     */
    public boolean has(String key) {
        return (booleanMap.containsKey(key) || intMap.containsKey(key) || longMap.containsKey(key) || floatMap.containsKey(key)
                || doubleMap.containsKey(key) || charMap.containsKey(key) || stringMap.containsKey(key));
    }
 
    /**
     * Gibt einen Boolean zur�ck.
     * 
     * @param key
     * @return
     */
    public boolean getBoolean(String key) {
        return booleanMap.get(key);
    }
    
    /**
     * Gibt einen Integer zur�ck.
     * 
     * @param key
     * @return
     */
    
    public int getInt(String key) {
        return intMap.get(key);
    }
    
    /**
     * Gibt einen Long zur�ck.
     * 
     * @param key
     * @return
     */
    
    public long getLong(String key) {
        return longMap.get(key);
    }
    
    /**
     * Gibt einen Float zur�ck.
     * 
     * @param key
     * @return
     */
    
    public float getFloat(String key) {
        return floatMap.get(key);
    }
 
    /**
     * Gibt einen Double zur�ck.
     * 
     * @param key
     * @return
     */
    
    public double getDouble(String key) {
        return doubleMap.get(key);
    }
    
    /**
     * Gibt einen Char zur�ck.
     * 
     * @param key
     * @return
     */
    
    public char getChar(String key) {
        return charMap.get(key);
    }
    
    /**
     * Gibt einen String zur�ck.
     * 
     * @param key
     * @return
     */
    
    public String getString(String key) {
        return stringMap.get(key);
    }
    
    /**
     * Setzt eine neue Boolean-Variable.
     * 
     * @param key
     * @param value
     */
    
    public void set(String key, boolean value) {
        booleanMap.put(key, value);
    }
 
    /**
     * Setzt eine neue Int-Variable.
     * 
     * @param key
     * @param value
     */
    
    public void set(String key, int value) {
        intMap.put(key, value);
    }
    
    /**
     * Setzt eine neue Long-Variable.
     * 
     * @param key
     * @param value
     */
    
    public void set(String key, long value) {
        longMap.put(key, value);
    }
    
    /**
     * Setzt eine neue Float-Variable.
     * 
     * @param key
     * @param value
     */
    
    public void set(String key, float value) {
        floatMap.put(key, value);
    }
    
    /**
     * Setzt eine neue Double-Variable.
     * 
     * @param key
     * @param value
     */
    
    public void set(String key, double value) {
        doubleMap.put(key, value);
    }
 
    /**
     * Setzt eine neue Char-Variable.
     * 
     * @param key
     * @param value
     */	
    
    public void set(String key, char value) {
        charMap.put(key, value);
    }
    
    /**
     * Setzt eine neue String-Variable.
     * 
     * @param key
     * @param value
     */
    
    public void set(String key, String value) {
        stringMap.put(key, value);
    }
}