package classes;
import java.io.IOException;
import java.sql.SQLException;

import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.xml.stream.XMLStreamException;

import model.SudokuModel;

import view.MainView;
import view.NewHighscoreView;


/**
 * <strong>Klasse SudokuFieldListener</strong>
 * 
 * <p>Diese Klasse �bernimmt die �berpr�fung der Usereingaben
 * beim setzen der Zahlen in das Sudokufeld.</p>
 * 
 * <p>Des Weiteren k�mmert sich diese Klasse um das Stoppen des Timers
 * und teilweise um die Erstellung der Statistiken.</p>
 * 
 * @see 	Timer
 * @see 	controller.StatisticController
 * @see 	Registry
 * @see 	controller.HighscoreController
 * @see 	view.HighscoreView
 * @author 	Marco Martens
 * @version 1.0 Oktober 2012
 *
 */

public class SudokuFieldListener implements ListSelectionListener
{
	/**
	 * Die HauptGUI
	 */
	
	private MainView _view;
	
	/**
	 * HighscoreView
	 * 
	 * Wird aufgerufen wenn der Spieler ein Spiel erfolgreich beendet.
	 */
	
	private NewHighscoreView _hview;
	
	/**
	 * Das SudokuModel. 
	 */
	
	private SudokuModel _model;
	
	/**
	 * Flag f�r den Timer
	 */
	private static Boolean flag = false;
	
	/**
	 * Flag f�r das aktuelle Sudoku.
	 */
	
	private boolean currentGame = false;
	
	/**
	 * Timerinstanz
	 * @see Timer
	 */
	
	private Timer timer = null;
	
	/**
	 * Sudokufeldgr��e
	 */
	private int size = Registry.get().getInt("size");
	
	/**
	 * Konstruktor
	 * 
	 * <p>Legt das Model, die View und die Timerinstanz fest.</p>
	 * 
	 * @param _model
	 * @param _view
	 * @param timer
	 */
	
	public SudokuFieldListener (SudokuModel _model, MainView _view, Timer timer) {
		this._model = _model;
		this._view = _view;
		this.timer = timer;
	}
	
	/**
	 * Setzt den Flag
	 * 
	 * @param bool
	 */
	
	public static void setFlag(boolean bool) {
		flag = bool;
	}
	
	/**
	 * Wird aufgerufen wenn sich ein Wert im Model �ndert.
	 * Dabei �bernimmt diese Funktion folgende Aufgaben:
	 * <li>Timer Starten & Stoppen</li>
	 * <li>Anzahl der gespielten Spiele erh�hen</li>
	 * <li>Gewonne und Verlorene Spiele erh�hen</li>
	 * <li>Highscore erstellen</lI>
	 * <li>Bestimmung der freien Felder</li>
	 */
	@Override
	public void valueChanged(ListSelectionEvent e) {
				int counter = 0; 
				
				
						
				for (int row = 0; row < _model.getRowCount(); row++) {
					for (int col = 0; col < _model.getColumnCount(); col++) {
						if(_model.getValueAt(row, col) != null ) {
							counter++;
						}
					}
				}
				
				
				if(flag==true && counter != 0) {
						//Neue Spiele erh�hen
				   Registry.get().set("playedgames", Registry.get().getInt("playedgames")+1);
				   currentGame = true;
				   if (timer.isStarted() == false) {
					   if(counter!=81) {
						   timer.start();  
					   }
					  
				   } else {
					   timer.resetTimer();
					   timer.resumeTimer();
					  
					   
				   }
					
				}
				
				if (currentGame == true && counter == (size*size)) {
					 if (timer.isStarted() == true) {
						 timer.setRunning(false);
					 }
					
					 Registry.get().set("wongames", Registry.get().getInt("wongames")+1);
					 currentGame = false;
					 if (!_view.getTime().endsWith("Millisekunden")) {
						 
					 do {_hview = new NewHighscoreView(null);
						 if(_hview.getName() == null || _hview.getName().equals("")) {
							 JOptionPane.showMessageDialog(null,"Geben Sie einen Namen ein!", "Fehler", JOptionPane.CANCEL_OPTION); 
						 }
					 } while (_hview.getName() == null || _hview.getName().equals(""));
					 		 
					 
						 
					 String name = _hview.getName();
					 String sudoku = _model.SudokuModeltoString();
					
					 long time = timer.getTime();
					 double type = Math.sqrt((double)sudoku.length());
					 
					 	if(Registry.get().getString("savetype").equals("text")) {
						 HighscoreXML hXML = new HighscoreXML("config/highscore.xml");
						 try {
								 hXML.addHighscore(sudoku, name, String.valueOf(time));
						} catch (XMLStreamException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					 	}
					 
					 	if(Registry.get().getString("savetype").equals("db")) {
					 		try {
								@SuppressWarnings("unused")
								HighscoreDatabase h = new HighscoreDatabase(name, sudoku, (int) type, time);
							} catch (InstantiationException
									| IllegalAccessException
									| ClassNotFoundException | SQLException e1) {
								// TODO Auto-generated catch block
								JOptionPane.showMessageDialog(null,e1.getMessage(), "Fehler", JOptionPane.CANCEL_OPTION);
								e1.printStackTrace();
							} 
					 	}
					 
					 }
					  
					   
				}
				
				if (counter == 0) {
					_view.setEmptyCells("Noch freie Felder: ");
				} else {
					_view.setEmptyCells("Noch freie Felder: " + ((size * size) - counter));
				}
				
				flag = false;		
	}

}
