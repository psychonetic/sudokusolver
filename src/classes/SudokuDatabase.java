package classes;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * <strong>Klasse SudokuDatabase</strong>
 * 
 * Diese Klasse verwaltet die Sudokus in 
 * einer Datenbank. Dabei k�nnen Sudokus abgefragt, hinzugef�gt, gel�scht und erstellt werden.
 * 
 * @see 	SudokuHandler
 * @author 	Marco Martens
 * @version 1.0 Oktober 2012
 *
 */
public class SudokuDatabase extends SQLWrapper {
	
	/**
	 * TypeID
	 */
	
	private int typeID;
	

	/**
	 * Konstruktor
	 * 
	 * <p>Ruft den Konstuktor von {@link SQLWrapper} auf und verbindet sich mit 
	 * der Datenbank.</p>
	 * 
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public SudokuDatabase() throws InstantiationException,
			IllegalAccessException, ClassNotFoundException, SQLException {
		super();
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * F�gt ein neues Sudoku der Datenbank hinzu.
	 * 
	 * @param sudoku
	 * @param type
	 * @throws SQLException 
	 */
	
	public void addSudoku (String sudoku, int type) throws SQLException {
		PreparedStatement preState = conn.prepareStatement("SELECT id FROM type WHERE type =?");
		preState.setInt(1, type);
		ResultSet result = preState.executeQuery();
		//Datensatz gefunden
		if (result.next()  ) {
			this.typeID = result.getInt("id");
		} else {
			//Sonst Datensatz hinzuf�gen
			preState = conn.prepareStatement("INSERT INTO type(type) VALUES (?)", Statement.RETURN_GENERATED_KEYS);
			preState.setInt(1, type);
			preState.executeUpdate();
			
			ResultSet generatedKeys = preState.getGeneratedKeys();
			if (generatedKeys.next()) {
				this.typeID = generatedKeys.getInt(1);
			}
		}
		
		preState = conn.prepareStatement("SELECT id FROM sudoku WHERE sudoku =?");
		preState.setString(1, sudoku);
		result = preState.executeQuery();
		//Kein Datensatz
		if (!result.next()  ) {
			preState = conn.prepareStatement("INSERT INTO sudoku(sudoku, t_id) VALUES (?,?)");
			preState.setString(1, sudoku);
			preState.setInt(2, this.typeID);
			preState.executeUpdate();
		} 
			
		
		
	}
	
	/**
	 * L�dt ein Sudoku aus der Datenbank und l�dt dessen Datensatz.
	 * 
	 * @param sudoku
	 * @return sudokuneu
	 */
	
	public String getSudoku(String sudoku) {
		String sudokuneu = "";
		try {
			PreparedStatement preState = conn.prepareStatement("SELECT id,sudoku FROM sudoku WHERE sudoku=?;");
			preState.setString(1, sudoku);
			ResultSet result = preState.executeQuery();
			while (result.next()) {
				sudokuneu = result.getString("sudoku");
			}
			preState.close();
			conn.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sudokuneu;
		
	}
	
	/**
	 * L�dt alle Sudokus aus der Datenbank.
	 * 
	 * 
	 * @return sudokus
	 */
	
	public List<String> getAllSudokus () {
		List<String>  sudokus = new ArrayList<String>();
		Statement state;
		try {
			state = conn.createStatement();
			ResultSet result = state.executeQuery("SELECT sudoku FROM sudoku");
			while (result.next()) {
				sudokus.add(result.getString("sudoku"));
			}
			state.close();
			conn.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sudokus;
		
		
	}
	
	/**
	 * 
	 * L�dt alle Sudokus einer bestimmten Gr��e.
	 * 
	 * @param type
	 * @return
	 */
	
	public List<String> getAllSudokusByType(int type) {
		List<String>  sudokus = new ArrayList<String>();
		try {
			PreparedStatement preState = conn.prepareStatement("SELECT id,sudoku FROM sudoku WHERE sudoku=?;");
			preState.setInt(1, type);
			ResultSet result = preState.executeQuery();
			while (result.next()) {
				sudokus.add(result.getString("sudoku"));
			}
			preState.close();
			conn.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sudokus;
	}
	
	/**
	 * L�scht ein Sudoku aus der Datenbank.
	 * 
	 * @param sudoku
	 * @return
	 */
	
	public boolean deleteSudoku(String sudoku) {
		try {
			PreparedStatement preState = conn.prepareStatement("DELETE FROM sudoku WHERE sudoku=?;");
			preState.setString(1, sudoku);
			
			if (preState.executeUpdate() > 0) {
				return true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
		
	}
}
