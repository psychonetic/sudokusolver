package classes;

import java.util.ArrayList;
import java.util.List;


/** 
 * <strong>Klasse BaseSolver</strong>
 * 
 * <p>Diese Klasse enth�lt elementare Methoden 
 * die ben�tigt werden um ein Sudoku zu l�sen.
 * Es sind Methoden enthalten um ein Sudoku zu
 * setzen, zur�ckzugeben und einzelne Zahlen
 * zu �berpr�fen ob sie an der Stelle [x][y]
 * m�glich sind.</p>
 * 
 * 
 * 
 * @author 	Marco Martens
 * @version 1.0 29.07.2012
 * 
 */
public class BaseSolver {

	protected int boxSize = (int) Math.sqrt((Registry.get().getInt("size")));
	
	
	/**
	 * Das Sudoku als 2D-Array.
	 *
	 */
	
	protected int[][] input;
	
	/**
	 * Konstruktor
	 * 
	 * <p>Setzt ein Sudoku beim Erstellen einer Instanz 
	 * der Klasse (Konstruktor).</p>
	 * 
	 * @param input 
	 */
	public BaseSolver (int[][] input) {
		this.input = input; 
	}
	
	public BaseSolver () {
		
	}
	
	
	 /**
	  * Gibt den Wert an der Stelle (Zeile/Spalte) zur�ck.
	  * 
	  * @param row
	  * @param col
	  * @return input[row][col]
	  */
	 public int getValueAt (int row, int col) {
			return this.input[row][col];
	}
	
	 /**
	  * Kopiert ein Sudoku-Array.
	  * 
	  * Diese Funktion wird ben�tigt f�r das
	  * Generieren von Sudokus.
	  * @see SudokuGenerator
	  * 
	  * 
	  * @param source
	  * @param target
	  * @return copySudoku
	  */
	 protected int[][] copyArray (int[][] source, int[][] target) {
			for (int row = 0; row < source.length; row++) {
				for (int col = 0; col < source[0].length; col++) {
					source[row][col]=target[row][col];
				}
			}
			return target;
	}
	 
	/**
	 * �berpr�ft die Zeile, ob die Zahl
	 * m�glich ist.
	 * 
	 * 
	 * @param 	row 
	 * @param 	value 
	 * @return 	boolean
	 */
	
	protected boolean checkRow (int row, int value) {
		
		for (int i = 0; i < input.length; i++) {
			if (this.input[row][i]==value) {
				return true;
			}
		}
		return false;
	}
	/**
	 * �berpr�ft die Spalte, ob die Zahl
	 * m�glich ist.
	 * 
	 * 
	 * @param 	col
	 * @param 	value 
	 * @return 	boolean
	 */
	
	protected boolean checkCol (int col, int value) {
		
		
		for (int i = 0; i < input[0].length; i++) {
			if (this.input[i][col] == value) {
				return true;
			}
		}
		return false;
	}
	/**
	 * �berpr�ft die 3x3 Box, ob die 
	 * Zahl m�glich ist.
	 * 
	 * @param 	row 
	 * @param 	col 
	 * @param 	value 
	 * @return 	boolean
	 */
	protected boolean checkBox (int row, int col, int value) {
		int gridX = row/boxSize * boxSize;
		int gridY = col/boxSize * boxSize;
		
		for (int i = gridX; i < gridX + boxSize; i++ ) {
			for (int j = gridY; j < gridY + boxSize; j++) {
				if (this.input[i][j] == value) {
					return true;
				}
			}
		}
		return false;
	}
	/**
	 * �berpr�ft ob die Zahl an der angegeben 
	 * Position([X][Y]) m�glich ist.
	 * 
	 * Dabei wird die Zeile, Spalte und die
	 * dazugeh�rige Box kontrolliert.
	 * 
	 * @see #checkRow(int, int)
	 * @see #checkCol(int, int)
	 * @see #checkBox(int, int, int)
	 * 
	 * 
	 * @param 	row
	 * @param 	col
	 * @param 	value
	 * @return 	boolean
	 */
	public boolean isPossible (int row, int col, int value) {
		if (this.checkRow(row, value)) {
			return true;
		}
		if (this.checkCol(col, value)) {
			return true;
		}
		if (this.checkBox(row, col, value)) {
			return true;
		}
		return false;
	}
	protected boolean checkSudokuSize(int size) {
		int counter = 0;
		for (int row = 0; row < input.length; row++) {
			for (int col = 0; col < input[0].length; col++) {
				if (this.input[row][col]==0) {
					counter++;
				}
			}
		}
		if (counter==size) {
			return true;
		}
		return false;
	}
	/**
	 * �berpr�ft ob das Sudoku gel�st ist.
	 * 
	 * Wenn alle Zellen ungleich 0 sind,
	 * dann ist das Sudoku gel�st.
	 * 
	 *  
	 * @return boolean
	 */
	public boolean isSolved() {
	
		for (int row = 0; row < input.length; row++) {
			for (int col = 0; col < input[0].length; col++) {
				if(this.input[row][col] == 0) {
					return false;
				}
			}
		}
		return true;
		
		
	}
	/**
	 * Gibt die Kandidaten zur�ck.
	 * 
	 * @param 	row
	 * @param 	col
	 * @return 	candidates
	 */
	
	public List<Integer> getCandidates (int row, int col) {
		
		List<Integer>  candidates = new ArrayList<Integer>();
		if (this.input[row][col] == 0) {
		for (int elements = 1; elements <= input.length; elements++) {
			candidates.add(elements);
		}
		
		for (int value = 1; value <= input.length; value++) {	
				if (this.isPossible(row, col, value)==true) {
					for (int i = 0; i < candidates.size(); i++) {
			    		if (candidates.get(i).equals(value)) {
			    			candidates.remove(i);
			    		}
			    	}
				}
			}
		}
		return candidates;
	}
	
	/**
	 * Gibt das ungel�ste/gel�ste Sudoku
	 * als Array wieder.
	 * 
	 * Info: Wird nur f�r Debugginausgaben ben�tigt, wenn 
	 * keine GUI vorhanden ist.
	 * 
	 * @return	input
	 */
	public void returnSudoku () {
		for ( int zeile = 0; zeile < this.input.length; zeile++ )
	    {
	      System.out.print("Zeile " + zeile + ": ");
	      for ( int spalte=0; spalte < this.input[zeile].length; spalte++ )
	        System.out.print( this.input[zeile][spalte] + " ");
	      System.out.println();
	    }
			
		
	}
	/**
	 * Gibt die aktuelle Boxgr��e zur�ck.
	 * 
	 * @return boxSize
	 */
	public int getBoxSize() {
		return this.boxSize;
	}
	
	public void setBoxSize(int size) {
		this.boxSize = size;
	}
	
	
	
	
	
	
	
}
