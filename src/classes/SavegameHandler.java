package classes;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import exception.SudokuException;


/**
 * <strong>Klasse SavegameHandler</strong>
 * 
 * <p>Diese Klasse verwaltet die Spielst�nde
 * der Sudokus in Textdateien.
 * Es ist m�glich neue Spielst�nde zu erstellen, zu l�schen 
 * oder zu laden.</p>
 * 
 * @see 	SavegameHandlerDatabase
 * @see 	FileHandler
 * @author 	Marco Martens
 * @version 1.0 Oktober 2012
 *
 */

public class SavegameHandler  extends FileHandler{
	
	/**
	 * Savegamefile
	 */
	private File savegamefile = null;
	
	/**
	 * Aktuelle Sudokufeldgr��e der GUI 
	 */
	
	private int size = Registry.get().getInt("size");
	
	/**
	 * Spielst�nde
	 */
	
	private HashMap<String, String> saves;
	
	/**
	 * Trennzeichen f�r Name:Sudoku 
	 */
	
	private final String SEPERATOR = ":";
	
	/**
	 * L�dt die Spielst�nde.
	 * 
	 * @param  savegamefile
	 * @return saves
	 * @throws Exception
	 * @throws SudokuException
	 */
	public HashMap<String, String> loadGame (File savegamefile) throws Exception, SudokuException {
		 List<String> savegames = new ArrayList<String>();
		 BufferedReader reader = null;
				
				if(openFile(savegamefile, false)) {
					reader = new BufferedReader( new FileReader(savegamefile)); 
					
					saves = new HashMap<String, String>();
					String line = null;
					while ((line = reader.readLine()) != null) {
							savegames.add(line);
						
						
						
					}
					reader.close();
				}
				for (int games = 0; games < savegames.size(); games++) {
					String tempgame = savegames.get(games);
					String [] temp = tempgame.split(":");
					if (temp[1].length() == ((size*size))) {
						saves.put(temp[0], temp[1]);
					}
					
				}
			
		return saves;
				
			
	}
	/**
	 * Speichert einen Spielstand.
	 * 
	 * @param savegamefile
	 * @param sudoku
	 * @param name
	 * @throws Exception
	 */
	
	public void saveGame (File savegamefile, String sudoku, String name) throws Exception {
		BufferedWriter writer = null;
		
		if (sudoku.length() != (size*size)) {
			throw new Exception("Das Sudoku ist ung�ltig");
		} 
		if (openFile(savegamefile, true)) {
			writer = new BufferedWriter( new FileWriter(savegamefile,  true));
			writer.write(name + SEPERATOR + sudoku);
			writer.write(System.getProperty("line.separator"));
			writer.flush();	
			
		}
		if (writer != null) {
			 writer.close();
		 } 
		
	}
	/**
	 * L�scht einen ausgew�hlten Spielstand.
	 * 
	 * @param name
	 * @param sudoku
	 * @return
	 * @throws Exception
	 */
	public boolean deleteGame (String name, String sudoku)  throws Exception{
		/*
		 * Lesen
		 * aktuelle Zeile != l�schende Zeile
		 * schreibe in tempdatei
		 * am Ende l�schen
		 * 
		 */
		savegamefile = new File("config/save.txt");
		File tempfile = new File("config/temp.txt"); 
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(tempfile))); 
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(savegamefile))); 
		if (openFile(savegamefile, true)) {
			
			String line;
			String search = name + ":" + sudoku;
			while ((line = reader.readLine()) != null) {
				
				if (!line.equals(search)) {
					tempfile.createNewFile();
					
					writer.write(line);
					writer.write(System.getProperty("line.separator"));
					writer.flush();	
					
				} 
				
								
			}
			writer.close();
		}
			reader.close();
		
			savegamefile.delete();
			if (!tempfile.renameTo(savegamefile)) {
				return false;
			}
			
			return true;
	}
	
	
	
}
