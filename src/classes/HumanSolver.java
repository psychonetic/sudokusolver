package classes;
import java.util.*;


/** 
 *  <strong>Klasse Human_Solver</strong>
 *  
 * 	<p>Diese Klasse l�st ein Sudoku, wie es ein Mensch 
 * 	tun w�rde. Dabei kommen verschiedene Verfahren zum
 * 	Einsatz, die in den folgenden Versionen
 * 	implentiert werden.
 * 	F�r den Anfang soll es nur m�glich sein,
 * 	eine Zahl zu setzen wenn sie eindeutig ist.</p>
 * 
 *  @see 		BacktrackSolver
 *  @see 		BaseSolver
 * 	@author 	Marco Martens
 * 	@version 	1.1
 * 
 */

public class HumanSolver extends BaseSolver {

	/**
	 * NakedSingle-Algorithmus 
	 */
	private boolean nakedSingle = true;
	
	/**
	 * HiddenSingle-Algorithmus 
	 */
	private boolean hiddenSingle = true;
	

	/**
	 * Konstruktor
	 * 
	 * <p>Legt das zu l�sende Sudoku fest und welche Algorithmen 
	 * verwendet werden sollen.</p>
	 * 
	 * @see Registry
	 */
	
	public HumanSolver (int[][] input) {
		super(input);
		this.nakedSingle = Boolean.parseBoolean(Registry.get().getString("nakedsingle"));
		this.hiddenSingle = Boolean.parseBoolean(Registry.get().getString("hiddensingle"));
	
	}
	
	/**
	 * L�st das Sudoku.
	 * 
	 * <p>Dabei werden bisher zwei Algorithmen verwendet: NakedSingle und HiddenSingle.
	 * Weitere Algorithmen werden in sp�teren Versionen noch implementiert</p>
	 * @return solvedSudoku
	 */
	
	public boolean solve () {
		for (int row = 0; row < input.length; row++) {
			for (int col = 0; col < input[0].length; col++) {
				if(nakedSingle == true) {
					if (findNakedSingle(row, col)==true) {
						return solve();
					}
				}
				if (hiddenSingle == true) {
					if (findHiddenSingle(row, col)  == true) {
						return solve();
					}
				}
			
				
			}
		}
		
		return false;
	}
	/**
	 * �berpr�ft ob die Zelle einen NakedSingle enth�lt.
	 * 
	 * @param row
	 * @param col
	 * @return boolean
	 */
	
	private boolean findNakedSingle (int row, int col) {
		List<Integer>  candidates = new ArrayList<Integer>();
		candidates = getCandidates(row, col);
		
		if (candidates.size() == 1) {
			int value = candidates.get(0);
			input[row][col] = value;
			return true;
		}
		return false;
		
	}
	/**
	 * �berpr�ft ob die Zelle einen HiddenSingle enth�lt.
	 * 
	 * In dieser Funktion gibt es zwar nur die Methoden {@link #alloneCandidateRow(int, int)} und 
	 * {@link #alloneCandidateCol(int, int)}
	 * dennoch funktioniert die Suche nach den HiddenSingles in den Boxen.
	 * 
	 * <p><strong>Deswegen wurde die Funktion alloneCandidateBox(int, int, value) wieder entfernt!</p></strong>
	 * 
	 * @see #alloneCandidateCol(int, int)
	 * @see #alloneCandidateRow(int, int)
	 * @see #isCandidate(int, int, int)
	 * @param row
	 * @param col
	 * @return
	 */
	private boolean findHiddenSingle (int row, int col) {
		for (int value = 1; value <= input.length; value++) {
			//Ein einzelner Kandidat vorhanden?
			if (alloneCandidateRow(col, value) == true) {
						
				for (int srow = 0; srow < input.length; srow++) {
					
					if(isCandidate(srow, col, value)==true) {
							input[srow][col] = value;
							return true;
						}
					}
				}
			
			if (alloneCandidateCol(row, value) == true) {
						
					for (int scol = 0; scol < input[0].length; scol++) {
						if(isCandidate(row, scol, value)==true) {
							input[row][scol] = value;
							return true;
							}
						}
					}
			
			if(alloneCandidateBox(row, col, value)==true) {
				int gridX = row/boxSize * boxSize;
				int gridY = col/boxSize * boxSize;
				
					for (int i = gridX; i < gridX + boxSize; i++ ) {
						for (int j = gridY; j < gridY + boxSize; j++) {
							if(isCandidate(i, j, value)==true) {
								input[i][j] = value;
								return true;
							}
						}
					}
							
				}
			}
		
			
		return false;
	}
	
	/**
	 * �berpr�ft das Vorkommen eines Kandidaten in einem Feld.
	 * 
	 * @param row
	 * @param col
	 * @param value
	 * @return boolean
	 */
	private boolean isCandidate (int row, int col, int value) {
		List<Integer>  candidates = new ArrayList<Integer>();
		candidates = getCandidates(row, col); // 7 [1,2,3,4]
		
		int counter = 0;
		for (int i = 0; i < candidates.size(); i++) {
			if (candidates.get(i).equals(value)==false) {
				//Erh�hen wenn Kandidat ungleich Zahl ist.
				counter = counter +1;
			} 
		}
		//Wenn der Counter der Kandidaten ist, dann ist es kein Kandidat.
		if (counter == candidates.size()) {
			return false;
		}
		return true;
		
		
	}
	/**
	 * �berpr�ft ob der Kandidat in einer Zeile alleine vorkommt.
	 * 
	 * Das Prinzip ist �hnlich der {@link isCandidate} Funktion.
	 * 
	 * @param row
	 * @param value
	 * @return boolean
	 */
	private boolean alloneCandidateCol (int row, int value) {
		int counter = 0;
		int tempcount = 0;
		
		for (int col = 0; col < input[0].length; col++) {
			
			if (input[row][col]==0) {
				
				tempcount = tempcount + 1;
				if (isCandidate(row, col, value)==false) { // [1,2,3]
					counter = counter + 1;
				}
			}
		}
		if (tempcount - counter == 1) {
			
			return true;
		}
		return false;
	}
	/**
	 * �berpr�ft ob der Kandidat in einer Zeile alleine vorkommt.
	 * 
	 * Das Prinzip ist �hnlich der {@link isCandidate} Funktion.
	 * 
	 * @param col
	 * @param value
	 * @return boolean
	 */
	private boolean alloneCandidateRow (int col, int value) {
		int counter = 0;
		int tempcount = 0;
		
		for (int row = 0; row < input.length; row++) {
			
			if (input[row][col]==0) {
				//Entspricht den leeren Feldern
				tempcount = tempcount + 1;
				if (isCandidate(row, col, value)==false) { // [1,2,3]
					//Wird erh�ht wenn es kein Kandidat ist.
					counter = counter + 1;
				}
			}	
		}
		//Wenn der Kandidat nur einmal vorkommt,
		//dann muss AnzahlLeererFelder - AnzahlKeinKandidat == 1 sein.
		if (tempcount - counter == 1) {
			
			return true;
		}
		return false;
	}
	/**
	 * �berpr�ft ob der Kandidat in einer Box alleine vorkommt.
	 * 
	 * Das Prinzip ist �hnlich der {@link isCandidate} Funktion.
	 * 
	 * @param row
	 * @param col
	 * @param value
	 * @return boolean
	 */
	private boolean alloneCandidateBox(int row, int col, int value) {
		int counter = 0;
		int tempcount = 0;
		
		int gridX = row/boxSize * boxSize;
		int gridY = col/boxSize * boxSize;
		
		for (int i = gridX; i < gridX + boxSize; i++ ) {
			for (int j = gridY; j < gridY + boxSize; j++) {
			
				if (input[i][j]==0) {
					tempcount = tempcount + 1;
					if (isCandidate(i, j, value)==false) { // [1,2,3]
						//Wird erh�ht wenn es kein Kandidat ist.
						counter = counter + 1;
					}
				}
			}
		}
		if (tempcount - counter == 1) {
			return true;
		}
		return false;
		
	}
}
	
	
	
	
	
	
	

