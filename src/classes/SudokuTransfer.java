package classes;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import exception.SudokuException;

/**
 * <strong>Klasse SudokuTransfer</strong>
 * 
 * <p>Diese Klasse ermöglicht es Sudokus zu importieren und zu exportieren.
 * Der Import und der Export kann von einer Textdatei zu einer Datenbank erfolgen
 * oder auch umkehrt.</p>
 * 
 * 
 * 
 * @see 	SudokuDatabase
 * @see		SudokuHandler
 * @author 	Marco Martens
 * @version	1.0 Oktober 2012 
 *
 */

public class SudokuTransfer {
	
	/**
	 * SudokuDatabaseinstanz 
	 */
	
	private SudokuDatabase db;
	
	/**
	 * SudokuHandlerinstanz 
	 */
	
	private SudokuHandler sh;
	
	/**
	 * Konstruktor 
	 * 
	 * <p>Erstellt die Sudokudatabaseinstanz und die SudokuHandlerinstanz.</p>
	 * 
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	
	public SudokuTransfer () throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		db = new SudokuDatabase();
		sh = new SudokuHandler();
	}
	
	/**
	 * Export die Sudokus in eine Textdatei.
	 * 
	 * @param file
	 * @throws SudokuException
	 * @throws IOException
	 */
	
	public void exportSudokusToText(File file) throws SudokuException, IOException {
		List<String>  sudokus = new ArrayList<String>();
		
		sudokus = db.getAllSudokus();
		
		for (int i = 0; i < sudokus.size(); i++) {
				sh.writeSudoku(file, sudokus.get(i));
			
		}
	}
	
	/**
	 * Importiert die Sudokus aus einer Textdatei.
	 * 
	 * @param file
	 * @throws IOException
	 * @throws SudokuException
	 * @throws SQLException 
	 */
	public void importSudokusToDatabase(File file) throws IOException, SudokuException, SQLException {
		List<String>  sudokus = new ArrayList<String>();
		
			sudokus = sh.readSudoku(file);
			for (int i = 0; i < sudokus.size(); i++) {
				
				db.addSudoku(sudokus.get(i),(int) Math.sqrt(sudokus.get(i).length()));
			}
		 
		
	}
}
