package classes;

import java.util.List;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/**
 * <strong>Klasse Chart</strong>
 * 
 * <p>Diese Klasse stellt die Ergebnisse des 
 * Benchmarks grafisch dar in einem Diagramm.
 * Dabei verwendet die Klasse jfreechart-1.0.14 zur Erstellung
 * des Diagrammes.</p>
 * 
 * <p>F�r weitere Informationen zu JFreeChart:
 * <a href="http://www.jfree.org/jfreechart/">JFreeChart</a></p>
 * @author Marco Martens
 * @version 1.0 Oktober 2012
 *
 */

public class Chart {
	/**
	 * X-Achse
	 */
	private String xAxe;
	/**
	 * Y-Achse
	 */
	private String yAxe;
	/**
	 * Name des Diagrammes
	 */
	private String chartName;
	/**
	 * Konstruktor
	 * 
	 * <p>Legt die X- und Y-Achse fest und den Namen des Diagrammes.</p>
	 * 
	 * @param xAxe
	 * @param yAxe
	 * @param chartName
	 */
	public Chart (String xAxe, String yAxe, String chartName) {
		this.xAxe = xAxe;
		this.yAxe = yAxe;
		this.chartName = chartName;
	}
	/**
	 * Erstellt das Panel mit dem Diagramm.
	 * 
	 * @param numberOfSudokus
	 * @param data
	 * @return panel
	 */
	public ChartPanel run(int numberOfSudokus, List<Long> data) {
		//Daten
		
		XYSeries series = new XYSeries(chartName);
		for (int i = 0; i < numberOfSudokus; i++) {
			series.add(i, data.get(i));
		}
		
		XYSeriesCollection dataset = new XYSeriesCollection();
		dataset.addSeries(series);
	
		//Plotten
		//XYPlot plot = new XYPlot(data, xAx, yAx, dot);
		JFreeChart chart = ChartFactory.createXYLineChart(
				xAxe, yAxe, chartName,  dataset, PlotOrientation.VERTICAL, true, true, false);
		
		//Diagramm fertigstellen
		ChartPanel panel = new ChartPanel(chart);
		
		return panel;
		
	}
}
