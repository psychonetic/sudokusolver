package classes;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

import exception.SudokuException;



/**
 * <strong>Klasse SudokuHandler</strong>
 * <p>Diese Klasse verwaltet die Sudokus in den txt-Dateien.
 * Es ist m�glich neue Sudokus zu speichern und zu laden. 
 * </P>
 * 
 * 
 * @see		FileHandler
 * @see		SudokuDatabase
 * @author 	Marco Martens
 * @version	1.0 Oktober 2012
 *
 */

public class SudokuHandler extends FileHandler  {
	
	/**
	 * L�dt die Sudokus aus einer Textdatei.
	 * 
	 * @param 	file
	 * @return 	sudokus
	 * @throws 	IOException
	 * @throws 	SudokuException
	 */
	public List<String> readSudoku (File file) throws IOException, SudokuException  {
		//Return List
		BufferedReader reader = null;
		String line = null;
		
		List<String>  sudokus = new ArrayList<String>();
		
			if (openFile(file, false)) {
				reader = new BufferedReader( new FileReader(file)); 
				
				while ((line = reader.readLine()) != null) {
					if (line.length() != 81) {
						reader.close();
						throw new SudokuException("Die Sudokus sind ung�ltig. Bitte �ndern Sie die Einstellungen oder w�hlen Sie eine andere Datei aus.");
					}
					
					
					//Neue Liste anlegen
					sudokus.add(line);
					
				}
				reader.close();
				
			}
			
			return sudokus;	
		}
		
	/**
	 * Schreibt ein Sudoku in eine Textdatei.
	 * 
	 * @param file
	 * @param Sudoku
	 * @throws SudokuException
	 * @throws IOException
	 */
	
	public void writeSudoku (File file, String Sudoku) throws SudokuException, IOException {
		BufferedWriter writer = null;
			if (Sudoku.length() < 0) {
				throw new SudokuException("Die Sudokus sind ung�ltig. Bitte �ndern Sie die Einstellungen oder w�hlen Sie eine andere Datei aus.");
			}
			if (openFile(file, true)) {
				writer = new BufferedWriter( new FileWriter(file,  true)); 
				writer.write(Sudoku);
				writer.write(System.getProperty("line.separator"));
				writer.flush();	
			}
			writer.close();	
	}
		 
}
