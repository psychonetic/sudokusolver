package classes;

import java.util.ArrayList;
import java.util.List;




/**<strong>Klasse SudokuGenerator</strong>
 * 
 * <p>Diese Klasse erm�glicht das Erstellen von komplett
 * neuen Sudokus. Dabei kann zwischen der Art der Erstellung unterschieden 
 * werden und wie schwer das Sudoku sein soll.</p>
 * 
 * Das Erstellen �bernehmen folgende Methoden:
 * <li>{@link #backtrackingRemover(int)}</li>
 * <li>{@link #humanRemover(int)}</li>
 * 
 * <p>Der Schwierigkeitsgrad wird durch die Anzahl der Zahlen im Sudoku bestimmt!</p>
 * 
 * @see		BaseSolver
 * @see 	BacktrackSolver
 * @see 	HumanSolver
 * @author 	Marco Martens
 * @version 1.0 Oktober 2012
 *
 */
public class SudokuGenerator extends BaseSolver  {
	
	/**
	 * BacktrackSolver Instanz
	 */
	
	private BacktrackSolver bSolver;
	
	/**
	 * HumanSolver Instanz
	 */
	
	private HumanSolver solver;	
	
	/**
	 * Gr��e des zu erstellenden Sudokus
	 */
	
	public int size;
	
	/**
	 * Art der Erstellung:
	 * <p>Human oder Backtracking</p>
	 */
	private String type = "";
	
	/**
	 * Flag ob das Sudoku fertig erstellt ist.
	 */
	
	private boolean generated = false;
	
	/**
	 * Konstruktor
	 * 
	 * <p>Legt die Gr��e des zu erstellenden Sudokus fest und die Art der Erstellung.</p>
	 * 
	 * @param type
	 * @param size
	 */
	
	public SudokuGenerator (String type, int size) {
		this.type = type;
		this.size = size;
	}
	
	/**
	 * Generiert ein Sudoku
	 * 
	 * <p>Das Sudoku wird komplett auf 0 gesetzt. Danach werden unabh�ngige Bl�cke, die sich also 
	 * nicht gegenseitig durch ihre Zahlen beeinflussen jeweils vollst�ndig ausgef�llt. Anschlie�end
	 * wird das Sudoku mit einem BacktrackSolver vollst�ndig gel�st.
	 * Nachdem das Sudoku gel�st ist, werden nach und nach Zahlen entfernt, bis das Sudoku
	 * die gew�nschte Gr��e erreicht hat. Das Entfernen erfolgt durch den {@link #backtrackingRemover(int)} oder
	 * durch den {@link #humanRemover(int)}.
	 * 
	 * <p>Zu guter letzt wird das Sudoku zur�ckgegeben.</p>
	 * 
	 * <p><strong>Achtung: Sudokus mit 17-22 Feldern k�nnen sehr lange brauchen bis sie erstellt werden!</strong></p>
	 * 
	 * @see  	#backtrackingRemover(int)
	 * @see  	#humanRemover(int)
	 * @see  	#prepareSudoku()
	 * @return 	sudoku
	 */ 
	
	public int[][] generate() {
		
		
		//3 Boxen f�llen
		
		//Zahlen entfernen
		while (generated == false) {
			input  = new int[9][9];
			prepareSudoku();
			if(this.type.equals("human")) {
				if (humanRemover(this.size)) {
					
					generated = true;
				}
			}
			if(this.type.equals("backtracking")) {
				if (backtrackingRemover(this.size)) {
					
					generated = true;
				}
			}
			
			
			
		}
		return this.input;
		
	}
	
	/**
	 * Entfernt die Zahlen ohne jegliche Logik.
	 * Die einzige Bedingung ist, dass das Sudoku nur eine 
	 * L�sung haben darf.
	 * 
	 * @see		#humanRemover(int)
	 * @see 	#generate()
	 * @param 	value
	 * @return 	boolean
	 */
	
	private boolean backtrackingRemover (int value) {
		
		bSolver = new BacktrackSolver(this.input);
		
		bSolver.solve(0, 0);
		
		//Kopie des Sudoku
		int[][] copySudoku = new int[9][9];
		
		copyArray(copySudoku, this.input);
		//hier
	
		int randomRow;
		int randomCol;
		int breaker = 0; 
		while (checkSudokuSize(value)==false) {
			breaker++;
			if (breaker == 200) {
				return false;
			}
			
			randomRow =  (int) (Math.random()*9);
			randomCol =  (int) (Math.random()*9);
			
			
			if (copySudoku[randomRow][randomCol] != 0) {		
				copySudoku[randomRow][randomCol]=0; 
				if (bSolver.getAllSolutions(2).size() == 1) {
					copyArray(this.input, copySudoku);
					breaker = 0;
				} 
			}
			
		}
		
		return true;
	}
	
	/**
	 * Entfernt die Zahlen unter der Bedingung, 
	 * dass sie durch den HumanSolver und dessen Algorithmen l�sbar sind.
	 * 
	 * @see 	#backtrackingRemover(int)
	 * @see  	#generate()
	 * @param 	value
	 * @return 	boolean
	 */
	
	private boolean humanRemover (int value) {
		solver = new HumanSolver(this.input);
		
		bSolver = new BacktrackSolver(this.input);
		bSolver.solve(0, 0);
		bSolver = null;
		
		//Kopie des Sudoku
		int[][] copySudoku = new int[9][9];
		
		copyArray(copySudoku, this.input);
		int randomRow;
		int randomCol;
		int breaker = 0; 
		while (true) {
			
			
			
			breaker++;
			if (breaker == 200) {
				return false;
			}
			if(checkSudokuSize(value)==true)  {
				return true;
			}
			randomRow =  (int) (Math.random()*9);
			randomCol =  (int) (Math.random()*9);
				
			if (input[randomRow][randomCol] != 0) {
				
				input[randomRow][randomCol]=0; 
				solver.solve();
				if(solver.isSolved()) {
					copySudoku[randomRow][randomCol]=0; 
					copyArray( this.input, copySudoku);
					breaker = 0;
					
				} else {
					copyArray(this.input, copySudoku);
				}
			}
		}
	
	}
	
	/**
	 * Bereitet das Sudoku vor und f�llt die ersten Zahlen in das Sudoku.
	 * 
	 * <p>Dabei werden zuf�llig 3 Boxen ausgew�hlt die sich nicht beeinflussen.
	 * Nachdem das Sudoku vorbereitet wurde, kann es gel�st werden.</p>
	 * 
	 * <p>Beispiele:</p>
	 * 
	 * <p>x|0|0</p>
	 * <p>0|x|0</p>
	 * <p>0|0|x</p>
	 * <br/>
	 * 
	 * <p>0|0|x</p>
	 * <p>0|x|0</p>
	 * <p>x|0|0</p>
	 * 
	 * @see #prepareBox(int, int)
	 * @see #generate()
	 * @return sudoku
	 */
	
	private int[][] prepareSudoku () {
		int randomRow =  (int) (Math.random()*9);
		int randomCol =  (int) (Math.random()*9);
		//Anzahl der bef�llten Bl�cke
		
		//Alte Werte Abspeichern
		
		List<Integer> forbiddenRows = new ArrayList<Integer>();
		List<Integer> forbiddenCols = new ArrayList<Integer>();
		
		int run = 0;
		
		prepareBox (randomRow, randomCol);
		
		forbiddenRows.add(randomRow);
		forbiddenCols.add(randomCol);
		while (run < 3) {
			
			
			if (randomRow <= 5) {
				randomRow = randomRow + 3;
			} else {
				randomRow = 0;
			}
			
			if (randomCol <= 5) {
				randomCol = randomCol + 3;
			} else {
				randomCol = 0;
			}
			
			for (int i = 0; i < forbiddenRows.size(); i++) {
				for (int j = 0; j < forbiddenCols.size(); j++) {
					if(isAllowedBlock(forbiddenRows.get(i), forbiddenCols.get(j), randomRow, randomCol)==true) {
						//Wenn erlaubter Block -> setzen
						prepareBox(randomRow, randomCol);
						run++;
						
					}
				}
			}
		}
		
		return this.input;
	}
	
	
	/**
	 * 
	 * F�llt eine Box
	 * 
	 * @see #prepareSudoku()
	 * @param row
	 * @param col
	 */
	
	private void prepareBox (int row, int col) {
		int gridX = row/3 * 3;
		int gridY = col/3 * 3;
		
		for (int i = gridX; i <= gridX + 2; i++ ) {
			for (int j = gridY; j <= gridY + 2; j++) {
				while(this.input[i][j]==0) {
					int randomValue =  (int) (Math.random()*9+1);
					if (isPossible(i, j, randomValue) == false) {
						this.input[i][j] = randomValue;
					}
				}
			}
		}
	}
	
	/**
	 * �berpr�ft ob die Box gef�llt werden darf.
	 * 
	 * @param x
	 * @param y
	 * @param rowNew
	 * @param colNew
	 * @return boolean
	 */
	private boolean isAllowedBlock(int x, int y, int rowNew, int colNew) {
		int row;
		int col;
		int gridX = x/3 * 3;
		int gridY = y/3 * 3;
		
		//Check Row
		for (col = gridY; col <= gridY + 2; col++) {
			if (colNew == col) {
				return false;
			}
		}
		
		//Check Col
		for (row = gridX; row <= gridX + 2; row++) {
			if (rowNew == row) {
					return false;
			}
		}
		
		return true;
		
	}
	
	
		
	
}
