package classes;
import java.io.File;

import javax.swing.filechooser.FileFilter;

/**
 * <strong>Klasse TxtFileFilter</strong>
 * 
 * <p>Diese Klasse wird f�r {@link view.SudokuFileChooserView} ben�tigt,
 *  um die m�glichen Dateien zu filtern die ausgew�hlt werden d�rfen. Beim �ffnen des Sudokus
 *  oder beim Speichern ist nur das Textformat erlaubt (.txt).</p>
 * 
 * @see view.SudokuFileChooserView
 * @author 	Marco Martens
 * @version 1.0 Oktober 2012
 */
public class TxtFileFilter extends FileFilter {

	/**
	 * Erlaubt nur TextDateien.
	 * 
	 */
	@Override
	public boolean accept(File file) {
		if (file.isDirectory()) {
			return true;
		}
		return file.getName().toLowerCase().endsWith(".txt");
		
	}
	
	/**
	 * Gibt die Bezeichnung zur�ck
	 * 
	 * @return "Textdateien"
 	 */
	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return "Textdateien";
	}

}
