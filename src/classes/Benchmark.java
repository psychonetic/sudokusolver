package classes;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import exception.SudokuException;

/**
 * <strong>Klasse Benchmark</strong>
 * 
 * <p>Diese Klasse l�st mehrere Sudokus hintereinander und
 * fasst die Daten in einem Diagramm zusammen.
 * Dabei werden folgende Ergebnisse dokumentiert:
 *  <li>Zeit pro Sudoku</li>
 *  <li>Durchschnittliche Zeit pro Sudoku</li>�
 *  <li>Gesamte Zeit f�r den alle Sudokus</li>
 *  <li>Versuche (Backtracks) pro Sudoku</li>
 *  <li>Durchschnittliche Versuche</li>
 *  <li>Gesamte Versuche aller Sudokus</li></p>
 *  
 * 
 * @author 	Marco Martens
 * @version 1.0 Oktober 2012
 *
 */
public class Benchmark {
	
	/**
	 * Sudokus
	 * 
	 * Enth�lt die zul�senden Sudokus.
	 */
	private List<String> sudokus;
	/**
	 * Anzahl
	 */
	public int numberOfSudokus = 0;
	
	/**
	 * Gesamtzeit
	 */
	public long completeTime = 0;
	
	/**
	 * Gesamtversuche
	 */
	public long completeBacktracks = 0;
	/**
	 * Durschnittszeit
	 */
	public long middleTime = 0;
	/**
	 * Durchschnittsversuche
	 */
	public long middleBacktracks = 0;
	/**
	 * Sudokudatei/quelle
	 * 
	 */
	public File sudokuFile = null;
	
	/**
	 * Backtracks
	 * 
	 * Enth�lt alle Backtracks der gel�sten Sudokus
	 */
	public List<Long> backtracks = new ArrayList<Long>();
	
	/**
	 * Zeiten
	 * 
	 * Enth�lt alle Zeiten der gel�sten Sudokus
	 */
	public List<Long> times = new ArrayList<Long>();
	
	/**
	 * 
	 * Konstruktor
	 * 
	 * <p>Legt die Datei fest, aus welcher 
	 * Sudokus gel�st werden sollen.</p>
	 * 
	 * @param sudokuFile
	 */
	public Benchmark (File sudokuFile) {
		this.sudokuFile = sudokuFile;
	}
	
	/**
	 * 
	 * Konstruktor
	 * 
	 */
	public Benchmark() {
		
	}
	
	/**
	 * Gibt alle Backtracks wieder.
	 * 
	 * @return backtracks
	 */
	public List<Long> getBacktracks () {
		return this.backtracks;
	}
	/**
	 * Gibt alle Zeiten wieder.
	 * 
	 * @return times
	 */
	public List<Long> getTimes () {
		return this.times;
	}
	/**
	 * Gibt die Gesamtanzahl der Versuche wieder.
	 * 
	 * @return completeBacktracks
	 */
	public long getCompleteBacktrack() {
		
		for (int i = 0; i < backtracks.size(); i++) {
			this.completeBacktracks = this.completeBacktracks + this.backtracks.get(i);
		}
		
		return this.completeBacktracks;
	}
	/**
	 * Gibt die Gesamtzeit wieder.
	 * 
	 * @return completeTime
	 */
	public long getCompleteTime() {
		for (int i = 0; i < times.size(); i++) {
			this.completeTime = this.completeTime + this.times.get(i);
		}
		
		return this.completeTime;
	}
	
	/**
	 * Gibt die durschnittliche Zeit wieder, die f�r
	 * ein Sudoku zum L�sen ben�tigt wurde.
	 * 
	 * @return middleTime
	 */
	public long getMiddleTime() {
		this.middleTime = (this.completeTime / this.numberOfSudokus);
		return this.middleTime;
	}
	/**
	 * Gibt die durchschnittliche Anzahl der Versuche wieder,
	 * die f�r ein Sudoku zum L�sen ben�tigt wurde.
	 * 
	 * @return
	 */
	public long getMiddleBacktrack() {
		
		this.middleBacktracks = (this.completeBacktracks / this.numberOfSudokus);
		return this.middleBacktracks;
	}
	
	/**
	 * Legt die Anzahl der Sudokus f�r den Benchmark fest.
	 * 
	 * @param number
	 */
	public void setElementNumber (int number) {
		this.numberOfSudokus = number;
	}
	/**
	 * Gibt die Anzahl der Sudokus f�r den Benchmark zur�ck.
	 * @return numberOfSudokus
	 */
	public int getElementNumber () {
		return this.numberOfSudokus;
	}
	
	/**
	 * L�dt die Sudokus f�r den Benchmark
	 * 
	 * Die Funtion l�dt die Sudokus aus einer Textdatei
	 * oder aus einer Datenbank.
	 * Die Sudokus werden anschlie�end durch 
	 * das Backtracking gel�st.
	 * 
	 * @return sudokus
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @throws IOException
	 * @throws SudokuException
	 */
	public List<String> getSudokus() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException, IOException, SudokuException {
		this.sudokus = new ArrayList<String>();
		if(Registry.get().getString("savetype").equals("text")) {
			SudokuHandler su = new SudokuHandler();
				this.sudokus = su.readSudoku(this.sudokuFile);
				
		}
		if (Registry.get().getString("savetype").equals("db")) {
			SudokuDatabase sDB = new SudokuDatabase();
			this.sudokus = sDB.getAllSudokus();
			
		}
		return this.sudokus;
	}
	
	/**
	 * Startet den Benchmark als Thread
	 * 
	 * 
	 * @see #SudokuStringtoArray(String)
	 * @return boolean
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @throws IOException
	 * @throws SudokuException
	 */
	
	public boolean run () throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException, IOException, SudokuException {
		getSudokus();
		if (this.numberOfSudokus <= this.sudokus.size()) {
			
		for (int solve = 0; solve < this.numberOfSudokus; solve++) {
			
			String strSudoku = this.sudokus.get(solve);
			int[][]newSudoku = SudokuStringtoArray(strSudoku);
			BacktrackSolver solver = new BacktrackSolver(newSudoku);
			
			
			long timenow = System.currentTimeMillis();
			solver.solve(0, 0);	
			long timebench = System.currentTimeMillis();
			this.backtracks.add(solver.getBacktracks());
			this.times.add(timebench-timenow);
		}
		} else {
			return false;
		}
		return true;
		
	}
	/**
	 * Wandelt die Sudokus in ein Array um.
	 * 
	 * Die Umwandlung muss erfolgen, da der {@link BacktrackSolver} 
	 * nur mit einem Array arbeiten kann.
	 * @param sudoku
	 * @return sudoku Gibt das Sudoku als Array zur�ck.
	 */
	private int[][] SudokuStringtoArray(String sudoku) {
		int[][]newSudoku = new int[9][9];
		
		String orgSudoku = sudoku;
		int stringend = orgSudoku.length();
		int end = 1;
		int start = 0;
		while (end <= stringend) {
			for (int row = 0; row < newSudoku.length; row++) {
				for (int col = 0; col < newSudoku[0].length; col++) {
					int value = Integer.parseInt(sudoku.substring(start, end));
					newSudoku[row][col] = value;
					
					
					end++;
					start++;
				}
			}
		}
		return newSudoku;
	}
}
