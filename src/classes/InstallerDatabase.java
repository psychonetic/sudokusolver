package classes;

import java.sql.PreparedStatement;
import java.sql.SQLException;
/**
 * <strong>Klasse InstallerDatabase</strong>
 * 
 * <p>Diese Klasse legt die ben�tigten Tabellen an
 * damit der Solver auf die Datenbank zugreifen kann.
 * Die Tabellen k�nnen mit dieser Klasse auch wieder entfernt werden.
 * Folgende Tabellen werden in der Datenbank angelegt:
 * <li>{@link #createSudokuTable()}</li>
 * <li>{@link #createTypeTable()}</li>
 * <li>{@link #createSaveGameTable()}</li>
 * <li>{@link #createHighscoreTable()}</li>
 * <li>{@link #createNameTable()}</li></p>
 * 
 * @see 	SQLWrapper
 * @author 	Marco Martens
 * @version 1.0 Oktober 2012
 *
 */
public class InstallerDatabase extends SQLWrapper {

	/**
	 * Konstruktor
	 * 
	 * <p>Ruft den Konstuktor von {@link SQLWrapper} auf und verbindet sich mit 
	 * der Datenbank.</p>
	 * 
	 * @param args
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 */
	
	public InstallerDatabase () throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		super();
	}
	
	/**
	 * Erstellt die Tabellen in der Datenbank.
	 * @see #createSudokuTable()
	 * @see #createTypeTable()
 	 * @see #createSaveGameTable()
	 * @see #createHighscoreTable()
 	 * @see #createNameTable()
	 * @throws SQLException
	 */
	
	public void install () throws SQLException {
		deinstall();
		createSudokuTable();
		createTypeTable();
		createHighscoreTable();
		createNameTable();
		createSaveGameTable();
		addRelations();
	}
	/**
	 * L�scht die Tabellen in der Datenbank und alle Datens�tze in den Tabellen.
	 * @see #createSudokuTable()
	 * @see #createTypeTable()
 	 * @see #createSaveGameTable()
	 * @see #createHighscoreTable()
 	 * @see #createNameTable()
	 * @throws SQLException
	 */
	public void deinstall () throws SQLException {
		deleteHighscoreTable();
		deleteTypeTable();
		deleteSudokuTable();
		deleteNameTable();
		deleteSaveGameTable();
	}
	
	/**
	 * Erstellt die Tabelle Type
	 * 
	 * @throws SQLException
	 */
	
	private void createTypeTable() throws SQLException {
		PreparedStatement preState = conn.prepareStatement("CREATE TABLE IF NOT EXISTS `type` (`id` int(11) NOT NULL AUTO_INCREMENT,`type` tinyint(2) NOT NULL,PRIMARY KEY (`id`),UNIQUE KEY `type` (`type`),UNIQUE KEY `id` (`id`)) ENGINE=InnoDB");
		preState.execute();
	}
	
	/**
	 * Erstellt die Tabelle Sudoku
	 * 
	 * @throws SQLException
	 */
	
	private void createSudokuTable() throws SQLException {
		PreparedStatement preState = conn.prepareStatement("CREATE TABLE IF NOT EXISTS `sudoku` (`id` int(11) NOT NULL AUTO_INCREMENT,`sudoku` varchar(625) NOT NULL, `t_id` int(11) NOT NULL, PRIMARY KEY (`id`),UNIQUE KEY `sudoku` (`sudoku`),UNIQUE KEY `id` (`id`)) ENGINE=InnoDB;");
		preState.execute();
	}
	/**
	 * Erstellt die Tabelle Highscore
	 * 
	 * @throws SQLException
	 */
	
	private void createHighscoreTable() throws SQLException {
		PreparedStatement preState = conn.prepareStatement("CREATE TABLE IF NOT EXISTS `highscore` ( `id` int(11) NOT NULL AUTO_INCREMENT,`s_id` int(11) NOT NULL,`n_id` int(11) NOT NULL,`time` float NOT NULL,PRIMARY KEY (`id`),KEY `s_id` (`s_id`),KEY `n_id` (`n_id`)) ENGINE=InnoDB");
		preState.execute();
	}
	/**
	 * Erstellt die Tabelle Names
	 * 
	 * @throws SQLException
	 */
	
	private void createNameTable() throws SQLException {
		PreparedStatement preState = conn.prepareStatement("CREATE TABLE IF NOT EXISTS `name` (`id` int(11) NOT NULL AUTO_INCREMENT,`name` varchar(80) NOT NULL, PRIMARY KEY (`id`),UNIQUE KEY `name` (`name`),UNIQUE KEY `id` (`id`)) ENGINE=InnoDB");
		preState.execute();
	}
	/**
	 * Erstellt die Tabelle Savegames
	 * 
	 * @throws SQLException
	 */
	
	private void createSaveGameTable() throws SQLException {
		PreparedStatement preState = conn.prepareStatement("CREATE TABLE IF NOT EXISTS `savegame` (`id` int(11) NOT NULL AUTO_INCREMENT,`sudoku` varchar(625) NOT NULL,`name` VARCHAR(80) NOT NULL,PRIMARY KEY (`id`), UNIQUE KEY `id` (`id`)) ENGINE=InnoDB");
		preState.execute();
	}
	/**
	 * L�scht die Tabelle Type
	 * 
	 * @throws SQLException
	 */
	private void deleteTypeTable() throws SQLException {
		PreparedStatement preState = conn.prepareStatement("DROP TABLE IF EXISTS `type`");
		preState.execute();
	}
	
	/**
	 * L�scht die Tabelle Sudoku
	 * 
	 * @throws SQLException
	 */
	
	private void deleteSudokuTable() throws SQLException {
		PreparedStatement preState = conn.prepareStatement("DROP TABLE IF EXISTS `sudoku`");
		preState.execute();
		
	}
	
	/**
	 * L�scht die Tabelle Name
	 * 
	 * @throws SQLException
	 */
	
	private void deleteNameTable() throws SQLException{
		PreparedStatement preState = conn.prepareStatement("DROP TABLE IF EXISTS `name`");
		preState.execute();
	}
	
	/**
	 * 
	 * L�scht die Tabelle Savegames
	 * 
	 * @throws SQLException
	 */
	
	private void deleteSaveGameTable() throws SQLException{
		PreparedStatement preState = conn.prepareStatement("DROP TABLE IF EXISTS `savegame`");
		preState.execute();
	}

	/**
	 * L�scht die Tabelle Highscores
	 * 
	 * @throws SQLException
	 */
	
	private void deleteHighscoreTable() throws SQLException{
		PreparedStatement preState = conn.prepareStatement("DROP TABLE IF EXISTS `highscore`");
		preState.execute();
	}
	
	/**
	 * F�gt die Beziehungen zwischen den Tabellen hinzu.
	 * 
	 * @throws SQLException
	 */
	private void addRelations() throws SQLException {
		PreparedStatement preState = conn.prepareStatement("ALTER TABLE `highscore`ADD CONSTRAINT `highscore_ibfk_1` FOREIGN KEY (`s_id`) REFERENCES `sudoku` (`id`),ADD CONSTRAINT `highscore_ibfk_2` FOREIGN KEY (`n_id`) REFERENCES `name` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;");
		preState.execute();
	}

}
