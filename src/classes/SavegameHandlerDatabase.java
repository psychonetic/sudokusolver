package classes;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * <strong>Klasse SaveGameHandlerDatabase</strong>
 * 
 * <p>Diese Klasse erm�glicht es, die Spielst�nde
 * �ber die Datenbank zu verwalten.
 * Es ist m�glich Spielst�nde zu laden, l�schen oder neue zu erstellen.</p>
 * 
 * 
 * @see 	SQLWrapper
 * @see 	SavegameHandler
 * @author 	Marco Martens
 * @version 1.0 Oktober 2012
 *
 */
public class SavegameHandlerDatabase extends SQLWrapper {

	/**
	 * Konstruktor
	 * 
	 * <p>Ruft den Konstuktor von {@link SQLWrapper} auf und verbindet sich mit der Datenbank.</p>
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	
	public SavegameHandlerDatabase() throws InstantiationException,
			IllegalAccessException, ClassNotFoundException, SQLException {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * L�dt einen Spielstand.
	 * 
	 * @param name
	 * @return sudoku
	 */
	public String loadGame (String name) {
		
		String sudokuneu = null;
		try {
			PreparedStatement preState = conn.prepareStatement("SELECT sudoku FROM savegame WHERE name=?;");
			preState.setString(1, name);
			ResultSet result = preState.executeQuery();
			while (result.next()) {
				sudokuneu = result.getString("sudoku");
			}
			preState.close();
			conn.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sudokuneu;
	}
	
	/**
	 * Speichert ein Sudoku
	 * 
	 * @param sudoku
	 * @param name
	 */
	
	public void saveGame (String sudoku, String name) {
		try {
			PreparedStatement preState = conn.prepareStatement("INSERT INTO savegame(sudoku, name) VALUES (?, ?)");
			preState.setString(1, sudoku);
			preState.setString(2, name);
			preState.executeUpdate();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * L�scht ein Sudoku.
	 * 
	 * @param name
	 * @return boolean
	 */
	
	public boolean deleteGame (String name) {
		try {
			PreparedStatement preState = conn.prepareStatement("DELETE FROM savegame WHERE name=?;");
			preState.setString(1, name);
			
			if (preState.executeUpdate() > 0) {
				return true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	/**
	 * L�dt alle Savegames aus der Datenbank.
	 * 
	 * @return savegames
	 */
	
	public HashMap<String, String> getAllSavegames() {
		HashMap<String, String> sudokus = new HashMap<String, String>();
		try {
			String name = "";
			String sudoku = "";
			PreparedStatement preState = conn.prepareStatement("SELECT sudoku, name FROM savegame");
			ResultSet result = preState.executeQuery();
			while (result.next()) {
				name = result.getString("name");
				sudoku = result.getString("sudoku");
				sudokus.put(name, sudoku);
			}
			preState.close();
			conn.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sudokus;
	}
	
}
