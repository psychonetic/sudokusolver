package view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.DefaultTableModel;
import javax.xml.stream.XMLStreamException;

import model.HighscoreModel;

import classes.HighscoreDatabase;
import classes.HighscoreXML;
import classes.Registry;
import classes.ComboBoxRenderer;

/**
 * <strong>Klasse HighscoreView</strong>
 * 
 * <p>Zeigt den HighscoreView an</p>
 * 
 * 
 * @see		controller.HighscoreController
 * @author 	Marco Martens
 * @version 1.0 Dezember 2012
 *
 */

public class HighscoreView extends JDialog {

	private JButton exit;
	private JButton reset;
	private JPanel main;
	private JPanel buttons;
	private JTable highscoreTable;
	private DefaultTableModel _model;
	private JScrollPane scroll;
	
	
	private JComboBox<String> dropDown;
	private ComboBoxModel<String> comboModel;
	
	
	private List<HighscoreModel> sudokus = new ArrayList<HighscoreModel>();
	/**
	 * 
	 */
	private static final long serialVersionUID = 2490615452641393317L;

	
	public HighscoreView(Frame parent, DefaultTableModel _model) throws InstantiationException, IllegalAccessException, ClassNotFoundException, XMLStreamException, IOException, SQLException {
		//init
		super(parent, "Highscore", true);
		this._model = _model;
		initComponents();
	
		
		
	}

	public Object getSelectedItem () {
		return this.comboModel.getSelectedItem();
	}
	
	public void resetView() {
		// TODO Auto-generated method stub
		for (int row = 0; row < _model.getRowCount(); row++) {
			for (int col = 0; col < _model.getColumnCount(); col++) {
				if (col != 0) {
					_model.setValueAt("", row, col);
				}
				
			}
		}
		
		
	}

	public void initComponents() throws XMLStreamException, IOException, InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		setSize(400,300);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		main = new JPanel(new BorderLayout(5,5));
		buttons = new JPanel(new FlowLayout());
		
		highscoreTable = new JTable();
		scroll = new JScrollPane(highscoreTable);
		highscoreTable.setEnabled(false);
		highscoreTable.setModel(this._model );
		highscoreTable.setShowGrid(false);
		for (int i = 1; i <= 10; i++) {
			int tmpi = i -1;
			
			_model.setValueAt(i, tmpi, 0);
		}
		exit = new JButton("Ok");
		reset = new JButton("Zurücksetzen");
		dropDown = new JComboBox<String>();
	
		
		buttons.add(exit);
		buttons.add(reset);
		
		if(Registry.get().getString("savetype").equals("db")) {
			HighscoreDatabase hDb = null;
			
			hDb = new HighscoreDatabase();
		
		
			
			sudokus = hDb.getAllHighscores();
			
		} 
		
		if(Registry.get().getString("savetype").equals("text")) {
		HighscoreXML hXML = new HighscoreXML("config/highscore.xml");
			for (int s = 0; s < hXML.getHighscores().size(); s++) {
				sudokus = hXML.getHighscores();
				//sudokus.add(sudokusXML.get(s).getSudoku());
			}
		}
		scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		highscoreTable.getColumnModel().getColumn(0).setHeaderValue("Platz");
		highscoreTable.getColumnModel().getColumn(1).setHeaderValue("Spieler");
		highscoreTable.getColumnModel().getColumn(2).setHeaderValue("Zeit");
		Vector<String> sudokuVec = new Vector<String>();
		for (int i = 0; i < sudokus.size(); i++) {
			if(!sudokuVec.contains(sudokus.get(i).getSudoku())) {
				
				sudokuVec.add(sudokus.get(i).getSudoku());
				
			}
			
		}
			
		comboModel = new DefaultComboBoxModel<String>(sudokuVec);
		dropDown.setRenderer(new ComboBoxRenderer());
		dropDown.setModel(comboModel);
		comboModel.setSelectedItem("Bitte auswählen");
		main.add(BorderLayout.NORTH, dropDown);
		main.add(BorderLayout.SOUTH, buttons);
		main.add(BorderLayout.CENTER, scroll);
		getContentPane().add(main);
		
	}

	public void setResetListener(ActionListener e) {
		// TODO Auto-generated method stub
		reset.addActionListener(e);
	}
	public void setExitListener(ActionListener e) {
		exit.addActionListener(e);
	}
	public void setDropDownListener (ActionListener e) {
		this.dropDown.addActionListener(e);
	}
	

	
}
