package view;
import javax.swing.*;
import javax.swing.event.ListSelectionListener;
import classes.SudokuRenderer;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.WindowAdapter;

import model.SudokuModel;

/**
 * <strong>Klasse MainView</strong>
 * 
 * <p>Diese Klasse ist die Hauptview,
 * auf der alle anderen Views ihren Platz finden bzw. 
 * von hier aufgerufen werden.</p>
 * 
 * 
 * @see		AboutView
 * @see		BenchView
 * @see		HighScoreView
 * @see		LicenceView
 * @see		NewHighscoreView
 * @see		NewSaveGameView
 * @see		SettingsView
 * @see		StartBenchmarkView
 * @see		StatisticView
 * @see		SudokuFileChooserView
 * @see		TransferView
 * 
 * @author 	Marco Martens
 * @version 1.0 Dezember 2012
 *
 */


public class MainView extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8077690235152382078L;
	//Menuleiste
	private JMenuBar menu;
	//Menuleisten Elemente
	private JMenu datei;
	private JMenu savegame;
	private JMenu generator;
	private JMenu benchmark;
	private JMenu help;
	private JMenu highscore;
	private JMenu statistic; 
	//Datei - Elemente
	private JMenuItem openSudoku;
	private JMenuItem newSudoku;
	private JMenuItem saveSudoku;
	private JMenuItem fastLoading;
	private JMenuItem transfer;
	private JMenuItem properties;
	private JMenuItem close;
	//Highscore - Elemente
	private JMenuItem showHighscore;
	//Generieren - Elemente
	private JMenuItem generate;	
	//Spielstand - Elemente
	private JMenuItem load;
	private JMenuItem save;
	//Benchmark - Elemente
	private JMenuItem test;
	
	//Statistic Elemente
	private JMenuItem showStatistic;
	//�ber - Elemente
	private JMenuItem about;
	private JMenuItem licence;
	
	
	//Panels
	private JPanel ButtonPanel;
	private JPanel OutputPanel;
	private JPanel helpPanel;
	//Labels
	private JLabel backtracks;
	private JLabel time;
	private JLabel emptyCells;
	//Buttons
	private JButton backtracking;
	private JButton solve;
	private JButton tipp;
	//Tabelle/SudokuFeld
	private JTable sudokuTable;
	
	public static void main (String[] args) {
		@SuppressWarnings("unused")
		MainView g = new MainView();
		
	}
	
	public MainView() {
		super("Sudoku Solver");
		initComponents();
		
	}
	
	public void resetView() {
		// TODO Auto-generated method stub
		backtracks.setText("Anzahl der Versuche: ");
		time.setText("Ben�tigte Zeit: "); 
		emptyCells.setText("Noch freie Felder: ");
		for (int row = 0; row < sudokuTable.getRowCount(); row++) {
			for (int col = 0; col < sudokuTable.getColumnCount(); col++) {
				sudokuTable.setValueAt(null, row, col);
			}
		}
		
		revalidate();
		repaint();
	}


	public void initComponents() {
		// TODO Auto-generated method stub
		//Gr��e
				setSize(800, 640);
				//In der Mitte des Bildschirms zentrieren
				setLocationRelativeTo(null);
				//Exit-Button = Schlie�en des Programms
				setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
				//BorderLayout
				getContentPane().setLayout(new BorderLayout(5,5));
				//Menuleiste erstellen
				menu = new JMenuBar();
				
				//Menuelemente erstellen
				datei		= new JMenu("Datei");
				savegame 	= new JMenu("Spielstand");
				help 		= new JMenu("Hilfe");
				benchmark 	= new JMenu("Benchmark");
				generator   = new JMenu("Generator");
				highscore   = new JMenu("Highscore");
				statistic   = new JMenu("Statistik");
				
				sudokuTable = new JTable(9,9);
			
				sudokuTable.setDefaultRenderer(Object.class,  new SudokuRenderer());
				//Labels
				backtracks  = new JLabel ("Anzahl der Versuche: ");
				time 		= new JLabel ("Ben�tigte Zeit: ");
				emptyCells  = new JLabel("Noch freie Felder: ");
				
		
				
				sudokuTable.setRowHeight(55);
				
				
				
				
				
				//Unterelemente erstellen
				//Datei Elemente
				openSudoku = new JMenuItem("Sudoku �ffnen");
				openSudoku.setAccelerator(KeyStroke.getKeyStroke('O', InputEvent.CTRL_MASK));
				
				fastLoading = new JMenuItem("Schnelles Laden");
				fastLoading.setAccelerator(KeyStroke.getKeyStroke( 'c'));
				
				close = new JMenuItem("Beenden");
				
				saveSudoku = new JMenuItem("Sudoku speichern");
				saveSudoku.setAccelerator(KeyStroke.getKeyStroke('S', InputEvent.CTRL_MASK));
				
				newSudoku = new JMenuItem("Neu");
				newSudoku.setAccelerator(KeyStroke.getKeyStroke('N', InputEvent.CTRL_MASK));
				properties = new JMenuItem("Einstellungen");
				transfer = new JMenuItem("Import/Export");
				load = new JMenuItem("Laden");
				save = new JMenuItem("Speichern");
				
				
				//Generator - Elemente
				generate = new JMenuItem("Sudoku generieren");

			
				//Highscore Elemente
				showHighscore = new JMenuItem("Highscore");
				//Statistic Elemente
				showStatistic = new JMenuItem("Statistik");
				//Benchmark Elemente
				test = new JMenuItem("Benchmark starten");
				
				
				//�ber Elemente
				about = new JMenuItem("�ber");
				licence = new JMenuItem("Lizenz");
			
				
				//Menus zum Fenster hinzuf�gen
				menu.add(datei);
				menu.add(savegame);
				menu.add(generator);
				menu.add(benchmark);
				menu.add(highscore);
				menu.add(statistic);
				menu.add(help);
				//Untermen�s hinzuf�gen
				datei.add(newSudoku);
				datei.add(openSudoku);
				datei.add(fastLoading);
				datei.add(saveSudoku);
				datei.add(transfer);
				datei.add(properties);
				datei.add(close);
				
				savegame.add(load);
				savegame.add(save);
				
				generator.add(generate);
				
				benchmark.add(test);
				
				highscore.add(showHighscore);
				
				statistic.add(showStatistic);
				
				help.add(licence);
				help.add(about);
				
				
				//Panel
				ButtonPanel = new JPanel (new FlowLayout(2));
				OutputPanel = new JPanel (new GridLayout(2,1));
				helpPanel   = new JPanel(new BorderLayout(5,5));
				//Sudoku Feld
				//Buttons/Listener5
				solve		= new JButton("L�sen");
				backtracking = new JButton("Backtracking");
				tipp = new JButton("Tipp");
				//Buttons hinzuf�gen
				ButtonPanel.add(solve);
				ButtonPanel.add(backtracking);
				ButtonPanel.add(tipp);
				//Labels hinzuf�gen
				OutputPanel.add(backtracks);
				OutputPanel.add(time);
				OutputPanel.add(emptyCells);
				//Positionen festlegen
				helpPanel.add(BorderLayout.NORTH,OutputPanel);
				helpPanel.add(BorderLayout.SOUTH,ButtonPanel);
				
				getContentPane().add(BorderLayout.NORTH, menu);
				getContentPane().add(BorderLayout.CENTER, sudokuTable);
				getContentPane().add(BorderLayout.SOUTH, helpPanel);
	}	
	public void setTableModel(SudokuModel model) {
		sudokuTable.setModel(model);
	}
	
	public void setSudokuFieldListener (ListSelectionListener selectionListener) {
		sudokuTable.getSelectionModel().addListSelectionListener(selectionListener);
		
	}
	
	public void setBacktracks(String backtracks) {
		this.backtracks.setText(backtracks);
	}
	public  String getBacktracks (String backtracks)  {
		return this.backtracks.getText();
	}
	public void setTime(String time) {
		this.time.setText(time);
	}
	public String getTime() {
		return this.time.getText();
	}
	public void setEmptyCells(String emptyCells) {
		this.emptyCells.setText(emptyCells);
	}
	public void setOpenSudokuListener (ActionListener e) {
		this.openSudoku.addActionListener(e);
	}
	
	public void setNewSudokuListener (ActionListener e) {
		this.newSudoku.addActionListener(e);
	}
	
	public void setSaveSudokuListener (ActionListener e) {
		this.saveSudoku.addActionListener(e);
	}
	
	public void setTransferListener(ActionListener e) {
		this.transfer.addActionListener(e);
	}
	
	public void setFastLoadingListener (ActionListener e) {
		this.fastLoading.addActionListener(e);
	}
	
	public void setPropertiesListener (ActionListener e) {
		this.properties.addActionListener(e);
	}
	
	public void setExitListener (ActionListener e) {
		this.close.addActionListener(e);
	}
	
	public void setShowHighscoreListener (ActionListener e) {
		this.showHighscore.addActionListener(e);
	}
	
	public void setGenerateListener (ActionListener e) {
		this.generate.addActionListener(e);
	}
	
	public void setLoadListener (ActionListener e) {
		this.load.addActionListener(e);
	}
	
	public void setSaveListener (ActionListener e) {
		this.save.addActionListener(e);
	}
	
	public void setTestListener (ActionListener e) {
		this.test.addActionListener(e);
	}
	
	public void setShowStatisticListener (ActionListener e) {
		this.showStatistic.addActionListener(e);
	}
	
	public void setAboutListener (ActionListener e) {
		this.about.addActionListener(e);
	}
	
	public void setHumanListener (ActionListener e) {
		this.solve.addActionListener(e);
	}
	
	public void setLicenceListener (ActionListener e) {
		this.licence.addActionListener(e);
	}
	
	public void setBacktrackingListener (ActionListener e) {
		this.backtracking.addActionListener(e);
	}
	
	public void setTippListener (ActionListener e) {
		this.tipp.addActionListener(e);
	}

	public int getSelectedRow() {
		return sudokuTable.getSelectedRow();
	}

	public int getSelectedColumn() {
		return sudokuTable.getSelectedColumn();
	}
	public void setWindow(WindowAdapter windowsAdapter) {
		this.addWindowListener(windowsAdapter);
	}

	
}