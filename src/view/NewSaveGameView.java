package view;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * <strong>Klasse NewSaveGameView</strong>
 * 
 * <p>Dieser View wird dargestellt,
 * wenn der User seinen aktuellen Spielstand speichern m�chte.</p>
 * 
 * @see		SaveGameView
 * @author 	Marco Martens
 * @version 1.0 Dezember 2012
 *
 */


public class NewSaveGameView  extends JDialog {

	private static final long serialVersionUID = -6442640538688112109L;
	private JButton exit;
	private JButton save;
	
	private JTextField savegame;
	private JPanel buttons;
	private JLabel info;
	
	public NewSaveGameView (Frame parent) {
		//init
		super(parent, "Spielst�nde", true);
		initComponents();
	}
	
	public String getSaveGame() {
		return savegame.getText();
	}

	public void setSaveListener (ActionListener e) {
		this.save.addActionListener(e);
	}
	public void setExitListener (ActionListener e) {
		this.exit.addActionListener(e);
	}

	
	public void initComponents() {
		// TODO Auto-generated method stub
		setSize(400,125);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		//Panel
		buttons = new JPanel(new FlowLayout(4));
		//Textfeld
		savegame = new JTextField("");
		//Label
		info = new JLabel("Geben Sie einen Spielstand ein.");
		//Buttons
		exit = new JButton("Abbrechen");
		save = new JButton("Speichern");
		//Buttons auf das Panel hinzuf�gen
		buttons.add(save);
		buttons.add(exit);
		getContentPane().setLayout(new BorderLayout(5,5));	
		getContentPane().add(BorderLayout.NORTH, info);
		getContentPane().add(BorderLayout.CENTER, savegame);
		getContentPane().add(BorderLayout.SOUTH, buttons);
	}
	
}
