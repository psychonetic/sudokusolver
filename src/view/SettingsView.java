package view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;

import classes.Registry;

/**
 * <strong>Klasse SettingsView</strong>
 * 
 * <p>Zeigt ein umfangreiches Einstellungsmenu an
 * welches sich in mehrere Tabs gliedert, sodass
 * der User die �bersicht beh�lt.</p>
 * 
 * @see		controller.SettingsController
 * @author 	Marco Martens
 * @version 1.0 Dezember 2012
 *
 */

public class SettingsView extends JDialog implements ActionListener, ItemListener {

	final String SMALL = "4";
	final String MIDDLE = "9";
	final String BIG = "16";
	final String HUMAN = "human";
	final String BACKTRACKING = "backtracking";
	final String EASY = "easy";
	final String NORMAL = "normal";
	final String HARD = "hard";
	final String HELL = "hell";
	final String TEXT = "text";
	final String DATABASE = "db";
	
	
	private JPanel main;
	private JPanel buttons;
	
	private JPanel sudoku;
	private JPanel file;
	private JPanel sudokuGameSettings;
	private JPanel databaseSettings;
	private JPanel benchmarkSettings;
	private JPanel solverSettings;
	private JPanel backtrackingSolverSettings;
	private JPanel humanSolverSettings;
	private JPanel generatorSettings;
	
	private JButton save;
	private JButton exit;
	private JButton reset;
	
	private JButton searchFastFile; //JFileChooser
	private JButton searchBenchmarkFile; //JFileChooser
	
	private JButton installTables; //Meldung
	private JButton deinstallTables; //Meldung
	
	
	private JLabel hostLabel;
	private JLabel dbLabel;
	private JLabel userLabel;
	private JLabel pwLabel;
	private JLabel tableLabel;
	private JLabel dummyLabel;
	private JLabel fastLabel;
	private JLabel bMarckLabel;
	private JLabel sizeLabel;
	private JLabel typeLabel;
	private JLabel backtrackingLabel;
	private JLabel humanLabel;
	private JLabel generatorLabel;
	private JLabel gradeLabel;
	
	
	
	
	private JTextField host;
	private JTextField user;
	private JPasswordField password;
	private JTextField db;
	private JTextField fastLoadingFile;
	private JTextField benchmarkFile;
	
	
	private ButtonGroup size;
	private ButtonGroup type;
	private ButtonGroup grade;
	private ButtonGroup generator;
	
	private JCheckBox showSolve;
	private JCheckBox smartSolve;
	private JCheckBox hiddenSingle;
	private JCheckBox nakedSingle;
	
	private JRadioButton humanGenerate;
	private JRadioButton backtrackingGenerate;
	
	private JRadioButton small;
	private JRadioButton middle;
	private JRadioButton big;
	
	private JRadioButton text;
	private JRadioButton database;
	
	private JRadioButton easy;
	private JRadioButton normal;
	private JRadioButton hard;
	private JRadioButton hell;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -9010142412820061773L;

	
	
	public SettingsView(Frame parent) {
		super(parent, "Einstellungen", true);
		initComponents();
		
	}
	
	public String getUser() {
		return this.user.getText();
	}
	public void setUser (String user) {
		this.user.setText(user);
	}
	public char[] getPassword () {
		return this.password.getPassword();
	}
	public void setPassword (String password) {
		this.password.setText(password);
	}
	public String getHost () {
		return this.host.getText();
	}
	public void setHost (String host) {
		this.host.setText(host);
	}
	public String getDatabase () {
		return this.db.getText();
	}
	public void setDatabase (String db) {
		this.db.setText(db);
	}
	public String getSudokuFile () {
		return this.fastLoadingFile.getText();
	}
	public void setSudokuFile (String sudokuFile) {
		this.fastLoadingFile.setText(sudokuFile);
	}
	public String getBenchmarkFile () {
		return this.benchmarkFile.getText();
	}
	public void setBenchmarkFile (String benchmarkFile) {
		this.benchmarkFile.setText(benchmarkFile);
	}
	public String getSudokuSize() {
		if (this.small.isSelected()==true) {
			return SMALL;
		}
		if (this.middle.isSelected()==true) {
			return MIDDLE;
		}
		if (this.big.isSelected()==true) {
			return BIG;
		}
		return null;
	}
	public void setSudokuSize(String size) {
		if (size==null) {
			this.middle.setSelected(true);
		} else {
			switch (size) {
			case SMALL:
				this.small.setSelected(true);
				break;
			case MIDDLE:
				this.middle.setSelected(true);
				break;
			case BIG:
				this.big.setSelected(true);
				break;
			default:
			this.middle.setSelected(true);
					
			}
		}
		
			
	}
	public String getSaveType() {
		if (this.text.isSelected()==true) {
			return TEXT;
		}
		if (this.database.isSelected()==true) {
			return DATABASE;
		}
		return null;
	}
	public void setSaveType(String saveType) {
		if (saveType ==null ) {
			this.text.setSelected(true);
		} else {
			switch (saveType) {
			case TEXT: 
				this.text.setSelected(true);
				break;
			case DATABASE:
				this.database.setSelected(true);
				break;
			default:
				this.text.setSelected(true);
			}
		}
		
	}
	public String getSolverType() {
		if (this.humanGenerate.isSelected()==true) {
			return HUMAN;
		}
		if (this.backtrackingGenerate.isSelected()==true) {
			return BACKTRACKING;
		}
		return null;
	}
	public void setSolverType(String solverType) {
		switch (solverType) {
		case HUMAN:
			this.humanGenerate.setSelected(true);
			break;
		case BACKTRACKING:
			this.backtrackingGenerate.setSelected(true);
			break;
		default:
			this.humanGenerate.setSelected(true);
		}
	}
	public boolean isSelectNakedSingle() {
		return this.nakedSingle.isSelected();
	}
	public void setNakedSingle (Boolean bool) {
		this.nakedSingle.setSelected(bool);
	}
	public boolean isSelectHiddenSingle () {
		return this.hiddenSingle.isSelected();
	}
	public void setHiddenSingle (Boolean bool) {
		this.hiddenSingle.setSelected(bool);
	}
	public boolean isSelectShowSolve() {
		return this.showSolve.isSelected();
	}
	public void setShowSolve(Boolean bool) {
		this.showSolve.setSelected(bool);
	}
	public boolean isSelectSmartSolve() {
		return this.smartSolve.isSelected();
	}
	public void setSmartSolve(Boolean bool) {
		this.smartSolve.setSelected(bool);
	}
	public String getGeneratorType () {
		if (this.humanGenerate.isSelected()==true) {
			return HUMAN;
		}
		if (this.backtrackingGenerate.isSelected()==true) {
			return BACKTRACKING;
		}
		return null;
	}
	public void setGeneratorType (String generatorType) {
		if (generatorType == null) {
			this.backtrackingGenerate.setSelected(true);
		} else {
			switch (generatorType) {
			case HUMAN:
				this.humanGenerate.setSelected(true);
				break;
			case BACKTRACKING:
				this.backtrackingGenerate.setSelected(true);
				break;
			default:
				
			}
		}
		
	}
	
	public String getGrade () {
		if (this.easy.isSelected()==true) {
			return EASY;
		} 
		if (this.normal.isSelected()==true){
			return NORMAL;
		}
		if (this.hard.isSelected()==true) {
			return HARD;
		}
		if(this.hell.isSelected()==true) {
			return HELL;
		}
		return null;
	}
	public void setGrade (String grade) {
		if (grade==null) {
			this.normal.setSelected(true);
		} else {
			switch (grade) {
			case EASY:
				this.easy.setSelected(true);
				break;
			case NORMAL:
				this.normal.setSelected(true);
				break;
			case HARD:
				this.hard.setSelected(true);
				break;
			case HELL:
				this.hell.setSelected(true);
				break;
			default:
				this.normal.setSelected(true);
			}
		}
		
	}

	public void setGrade(String grade, boolean bool) {
		switch (grade) {
		case EASY:
			this.easy.setEnabled(bool);
			break;
		case NORMAL:
			this.normal.setEnabled(bool);
			break;
		case HARD:
			this.hard.setEnabled(bool);
			break;
		case HELL:
			this.hell.setEnabled(bool);
			break;
	    
		default: this.normal.setSelected(true);
		}	
	}
	
	@Override
	public void itemStateChanged(ItemEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	public void setSaveListener (ActionListener e) {
		this.save.addActionListener(e);
	}
	public void setExitListener (ActionListener e) {
		this.exit.addActionListener(e);
	}
	public void setResetListener (ActionListener e) {
		this.reset.addActionListener(e);
	}
	public void setsearchFastFileListener (ActionListener e) {
		this.searchFastFile.addActionListener(e);
	}
	public void setBenchmarkFileListener (ActionListener e) {
		this.searchBenchmarkFile.addActionListener(e);
	}
	public void setInstallTablesListener (ActionListener e) {
		this.installTables.addActionListener(e);
	}
	public void setDeinstallTablesListener (ActionListener e) {
		this.deinstallTables.addActionListener(e);
	}
	public void setGenerateTypeListener (ActionListener e) {
		this.backtrackingGenerate.addActionListener(e);
		this.humanGenerate.addActionListener(e);
	}
	
	public void setTypeListener (ActionListener e) {
		this.text.addActionListener(e);
		this.database.addActionListener(e);
	}
	public void setWarningGradeListener(ActionListener e) {
		this.hell.addActionListener(e);
	}
	
	public void setDatabaseFields (Boolean bool) {
		user.setEditable(bool);
		db.setEditable(bool);
		host.setEditable(bool);
		password.setEditable(bool);
	}
	

	public void resetView() {
		// TODO Auto-generated method stub
		this.fastLoadingFile.setText("");
		this.benchmarkFile.setText("");
		this.showSolve.setSelected(false);
		this.smartSolve.setSelected(false);
		this.hiddenSingle.setSelected(true);
		this.nakedSingle.setSelected(true);
		this.text.setSelected(true);
		this.backtrackingGenerate.setSelected(true);
		this.normal.setSelected(true);
		
	}

	
	public void initComponents() {
		// TODO Auto-generated method stub
		main = new JPanel(new BorderLayout(5,5));
		buttons = new JPanel(new FlowLayout());
		
		JTabbedPane tabbedPane = new JTabbedPane();
		
		save = new JButton("Speichern");
		exit = new JButton("Abbrechen");
		reset = new JButton("Standardeinstellungen laden");
		
		buttons.add(save);
		buttons.add(exit);
		buttons.add(reset);
		
		//Panels
		sudoku = new JPanel(new BorderLayout(5,5));
		
		file = new JPanel(new GridLayout(1,3));
		//////////////////////////////////////////
		fastLabel = new JLabel("Datei: ");
		fastLoadingFile = new JTextField(Registry.get().getString("fastFile"));
		fastLoadingFile.setEditable(false);

		
		searchFastFile = new JButton("Durchsuchen...");
		//////////////////////////////////////////
		
		//////////////////////////////////////////
		file.add(fastLabel);
		file.add(fastLoadingFile);
		file.add(searchFastFile);
		
		
		
		
		//////////////////////////////////////////
		
		sudokuGameSettings = new JPanel(new GridLayout(2,5));
		sizeLabel = new JLabel("Gr��e:");
		typeLabel = new JLabel("Typ des Speicherns:");
		
		size = new ButtonGroup();
		type = new ButtonGroup();
		
		small = new JRadioButton("4x4");
		small.addItemListener(this);
		
		middle =  new JRadioButton("9x9");
		middle.addItemListener(this);
		
		big = new JRadioButton("16x16");
		big.addItemListener(this);
		
		size.add(small);
		size.add(middle);
		size.add(big);
		
		text = new JRadioButton("Textdatei");
		text.addItemListener(this);
		
		database = new JRadioButton("Datenbank");
		database.addItemListener(this);
		
		type.add(text);
		type.add(database);
		
		
		///////////////////////////////////////////////
		//Database Settings
		///////////////////////////////////////////////
		databaseSettings = new JPanel(new GridLayout(6,2));
		//databaseSettings.setVisible(false);
		hostLabel = new JLabel("Host");
		dbLabel = new JLabel("Datenbank");
		userLabel = new JLabel("User");
		pwLabel   = new JLabel("Passwort");
		tableLabel = new JLabel("Tabellen");
		dummyLabel = new JLabel("");
		host = new JTextField("");
		
		user = new JTextField("");
		password = new JPasswordField("");
		db = new JTextField("");
		
		
		installTables = new JButton("Installation"); 
		deinstallTables = new JButton ("Deinstallation"); 
		
		
		databaseSettings.add(hostLabel);
		databaseSettings.add(host);
		databaseSettings.add(dbLabel);
		databaseSettings.add(db);
		databaseSettings.add(userLabel);
		databaseSettings.add(user);
		databaseSettings.add(pwLabel);
		databaseSettings.add(password);
		databaseSettings.add(tableLabel);
		databaseSettings.add(dummyLabel);
		databaseSettings.add(installTables);
		databaseSettings.add(deinstallTables);
		
		sudokuGameSettings.add(sizeLabel);
		sudokuGameSettings.add(small);
		sudokuGameSettings.add(middle);
		sudokuGameSettings.add(big);
		sudokuGameSettings.add(typeLabel);
		sudokuGameSettings.add(text);
		sudokuGameSettings.add(database);
		sudoku.add(BorderLayout.NORTH, file);
		sudoku.add(BorderLayout.CENTER,sudokuGameSettings );
		sudoku.add(BorderLayout.SOUTH, databaseSettings);
		
		///////////////////////////////////////////////
		//Benchmarck Settings
		///////////////////////////////////////////////
		benchmarkSettings = new JPanel();
		bMarckLabel = new JLabel("Datei: ");
		
		benchmarkFile = new JTextField(20);

		benchmarkFile.setEditable(false);
		
		searchBenchmarkFile = new JButton("Durchsuchen...");
		
		benchmarkSettings.add(bMarckLabel);
		benchmarkSettings.add(benchmarkFile);
		benchmarkSettings.add(searchBenchmarkFile);
		///////////////////////////////////////////////
		//Solver Settings
		///////////////////////////////////////////////
		solverSettings = new JPanel(new BorderLayout(2,2));
		backtrackingSolverSettings = new JPanel(new GridLayout(3,1));
		humanSolverSettings = new JPanel(new GridLayout(3,1));
		
		//BacktrackingSolver
		backtrackingLabel = new JLabel("Backtracking Solver");
		
		showSolve = new JCheckBox("Fortschritt anzeigen");
		showSolve.addItemListener(this);
		
		smartSolve = new JCheckBox("Intelligentes L�sen");
		smartSolve.addItemListener(this);
		
		
		backtrackingSolverSettings.add(backtrackingLabel);
		backtrackingSolverSettings.add(showSolve);
		backtrackingSolverSettings.add(smartSolve);
		//HumanSolver
		humanLabel = new JLabel("Human Solver");
		hiddenSingle = new JCheckBox("Hidden Single");
		nakedSingle = new JCheckBox("Naked Single");
		
		humanSolverSettings.add(humanLabel);
		humanSolverSettings.add(nakedSingle);
		humanSolverSettings.add(hiddenSingle);
		
		solverSettings.add(BorderLayout.NORTH, backtrackingSolverSettings);
		solverSettings.add(BorderLayout.CENTER, humanSolverSettings);
		///////////////////////////////////////////////
		//Generator
		///////////////////////////////////////////////
		generatorSettings = new JPanel(new GridLayout(8,1));
		
		generatorLabel = new JLabel("Generator");
		
		generator = new ButtonGroup();
		
		backtrackingGenerate = new JRadioButton("Backtracking-Generator");
		backtrackingGenerate.addItemListener(this);
		
		humanGenerate = new JRadioButton("Human-Generator");
		humanGenerate.addItemListener(this);
		
		generator.add(backtrackingGenerate);
		generator.add(humanGenerate);
		gradeLabel = new JLabel("Schwierigkeitsgrad");
		grade = new ButtonGroup();
		
		
		
		//Tooltipp texte
		String easyToolTip   = "<html><b>Generiert ein Sudoku mit ca. 45-55 festen Feldern </b></html>";
		String normalToolTip = "<html><b>Generiert ein Sudoku mit ca. 35-45 festen Feldern </b></html>";
		String hardToolTip   = "<html><b>Generiert ein Sudoku mit ca. 25-35 festen Feldern </b></html>";
		String hellToolTip 	 = "<html><b>Generiert ein Sudoku mit ca. 17-25 festen Feldern </b></html>";
		
		
		
		easy = new JRadioButton("Leicht");
		easy.addItemListener(this);
		easy.setToolTipText(easyToolTip);
		
		normal = new JRadioButton("Mittel");
		normal.addItemListener(this);
		normal.setToolTipText(normalToolTip);
		
		hard = new JRadioButton("Schwer");
		hard.addItemListener(this);
		hard.setToolTipText(hardToolTip);
		
		hell = new JRadioButton("H�lle");
		hell.addItemListener(this);
		hell.setToolTipText(hellToolTip);
		
		
		grade.add(easy);
		grade.add(normal);
		grade.add(hard);
		grade.add(hell);
		
		generatorSettings.add(generatorLabel);
		generatorSettings.add(backtrackingGenerate);
		generatorSettings.add(humanGenerate);
		generatorSettings.add(gradeLabel);
		generatorSettings.add(easy);
		generatorSettings.add(normal);
		generatorSettings.add(hard);
		generatorSettings.add(hell);
		
		
		tabbedPane.addTab("Sudoku", sudoku);
		tabbedPane.addTab("L�ser", solverSettings);
		tabbedPane.addTab("Generator", generatorSettings);
		tabbedPane.addTab("Benchmark", benchmarkSettings);
		main.add(BorderLayout.NORTH,tabbedPane);
		main.add(BorderLayout.SOUTH, buttons);
		getContentPane().add(main);
		
		setSize(500,500);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		this.setHiddenSingle(Boolean.valueOf(Registry.get().getString("hiddensingle")));
		this.setSudokuFile(Registry.get().getString("fastfile"));
		this.setSudokuSize(String.valueOf(Registry.get().getInt("size")));
		this.setGeneratorType(Registry.get().getString("generatetype"));
		if(Registry.get().getString("generatetype").equals("human")) {
			this.setGrade("hell", false);
		}
		this.setGrade(Registry.get().getString("grade"));
		this.setSmartSolve(Boolean.valueOf(Registry.get().getString("smartsolve")));
		this.setShowSolve(Boolean.valueOf(Registry.get().getString("showsolve")));
		this.setNakedSingle(Boolean.valueOf(Registry.get().getString("nakedsingle")));
		this.setUser(Registry.get().getString("user"));
		this.setDatabase(Registry.get().getString("db"));
		this.setPassword(Registry.get().getString("pw"));
		this.setHost(Registry.get().getString("host"));
		this.setSaveType(Registry.get().getString("savetype"));
		this.setBenchmarkFile(Registry.get().getString("bfile"));
		//Aufruf View
		//Daten aus Config holen
		//View changen
		
	}

}
