package view;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DecimalFormat;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import classes.Benchmark;
import classes.Chart;
import classes.Registry;
import exception.SudokuException;

/**
 * <strong>Klasse BenchView</strong>
 * 
 * <p>Zeigt den BenchmarkView an</p>
 * 
 * @see		controller.BenchmarkController
 * @author 	Marco Martens
 * @version 1.0 Dezember 2012
 */

public class BenchView extends JDialog{

	private StartBenchmarkView _sbView;

	/**
	 * F�r den Compiler
	 */
	private static final long serialVersionUID = -5698296414133112545L;

	
	private final DecimalFormat FORMATNUMBER =  new DecimalFormat( "###,###,###,###" );   
	private JPanel backtracks;
	private JPanel times;
	private JTextArea results;
	
	public BenchView(Frame parent, StartBenchmarkView _sbView) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException, IOException, SudokuException {
		super(parent, "Benchmarkergebnisse", true);
		this._sbView = _sbView;
		initComponents();
	}

	public void initComponents() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException, IOException, SudokuException {
		// TODO Auto-generated method stub
		
		setLocationRelativeTo(null);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLayout(new BorderLayout(5,5));
		results = new JTextArea();
		results.setEditable(false);
		backtracks = new JPanel();
		backtracks.setSize(450, 450);
		times = new JPanel();
		times.setSize(450, 450);
		int numberOfSudoku = _sbView.getSudokuMerge();
		Benchmark bMarck = null;
		if(Registry.get().getString("savetype")=="db") {
			bMarck = new Benchmark();
		}
		if(Registry.get().getString("savetype")=="text") {
			bMarck = new Benchmark(new File(Registry.get().getString("bfile")));
		}
		
		
		//Controller
		bMarck.setElementNumber(numberOfSudoku);
		
			
		bMarck.run();
		String output = "";
		
		output += "Versuche gesamt: " + FORMATNUMBER.format(bMarck.getCompleteBacktrack()) + " 					Zeit gesamt: " + FORMATNUMBER.format(bMarck.getCompleteTime()) + " Millisekunden \n";
		output += "Durchschnittliche Versuche: " + FORMATNUMBER.format(bMarck.getMiddleBacktrack()) + " 				Durchschnittliche Zeit: " + FORMATNUMBER.format(bMarck.getMiddleTime()) + " Millisekunden \n";
		//Diagramme
		//Backtracks
		setSize(1000,1000);
		//init/update
		Chart chartBacktracks = new Chart("Backtracks", "Sudoku", "Anzahl der Backtracks");
		backtracks = chartBacktracks.run(bMarck.getTimes().size(), bMarck.getBacktracks());
		getContentPane().add(BorderLayout.NORTH, backtracks);
		//Neuzeichnen
		backtracks.revalidate();
		backtracks.repaint();  
		//Times
		Chart chartTimes = new Chart("Zeit", "Sudoku", "Zeit in Millisekunden");
		times = chartTimes.run(bMarck.getTimes().size(), bMarck.getTimes());
		getContentPane().add(BorderLayout.CENTER, times);
		times.revalidate();
		times.repaint();
		results.setText(output);
		getContentPane().add(BorderLayout.SOUTH, results);
		results.revalidate();
		results.repaint();
		_sbView.setVisible(false);
		setVisible(true);
	}

}
