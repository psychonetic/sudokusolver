package view;
import java.awt.Frame;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;

import javax.swing.*;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

/**
 * <strong>Klasse LicenceView</strong>
 * 
 * <p>Zeigt den Lizenzview an</p>
 * 
 * 
 * @see		controller.LicenceController
 * @author 	Marco Martens
 * @version 1.0 Dezember 2012
 *
 */

public class LicenceView  extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8821884126632377443L;

	private JTextPane licence;
	private JScrollPane scroll;
	private JButton exit;
	
	public LicenceView(Frame parent) throws IOException, URISyntaxException {
		//Init
		super(parent, "Lizenz", true);
		initComponents();
	}
	
	
	public void resetView() {
		// TODO Auto-generated method stub
	}
	
	
	
	public void initComponents() throws IOException, URISyntaxException {
		// TODO Auto-generated method stub
		setSize(600,600);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLayout(null);
		licence = new JTextPane();
		SimpleAttributeSet set = new SimpleAttributeSet();
		StyleConstants.setAlignment(set,StyleConstants.ALIGN_CENTER);
		licence.setParagraphAttributes(set,true);
		licence.setEditable(false);
		
		
		exit = new JButton("OK");
		exit.setBounds(0, 525, 100, 25);
		
		//init
		String licenceText = ""; 
	
		
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream("ressource/licence.txt");
		
		int line;
		while((line = inputStream.read()) != -1) {	
			licenceText += (char)line;
		}
		licence.setText(licenceText);
		scroll = new JScrollPane(licence);
		scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scroll.setBounds(0,0,580,500);
		licence.setCaretPosition(0);
		getContentPane().add(scroll);
		getContentPane().add(exit);
		inputStream.close();
		
	}
	public void setExitListener (ActionListener e) {
		this.exit.addActionListener(e);
	}
	

	

	
	

}
