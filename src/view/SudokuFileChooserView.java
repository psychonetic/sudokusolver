package view;
import java.io.File;

import javax.swing.JFileChooser;
import classes.TxtFileFilter;

/**
 * <strong>Klasse SudokuFileChooserView</strong>
 * 
 * <p>�ffnet ein Durchsuchendialog um eine Datei auszuw�hlen.</p>
 * 
 * @see		controller.SaveSudokuTxtController
 * @see		controller.OpenSudokuTextController
 * @see		controller.SettingsController
 * @author 	Marco Martens
 * @version 1.0 Dezember 2012
 *
 */

public class SudokuFileChooserView {
	private int returnValue;
	private JFileChooser chooser;
	
	public SudokuFileChooserView (String type) {
		initComponents(type);
	}
	
	public int getReturnValue() {
		return this.returnValue;
	}
	
	public String getSelectedFileName () {
		if(chooser.getSelectedFile()!=null) {
			return String.valueOf(chooser.getSelectedFile());
		}
		return null;
	}
	public void initComponents(String type) {
		// TODO Auto-generated method stub
		chooser = new JFileChooser();
		chooser.setFileFilter(new TxtFileFilter());
		chooser.setMultiSelectionEnabled(false);
		File currentDirectory = new File("").getAbsoluteFile();
		chooser.setCurrentDirectory(currentDirectory);
		switch (type) {
			case "open":  returnValue = chooser.showOpenDialog(null);
			break;
			case "save":  returnValue = chooser.showSaveDialog(null);
			break;
			
			default: returnValue = chooser.showOpenDialog(null);
		}
		
		
		
	}
	

}
