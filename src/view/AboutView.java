package view;

import java.awt.Frame;
import java.awt.event.ActionListener;

import javax.swing.*;

/**
 * <strong>Klasse AboutView</strong>
 * 
 * <p>Zeigt das �berMenu an.</p>
 * 
 * @see		controller.AboutController
 * @author 	Marco Martens
 * @version 1.0 Dezember 2012
 *
 */

public class AboutView  extends JDialog{

	/**
	 * F�r den Compiler
	 */
	
	private static final long serialVersionUID = -8821884126632377443L;

	private JPanel main;
	private JTextArea aboutArea;
	private JButton exit;
	private String abouttext = "";
	
	public AboutView(Frame parent) {
		//Init
		super(parent, "�ber", true);
		initComponents();
	}
	
	private void setText(String text) {
		abouttext = text;
	}
	public String getText() {
		return this.abouttext;
	}
	
	public void initComponents() {
		// TODO Auto-generated method stub
		setSize(400,200);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		main = new JPanel();
		aboutArea = new JTextArea();
		exit = new JButton("OK");
		//setText
		setText ("Sudoku Solver und Generator " +  System.getProperty("line.separator") + 
				"Version: 2.0 " + System.getProperty("line.separator") +
				"Copyright: Marco Martens 2012-2013. All rights reserved. " + System.getProperty("line.separator"));
		//init
		aboutArea.setText(abouttext);
		aboutArea.setEditable(false);

		main.add(aboutArea);
		main.add(exit);
		getContentPane().add(main);
		pack();
	}
	
	public void setExitListener (ActionListener e) {
		this.exit.addActionListener(e);
	}
	

	

	
	

}
