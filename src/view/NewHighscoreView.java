package view;

import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
/**
 * <strong>Klasse NewHighscoreView</strong>
 * 
 * <p>Dieser View wird dargestellt, wenn der Spieler
 * ein Sudoku erfolgreich gel�st hat ohne L�ser.</p>
 * 
 * @see		HighscoreView
 * @author 	Marco Martens
 * @version 1.0 Dezember 2012
 *
 */
public class NewHighscoreView extends JDialog implements ActionListener{
	
	private static final long serialVersionUID = 1092840173318976580L;
	private JButton save;
	
	private JTextField name;
	private JPanel main;
	private JLabel info;
	private JLabel dummy;
	private String playerName;
	
	public NewHighscoreView (Frame parent) {
		//init
		super(parent, "Neuer Highscore", true);
		initComponents();
	}
	
	public String getName() {
		return this.playerName;
	}

	public void initComponents() {
		setSize(400,125);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		//Panel
		
		main = new JPanel(new GridLayout(2,1));
		//Textfeld
		name = new JTextField("");
		dummy = new JLabel("");
		//Label
		info = new JLabel("Geben Sie ihren Namen ein.");
		//Button
		save = new JButton("OK");
		save.addActionListener(this);
		//Buttons auf das Panel hinzuf�gen
		main.add(info);
		main.add(dummy);
		main.add(name);
		main.add(save);
		getContentPane().add(main);
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		this.playerName = name.getText();
		setVisible(false);
		dispose();
		
		
	}
}
