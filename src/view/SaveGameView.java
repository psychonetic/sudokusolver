package view;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Vector;
import javax.swing.*;
import classes.Registry;
import classes.SavegameHandlerDatabase;
import classes.SavegameHandler;

/**
 * <strong>Klasse SaveGameView</strong>
 * 
 * <p>Zeigt die Spielstände an</p>
 * 
 * @see		NewSaveGameView
 * @author 	Marco Martens
 * @version 1.0 Dezember 2012
 *
 */

public class SaveGameView extends JDialog {

	private static final long serialVersionUID = -1983382344772286026L;
	private JPanel savegame;
	private JPanel buttons;
	private JButton delete;
	private JButton exit;
	private JButton load;
	private JComboBox<String> dropDown;
	private DefaultComboBoxModel<String> comboModel; 
	
	
	public SaveGameView (Frame parent) throws URISyntaxException, Exception {
	//init
		super(parent, "Spielstände", true);
		initComponents();
	}
	
	public void setExitListener (ActionListener e) {
		this.exit.addActionListener(e);
	}
	
	public void setDeleteListener (ActionListener e) {
		this.delete.addActionListener(e);
	}
	
	public void setLoadListener (ActionListener e) {
		this.load.addActionListener(e);
	}
	
	public String getSavegame () {
		return (String) this.comboModel.getSelectedItem();
	}
	
	public void resetView() {
		// TODO Auto-generated method stub
		
	}
	
	public void initComponents() throws URISyntaxException, Exception {
		// TODO Auto-generated method stub
		setSize(400,400);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		savegame = new JPanel();
		buttons = new JPanel(new FlowLayout(2));
		dropDown = new JComboBox<String>();
		 //Model?^^ 
		HashMap<String, String> saves = new HashMap<String, String>();
		Vector<String> saveVec = new Vector<String>();
		if(Registry.get().getString("savetype").equals("text")) {
			SavegameHandler  sgame = new SavegameHandler ();
			saves = sgame.loadGame(new File("config/save.txt"));
		}
		if(Registry.get().getString("savetype").equals("db")) {
			
			SavegameHandlerDatabase sDB = null;
			
			sDB = new SavegameHandlerDatabase();
			
			saves = sDB.getAllSavegames();
		}
		
		for (Entry<String, String> e : saves.entrySet()) {
			saveVec.add((String) e.getKey());
		}
		comboModel = new DefaultComboBoxModel<String>(saveVec);
		
		dropDown.setModel(comboModel);
		if(saves.size() == 0) {
			comboModel.setSelectedItem("Keine Spielstände vorhanden");
		} else {
			comboModel.setSelectedItem("Bitte auswählen");
		}
		
		exit = new JButton("Abbrechen");
		load = new JButton("Laden");
		delete = new JButton("Löschen");
		

		getContentPane().add(savegame);
		getContentPane().add(buttons);
		getContentPane().add(dropDown);
		getContentPane().setLayout(new BorderLayout(5,5));
		
		buttons.add(delete);
		buttons.add(load);
		buttons.add(exit);
		getContentPane().add(BorderLayout.NORTH, dropDown);
		getContentPane().add(BorderLayout.CENTER, savegame);
		getContentPane().add(BorderLayout.SOUTH, buttons);
	}

	
}
