package view;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * <strong>Klasse StatisticView</strong>
 * 
 * <p>Zeigt dem User einige wichtige Daten 
 * zu seinem Spielverhalten.</p>
 * 
 * <p>Dazu geh�ren folgende Kriterien:</p>
 * 
 * <li>Letzte L�sungszeit</li>
 * <li>Zeitgesamt</li>
 * <li>Letzte Anzahl an Versuchen</li>
 * <li>Versuchegesamt</li>
 * <li>Anzahl des Backtrackingalgorithmus zur L�sung des Sudokus</li>
 * <li>Anzahl des Humansolvealgorithmus zur L�sung des Sudokus</li>
 * <li>Gespielte Spiele</li>
 * <li>Gewonnene Spiele</li>
 * <li>Verlorene Spiele</li>
 * <li>Anzahl der durchgef�hrten Benchmarks</li>
 * <li>Anzahl der Tipps</li>
 * 
 * @see		classes.StatisticController
 * @author 	Marco Martens
 * @version 1.0 Dezember 2012
 *
 */

public class StatisticView extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6379141796661349837L;
	private JPanel main;
	private JLabel title;
	private JPanel statistic;
	private JButton exit;
	
	private JLabel backtrackingLabel;
	private JLabel tippLabel;
	private JLabel lastBacktracksLabel;
	private JLabel backtracksLabel;
	private JLabel humanSolveLabel;
	private JLabel timeLabel;
	private JLabel lastTimeLabel;
	private JLabel playedGamesLabel;
	private JLabel loseGamesLabel;
	private JLabel wonGamesLabel;
	private JLabel benchmarksLabel;
	
	private JLabel backtracking;
	private JLabel tipp;
	private JLabel lastBacktracks;
	private JLabel backtracks;
	private JLabel humanSolve;
	private JLabel time;
	private JLabel lastTime;
	private JLabel playedGames;
	private JLabel loseGames;
	private JLabel wonGames;
	private JLabel benchmarks;
	
	public static void main (String[] args) {
		@SuppressWarnings("unused")
		StatisticView Pg = new StatisticView(null);
		
	}
	
	public StatisticView (Frame parent) {
		super();
		initComponents();
	}
	

	public void setBacktrackingSolve (String text) {
		backtracking.setText( text);
	}
	public  void setTipp (String text) {
		tipp.setText( text);
	}
	public  void setLastBacktracks (String text) {
		lastBacktracks.setText(text);
	}
	public void setBacktracks(String text) {
		backtracks.setText(text);
	}
	public  void setHumanSolve (String text) {
		humanSolve.setText( text);
	}
	public  void setTime(String text) {
		time.setText(time.getText() + text);
	}
	public  void setLastTime(String text) {
		lastTime.setText(text);
	}
	public  void setPlayedGames (String text) {
		playedGames.setText( text);
	}
	public  void setLoseGames (String text) {
		loseGames.setText(text);
	}
	public void setWonGames (String text) {
		wonGames.setText( text);
	}
	public void setBenchmarks (String text) {
		benchmarks.setText(text);
	}
	
	public void initComponents() {
		// TODO Auto-generated method stub
		main = new JPanel(new BorderLayout());
		title = new JLabel("Statistik");
		statistic = new JPanel(new GridLayout(11,2));
		
		 backtrackingLabel = new JLabel("Gel�ste Sudokus mit Backtracking: ");
		 tippLabel= new JLabel("Hilfe benutzt: ");
		 lastBacktracksLabel= new JLabel("Letzte Anzahl an Backtrackversuchen: ");
		 backtracksLabel= new JLabel("Gesamte Anzahl an Backtrackversuchen: ");
		 humanSolveLabel= new JLabel("Gel�ste Sudokus mit HumanSolve: ");
		 timeLabel= new JLabel("Gesamte L�sungszeit: ");
		 lastTimeLabel= new JLabel("Letzte Zeit beim L�sen mit einem Solver (in ms): ");
		 playedGamesLabel= new JLabel("Gespielte Sudokus: ");
		 loseGamesLabel= new JLabel("Nicht gel�ste oder abgebrochene Sudokus: ");
		 wonGamesLabel= new JLabel("Gel�ste Sudokus: ");
		 benchmarksLabel = new JLabel("Anzahl der Benchmarks: ");
		 
		 
		 backtracking = new JLabel();
		 tipp= new JLabel();
		 lastBacktracks= new JLabel();
		 backtracks= new JLabel();
		 humanSolve= new JLabel();
		 time= new JLabel();
		 lastTime= new JLabel();
		 playedGames= new JLabel();
		 loseGames= new JLabel();
		 wonGames= new JLabel();
		 benchmarks = new JLabel();
		 
		 statistic.add(playedGamesLabel);
		 statistic.add(playedGames);
		 statistic.add(wonGamesLabel);
		 statistic.add(wonGames);
		 statistic.add(loseGamesLabel);
		 statistic.add(loseGames);
		 statistic.add(tippLabel);
		 statistic.add(tipp);
		 statistic.add(backtrackingLabel);
		 statistic.add(backtracking);
		 statistic.add(humanSolveLabel);
		 statistic.add(humanSolve);
		 statistic.add(backtracksLabel);
		 statistic.add(backtracks);
		 statistic.add(lastBacktracksLabel);
		 statistic.add(lastBacktracks);
		 statistic.add(timeLabel);
		 statistic.add(time);
		 statistic.add(lastTimeLabel);
		 statistic.add(lastTime);
		 statistic.add(benchmarksLabel);
		 statistic.add(benchmarks);
		 
		
		
		exit = new JButton("Ok");
		
		main.add(BorderLayout.NORTH, title);
		main.add(BorderLayout.CENTER, statistic);
		main.add(BorderLayout.SOUTH, exit);
		
		getContentPane().add(main);
		setSize(600,500);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	}

	public void setExitListener(ActionListener e) {
		this.exit.addActionListener(e);
	}
	

}
