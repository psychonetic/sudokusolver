package view;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JSlider;

import classes.Registry;
import classes.SudokuHandler;
import exception.SudokuException;

/**
 * <strong>Klasse StartBenchmarkView</strong>
 * 
 * <p>Bietet dem User eine ungef�hre Auswahl zu treffen
 * wie viele Sudokus f�r einen Benchmark anhand seiner ausgew�hlten
 * Datei genutzt werden sollen.</p>
 * 
 * <p><strong>Nicht getestet mit gro�en Sudokus! Dies k�nnte unter Umst�nden viel Zeit in Anspruch nehmen.</strong></p>
 * 
 * @see		controller.BenchmarkController
 * @author 	Marco Martens
 * @version 1.0 Dezember 2012
 *
 */

public class StartBenchmarkView extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7460925902819967778L;

	private JSlider slider;
	private JButton start;

	public StartBenchmarkView (Frame parent) throws IOException, SudokuException {
		super(parent, "Benchmark", true);
		initComponents();
	}
	
	public void setStartListener(ActionListener e) {
		this.start.addActionListener(e);
	}

	
	public void resetView() {
		// TODO Auto-generated method stub
		
	}
	
	public int getSudokuMerge () {
		return this.slider.getValue();
	}

	
	public void initComponents() throws IOException, SudokuException {
		// TODO Auto-generated method stub
		getContentPane().setLayout(new BorderLayout(5,5));
		setSize(400,200);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		SudokuHandler su = new SudokuHandler();
	
			//getSize
			List<String>  sudokus = new ArrayList<String>();
			sudokus = su.readSudoku(new File(Registry.get().getString("bfile")));
			int min = 0;
			int max = sudokus.size();
			//init slider
			slider = new JSlider(min, max);
			slider.setPaintLabels(true);
			slider.setPaintTicks(true);
			slider.setPaintTrack(true);
			int bigTick = (max / 5);
			
			slider.setMajorTickSpacing(bigTick);
		
		
		start = new JButton("Benchmark starten");
		getContentPane().add(BorderLayout.NORTH, slider);
		getContentPane().add(BorderLayout.SOUTH, start );
	}
}
