package view;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 * <strong>Klasse TransferView</strong>
 * 
 * <p>Zeigt dem User ein Dialog an um einen Import oder Export vorzunehmen.</p>
 * 
 * 
 * @see		controller.TransferController
 * @author 	Marco Martens
 * @version 1.0 Dezember 2012
 */


public class TransferView extends JDialog {
	
	
	/**
	 * F�r den Compiler
	 */
	private static final long serialVersionUID = -711491537085900652L;
	
	private JButton exit;
	private JButton start;
	private JButton search;
	private JTextField file;
	
	private JLabel pathLabel;
	private JLabel dummy;
	private JLabel sourceLabel;
	private JLabel targetLabel;
	
	private JComboBox<String> source;
	private JComboBox<String> target;
	
	public TransferView (Frame parent) {
		//init
		super(parent, "Import und Export", true);
		initComponents();
	}
	
	public void setExitListener (ActionListener e) {
		this.exit.addActionListener(e);
	}
	public void setStartListener (ActionListener e) {
		this.start.addActionListener(e);
	}
	public void setSearchListener (ActionListener e) {
		this.search.addActionListener(e);
	}
	
	public String getSource () {
		return (String) source.getSelectedItem();
		
	}
	public String getTarget () {
		return  (String)target.getSelectedItem();
		
	}
	public String getFile () {
		return this.file.getText();
	}
	public void setFile (String text) {
		this.file.setText(text);
	}


	public void initComponents() {
		// TODO Auto-generated method stub
		setSize(400,200);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLayout(new GridLayout(6,2));
		//Buttons
		exit = new JButton("Abbrechen");
		start = new JButton("OK");
		search = new JButton("Durchsuchen...");
		dummy = new JLabel("");
		pathLabel = new JLabel ("Pfad zur Datei: ");
		file = new JTextField("");
		file.setEditable(false);
		sourceLabel = new JLabel("Quelle: ");
		targetLabel = new JLabel ("Ziel: ");
		
		String list[] = {"Text", "Datenbank"};
		source = new JComboBox<String>(list);
		source.setSelectedItem("Text");
		target = new JComboBox<String>(list);
		target.setSelectedItem("Datenbank");
		//Buttons auf das Panel hinzuf�gen
		getContentPane().add(pathLabel);
		getContentPane().add(file);
		getContentPane().add(dummy);
		getContentPane().add(search);
		getContentPane().add(sourceLabel);
		getContentPane().add(source);
		getContentPane().add(targetLabel);
		getContentPane().add(target);
		getContentPane().add(start);
		getContentPane().add(exit);
	}
}
