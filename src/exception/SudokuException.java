package exception;

/**
 * <strong>Klasse SudokuException</strong>
 * 
 * <p>Diese Exception wird aufworfen wenn die Sudokugr��e
 * nicht korrekt ist.</p>
 * 
 * @author 	Marco Martens
 * @version	1.0 Oktober 2012
 *
 */
public class SudokuException extends Exception {
	
	
	/**
	 * F�r den Compiler
	 */
	private static final long serialVersionUID = 5038076223739740514L;

	/**
	 * Konstruktor
	 */
	
	public SudokuException () {
		
	}
	
	/**
	 * Konstruktor
	 * 
	 * Ruft den Konstruktor der Exceptionklasse auf.
	 * @param s
	 */
	
	public SudokuException (String s) {
		super(s);
	}
	
	
}
