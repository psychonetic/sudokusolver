package model;
/**
 * <strong>Klasse HighscoreModel</strong>
 * 
 * <p>Diese Klasse stellt einen Highscore dar.
 * Der Highscore umfasst das Sudoku, den Namen und die Zeit.</p>
 * 
 * 
 * @see 	classes.HighscoreXML
 * @see		classes.HighscoreDatabase
 * @author 	Marco Martens
 * @version 1.0 Oktober 2012
 *
 */
public class HighscoreModel implements Comparable<HighscoreModel> {

	
	/**
	 * Name
	 */
	
	private String 	name;
	
	/**
	 * Sudoku 
	 */
	private String 	sudoku;
	
	/**
	 * Zeit
	 */
	
	private long time;
	
	/**
	 * Setzt den Namen
	 * 
	 * @param name
	 */
	
	public void setName (String name) {
		this.name = name;
	}
	
	/**
	 * Setzt das Sudoku
	 * 
	 * @param sudoku
	 */
	
	public void setSudoku (String sudoku) {
		this.sudoku = sudoku;
	}
	
	/**
	 * Setzt die Zeit 
	 * @param time
	 */
	
	public void setTime (long time) {
		this.time = time;
	}
	
	/**
	 * Gibt die Zeit zur�ck.
	 * 
	 * @return
	 */
	
	public long getTime () {
		return this.time;
	}
	
	/**
	 * Gibt den Namen zur�ck. 
	 * @return
	 */
	
	public String getName () {
		return this.name;
	}
	
	/**
	 * Gibt das Sudoku zur�ck.
	 * 
	 * @return
	 */
	
	public String getSudoku () {
		return this.sudoku;
	}
	
	/**
	 * Erm�glicht es verschiedene SudokuModel nach der Zeit
	 * zu sortieren um den besten Spieler zu ermitteln.
	 */

	@Override
	public int compareTo(HighscoreModel arg0) {
		// TODO Auto-generated method stub
		if (time < arg0.time) {
			return -1;
		}
		if (time > arg0.time) {
			return 1;
		}
		
		
		return 0;
	}

	
	
}
