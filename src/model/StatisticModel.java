package model;

/**
 * <strong>Klasse StatisticModel</strong>
 * 
 * <p>Diese Klasse stellt die Statistiken dar.
 * Es werden eine Vielzahl von Aktionen des Users protokolliert.
 * Dazu geh�ren folgende Kriterien:</p>
 * 
 * <li>Letzte L�sungszeit</li>
 * <li>Zeitgesamt</li>
 * <li>Letzte Anzahl an Versuchen</li>
 * <li>Versuchegesamt</li>
 * <li>Anzahl des Backtrackingalgorithmus zur L�sung des Sudokus</li>
 * <li>Anzahl des Humansolvealgorithmus zur L�sung des Sudokus</li>
 * <li>Gespielte Spiele</li>
 * <li>Gewonnene Spiele</li>
 * <li>Verlorene Spiele</li>
 * <li>Anzahl der durchgef�hrten Benchmarks</li>
 * <li>Anzahl der Tipps</li>
 * 
 * @author Marco Martens
 * @version 1.0 Dezember 2012
 *
 */


public class StatisticModel {

	
	/**
	 *  Letzte Zeit
	 */
	
	private long lastTime;
	
	/**
	 * Gesamtzeit
	 */
	
	private long time;
	
	/**
	 * Letzte Anzahl an Versuchen
	 */
	
	private long lastBacktracks;
	
	/**
	 * Gesamtanzahl der Versuche 
	 */
	
	private long backtracks;
	
	/**
	 * Anzahl der Verwendung des Backtrackingalgorithmus 
	 */
	
	private int countBacktracking;
	
	/**
	 * Anzahl der Verwendung des Humansolvealgorithmus
	 */
	
	private int countHumanSolve;
	
	/**
	 * Gespielte Sudokus
	 */
	
	private int playedSudokus;
	
	/**
	 * Gewonnene Sudokus 
	 */
	
	private int wonSudokus;
	
	/**
	 * Verlorene Sudokus
	 */
	
	private int loseSudokus;
	
	/**
	 * Anzahl der Benchmarks
	 */
	
	private int benchmarks;
	
	/**
	 * Anzahl der verwendeten Tipps 
	 */
	
	private int tipps;
	
	/**
	 * Gibt die letzte Zeit zur�ck
	 * 
	 * @return lastTime
	 */
	
	public long getLastTime() {
		return lastTime;
	}

	/**
	 * Setzt die letzte Zeit
	 * 
	 * @param lastTime
	 */
	
	public void setLastTime(long lastTime) {
		this.lastTime = lastTime;
	}
	
	/**
	 * Setzt die Gesamtzeit
	 * 
	 * @param time
	 */
	
	public void setTime (long time) {
		this.time = time;
	}
	
	/**
	 * Gibt die Gesamtzzeit zur�ck
	 * 
	 * @return time
	 */
	
	public long getTime () {
		return this.time;
	}

	/**
	 * Gibt die letzte Anzahl an Versuchen zur�ck
	 *  
	 * @return lastBacktracks
	 */
	

	public long getLastBacktracks() {
		return lastBacktracks;
	}

	/**
	 * Setzt die letzte Anzahl an Versuchen
	 * 
	 * @param l
	 */


	public void setLastBacktracks(long l) {
		this.lastBacktracks = l;
	}

	/**
	 * Gibt die Anzahl an Versuchen zur�ck
	 * 
	 * @return lastBacktracks
	 */


	public long getBacktracks() {
		return backtracks;
	}

	/**
	 * Setzt die Anzahl an Versuchen
	 * 
	 * @param l
	 */


	public void setBacktracks(long l) {
		this.backtracks = l;
	}


	/**
	 * Gibt die Anzahl an Backtrackings zur�ck
	 *  
	 * @return countBacktracking
	 */

	public long getCountBacktracking() {
		return countBacktracking;
	}


	/**
	 * Setzt die Anzahl an Backtrackings
	 * 
	 * @param i
	 */

	public void setCountBacktracking(int i) {
		this.countBacktracking = i;
	}


	/**
	 * Gibt die Anzahl an Humansolves zur�ck
	 * 
	 * @return countHumanSolve
	 */

	public long getCountHumanSolve() {
		return countHumanSolve;
	}

	/**
	 * Setzt die Anzahl an Humansolves
	 * 
	 * @param i
	 */


	public void setCountHumanSolve(int i) {
		this.countHumanSolve = i;
	}

	/**
	 * Gibt die Anzahl an gespielten Sudokus zur�ck
	 * 
	 * @return playedSudokus
	 */


	public int getPlayedSudokus() {
		return playedSudokus;
	}

	/**
	 * Setzt die Anzahl an gespielten Sudokus
	 * 
	 * @param playedSudokus
	 */


	public void setPlayedSudokus(int playedSudokus) {
		this.playedSudokus = playedSudokus;
	}

	/**
	 * Gibt die Anzahl an gewonnenen Sudokus zur�ck
	 * 
	 * @return wonSudokus
	 */


	public int getWonSudokus() {
		return wonSudokus;
	}

	/**
	 * Setzt die Anzahl an gewonnenen Sudokus
	 * 
	 * @param wonSudokus
	 */


	public void setWonSudokus(int wonSudokus) {
		this.wonSudokus = wonSudokus;
	}

	/**
	 * Gibt die Anzahl an verlorenen Sudokus zur�ck
	 * 
	 * @return
	 */


	public int getLoseSudokus() {
		return loseSudokus;
	}

	/**
	 * Setzt die Anzahl an verloreren Sudokus
	 * 
	 * @param loseSudokus
	 */


	public void setLoseSudokus(int loseSudokus) {
		this.loseSudokus = loseSudokus;
	}

	/**
	 * Gibt die Anzahl an Benchmarks zur�ck
	 * 
	 * 
	 * @return benchmarks
	 */
	
	public int getBenchmarks() {
		return benchmarks;
	}

	/**
	 * Setzt die Anzahl an Benchmarks
	 * 
	 * @param benchmarks
	 */
	
	public void setBenchmarks(int benchmarks) {
		this.benchmarks = benchmarks;
	}
	
	/**
	 * Gibt die Anzahl der verwendeten Tipps zur�ck
	 * 
	 * @return tipps
	 */

	public int getTipps() {
		return tipps;
	}

	/**
	 * Setzt die Anzahl an verwendeten Tipps
	 * 
	 * @param tipps
	 */
	
	public void setTipps(int tipps) {
		this.tipps = tipps;
	}


}
