package model;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

import classes.BaseSolver;
import classes.Registry;

/**
 * <strong>Klasse SudokuModel</strong>
 * 
 * <p>Diese Klasse repr�sentiert das JTableModel bzw. 
 * das Sudokufeld, auf dem der User seine Eingaben t�tig.</p>
 * 
 * 
 * @author Marco Martens
 * @version 1.0 Oktober 2012
 *
 */

public class SudokuModel extends AbstractTableModel {
	
	/**
	 * F�r den Compiler...
	 */
	
	private static final long serialVersionUID = 4914321112611553764L;
	
	/**
	 * Original Sudoku
	 * 
	 * <p>Das OriginalSudoku wird ben�tigt um zu �berpr�fen, ob die Zahl eine vorgebene Zahl ist oder vom User eingegeben wurde.
	 * Wenn es eine feste Zahl ist, dann darf der User diese Zahl nicht editieren.</p>
	 * 
	 * @see #isCellEditable(int, int)
	 */
	
	private int[][] orgSudoku = new int[Registry.get().getInt("size")][Registry.get().getInt("size")];
	
	/**
	 * Gr��e des Sudokufeldes
	 */
	
	Object[][] input =  new Object[Registry.get().getInt("size")][Registry.get().getInt("size")]; 

	/**
	 * Setzt das Originalsudoku
	 * 
	 * @param sudoku
	 */
	
	public void setOrgSudoku(int[][] sudoku) {
		this.orgSudoku = sudoku;
	}
	
	/**
	 * Gibt die Anzahl der Spalten wieder
	 * 
	 * @return input.lenght
	 * 
	 */
	
	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return input.length;
	}

	/**
	 * Gibt die Anzahl der Zeilen wieder
	 * 
	 * @return input[0].lenght
	 * 
	 */
	
	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return input[0].length;
	}

	/**
	 * Gibt den Wert an der Position Zeile/Spalte wieder
	 * 
	 * @param 	row
	 * @param 	col
	 * @return 	value
	 * 
	 */
	@Override
	public Object getValueAt(int row, int col) {
		// TODO Auto-generated method stub
		 
		return input[row][col];
		
	}
	
	/**
	 * Setzt den Wert an die Position Zeile/Spalte
	 * 
	 * <p>Der Wert wird aber nur unter der Bedinung gesetzt,
	 * dass dieser Wert hier g�ltig ist. F�r den User ist es so komfortabler,
	 * da er so keine "falschen" Werte eingeben kann.</p>
	 * 
	 * @param value
	 * @param row
	 * @param col
	 */
	
	@Override
	public void setValueAt (Object value, int row, int col) {
		
		
		
			Integer newvalue = null;
		
			try {
				
				newvalue = Integer.parseInt(value.toString());
				
			} 	catch (Exception e) {
				
				if (newvalue == null) {
					input[row][col]= null;
				} 
				
			}
		
				//Vom Typ Integer?
				if (newvalue instanceof Integer) {
					//�berpr�fen ob Zahl zwischen 1-9, wenn nein Message
					//Wenn Ja Sudoku holen
					//Wenn der aktuelle Wert = dem alten ist -> setzen
					//
					
					if ((newvalue > 0) && (newvalue <= getRowCount())) {
						
						int[][] sudoku = new int[getRowCount()][getColumnCount()];
						//Alle Values holen
						//int[9][9] sudoku;
						for (int tableRow = 0; tableRow < getRowCount(); tableRow++) {
							for (int tableCol = 0; tableCol < getColumnCount(); tableCol++) {
								
								if (getValueAt(tableRow, tableCol) == null) {
									sudoku[tableRow][tableCol] = 0;
								} else {
									sudoku[tableRow][tableCol]  = (int) getValueAt(tableRow, tableCol); 
								}
								
							}
						}
						
						BaseSolver solver = new BaseSolver(sudoku);
					
						
						@SuppressWarnings("unused")
						int oldValue; 
						if (getValueAt(row, col)==null) {
							oldValue = 0;
						} else {
							oldValue = (int) getValueAt(row, col);
						}
						
						if (sudoku[row][col] == newvalue) {
							sudoku[row][col] = newvalue;
						} else {
							if(solver.isPossible(row, col, newvalue )==true) {
								JOptionPane.showMessageDialog(null,"Die Zahl ist hier nicht erlaubt!", "Fehler", JOptionPane.CANCEL_OPTION);
							} else {
								input[row][col]=newvalue;
							}
						}
					}	else {
						JOptionPane.showMessageDialog(null,"Die Zahl muss zwischen 1-"+ getRowCount() + " liegen!", "Fehler", JOptionPane.CANCEL_OPTION);
					}
					
				fireTableDataChanged();
			
		}
				
		
		

	}
	
	/**
	 * Setzt alle Zellen des Models auf "null"
	 * 
	 */
	
	public void resetModel() {
		this.orgSudoku = new int[Registry.get().getInt("size")][Registry.get().getInt("size")];
		
		 this.input =  new Object[Registry.get().getInt("size")][Registry.get().getInt("size")]; 
	}
	
	/**
	 * Editierbarkeit des Models
	 * 
	 * @param 	rowIndex
	 * @param 	columnIndex
	 * @return 	boolean
	 * 
	 */
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		
		if (this.orgSudoku != null) {
			if(this.orgSudoku[rowIndex][columnIndex] != 0) {
						return false;
					}	
		}
		
		
		return true;
	}
	
	/**
	 *  Wandelt das SudokuModel in ein Array um
	 * 
	 *  <p> Diese Funktion wird ben�tigt, damit das Sudoku in einem einheitlichen Format ist.
	 *  Denn der {@link classes.HumanSolver} und der {@link classes.BacktrackSolver} k�nnen das Sudoku nur l�sen,
	 *  wenn es in einem Array vorliegt.
	 * 
	 *  @see #SudokuModeltoString()
	 *  @return sudoku
	 */
	
	public int[][] SudokuModeltoArray () {
		int[][] sudoku = new int[getRowCount()][getColumnCount()];
		//In Array umwandeln
		//Wird ben�tigt f�r das Backtracking
		for (int i = 0; i < getRowCount(); i++) {
			for (int j = 0; j < getColumnCount() ; j++) {
				if (getValueAt(i, j) == null) {
					sudoku[i][j] = 0;
				} else {
					sudoku[i][j] = (int) getValueAt(i, j);
				}
				
				
			}
		}
		return sudoku;
	}
	/**
	 * Wandelt das Sudoku in einem String um
	 * 
	 * <p>Diese Funktion wird ben�tigt, damit das Sudoku 
	 * gespeichert werden kann. Dabei wird das ganze Sudoku
	 * als 1-Zeiler abgelegt. </p>
	 * 
	 * @see classes.SavegameHandlerDatabase
	 * @see classes.SavegameHandler
	 * @see classes.HighscoreXML
	 * @see classes.HighscoreDatabase
	 * @see classes.SudokuDatabase
	 * @see classes.SudokuHandler
	 * 
	 * @return sudoku
	 */
	
	public String SudokuModeltoString () {
		String sudoku = "";
		for (int i = 0; i < getRowCount(); i++) {
			for (int j = 0; j < getColumnCount(); j++) {
				if (getValueAt(i, j) == null) {
					sudoku += "0"; 
				} else {
						sudoku += getValueAt(i, j);
					}
					
					
				}
		}
		return sudoku;
	}
	
	
}
