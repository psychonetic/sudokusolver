package main;
import controller.ApplicationController;

/**
 * <strong>Klasse Bootstrap</strong>
 * 
 * <p>Startet die Application</stron>
 * 
 * @see 	controller.ApplicationController
 * @author 	Marco Martens
 * @version 1.0 Dezember 2012
 */
public class Bootstrap {

	/**
	 * Startet die Application
	 * 
	 * @param args
	 */
	public static void main (String[] args) {
		ApplicationController   controller = new ApplicationController();
								controller.showView();
	}
	
}
